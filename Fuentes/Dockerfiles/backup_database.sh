docker-compose --project-directory .. run database pg_dump \
        --verbose --no-password --column-inserts --verbose \
        --host=localhost --username=finances_everywhere --dbname=dacs2017 \
        --file="fiew_${$(date +%Y%m%d-%I%M%S)}.sql"

