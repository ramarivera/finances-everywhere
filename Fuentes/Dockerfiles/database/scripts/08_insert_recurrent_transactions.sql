WITH frequency AS ( 
    INSERT INTO dacs_fe.frequency (period_length, valid_from, valid_until, offset_from_start)
    VALUES (3, '2020-03-01', '2020-09-01', 0) 
    RETURNING frequency_id AS id
)
INSERT INTO dacs_fe.recurrent_transaction
    (account_id, category_definition_id, title, expenditure, amount, frequency_id)
SELECT
    3,
    1,
    'Comida mensual',
    true,
    50000,
    frequency.id
FROM frequency;
