CREATE SCHEMA dacs_fe;

CREATE TABLE dacs_fe.currency
(
  currency_id SERIAL,
  name VARCHAR(100) NOT NULL,
  symbol VARCHAR(100) NOT NULL,
  iso_code VARCHAR(100),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY(currency_id)
);

CREATE TABLE dacs_fe.frequency
(
  frequency_id SERIAL,
  period_length INTEGER NOT NULL DEFAULT 0 CHECK (period_length IN (0, 1, 2, 3, 4, 5, 6)),
  valid_from TIMESTAMP NOT NULL DEFAULT NOW(),
  valid_until TIMESTAMP NOT NULL,
  offset_from_start INTEGER NOT NULL,
  PRIMARY KEY (frequency_id)
);

CREATE TABLE dacs_fe.department
(
  department_id SERIAL,
  parent_department_id INTEGER NULL,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(512),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (department_id),
  FOREIGN KEY (parent_department_id) REFERENCES dacs_fe.department (department_id)
);


CREATE TABLE dacs_fe.user_group
(
  user_group_id SERIAL,
  name VARCHAR(100),
  description VARCHAR(512),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (user_group_id)
);

CREATE TABLE dacs_fe.permission
(
  permission_id SERIAL,
  name VARCHAR(100),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (permission_id)
);


CREATE TABLE dacs_fe.setting
(
  setting_id SERIAL,
  setting_key VARCHAR(100) NOT NULL,
  setting_value VARCHAR(100),
  PRIMARY KEY (setting_id)
);


CREATE TABLE dacs_fe.user
(
  user_id SERIAL,
  department_id INTEGER NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  email VARCHAR(100),
  gender VARCHAR(100),
  birth_date TIMESTAMP NOT NULL DEFAULT NOW(),
  government_identification VARCHAR(100),
  government_identification_type VARCHAR(100),
  username VARCHAR(100),
  hashed_password VARCHAR(100),
  active BOOLEAN DEFAULT TRUE,
  locked BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (user_id),
  FOREIGN KEY (department_id) REFERENCES dacs_fe.department (department_id)
);


CREATE TABLE dacs_fe.group_permission
(
  group_permission_id SERIAL,
  user_group_id INTEGER NOT NULL,
  permission_id INTEGER NOT NULL,
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (group_permission_id),
  FOREIGN KEY (user_group_id) REFERENCES dacs_fe.user_group (user_group_id),
  FOREIGN KEY (permission_id) REFERENCES dacs_fe.permission (permission_id)
);


CREATE TABLE dacs_fe.account
(
  account_id SERIAL,
  department_id INTEGER NOT NULL,
  currency_id INTEGER NOT NULL,
  name VARCHAR(100) NOT NULL,
  code VARCHAR(100),
  description VARCHAR(512),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (account_id),
  FOREIGN KEY (department_id) REFERENCES dacs_fe.department (department_id),
  FOREIGN KEY (currency_id) REFERENCES dacs_fe.currency (currency_id)
);


CREATE TABLE dacs_fe.category_definition
(
  category_definition_id SERIAL,
  name VARCHAR(100) NOT NULL,
  code VARCHAR(100),
  description VARCHAR(512),
  budget DECIMAL(10) NOT NULL,
  active BOOLEAN DEFAULT TRUE,
  frequency_id INTEGER NOT NULL,
  PRIMARY KEY (category_definition_id),
  FOREIGN KEY (frequency_id) REFERENCES dacs_fe.frequency (frequency_id)
);


CREATE TABLE dacs_fe.category
(
  category_id SERIAL,
  category_definition_id INTEGER NOT NULL,
  budget DECIMAL(10) NOT NULL,
  valid_from TIMESTAMP NOT NULL DEFAULT NOW(),
  valid_until TIMESTAMP NOT NULL,
  notes VARCHAR(100),
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (category_id),
  FOREIGN KEY (category_definition_id) REFERENCES dacs_fe.category_definition (category_definition_id)
);


CREATE TABLE dacs_fe.recurrent_transaction
(
  recurrent_transaction_id SERIAL,
  account_id INTEGER NOT NULL,
  category_definition_id INTEGER NOT NULL,
  title VARCHAR(100) NOT NULL,
  expenditure BOOLEAN NOT NULL,
  amount DECIMAL(10) NOT NULL,
  frequency_id INTEGER NOT NULL,
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (recurrent_transaction_id),
  FOREIGN KEY (account_id) REFERENCES dacs_fe.account (account_id),
  FOREIGN KEY (category_definition_id) REFERENCES dacs_fe.category_definition (category_definition_id),
  FOREIGN KEY (frequency_id) REFERENCES dacs_fe.frequency (frequency_id)
);


CREATE TABLE dacs_fe.transaction
(
  transaction_id SERIAL,
  account_id INTEGER NOT NULL,
  category_id INTEGER NOT NULL,
  title VARCHAR(100) NOT NULL,
  comment VARCHAR(100),
  expenditure BOOLEAN NOT NULL,
  amount DECIMAL(10) NOT NULL,
  occurrence TIMESTAMP NOT NULL DEFAULT NOW(),
  recurrent_transaction_id INTEGER NULL,
  active BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (transaction_id),
  FOREIGN KEY (account_id) REFERENCES dacs_fe.account (account_id),
  FOREIGN KEY (category_id) REFERENCES dacs_fe.category (category_id),
  FOREIGN KEY (recurrent_transaction_id) REFERENCES dacs_fe.recurrent_transaction (recurrent_transaction_id)
);

CREATE TABLE dacs_fe.attachment
(
  attachment_id SERIAL,
  transaction_id INTEGER NOT NULL,
  name VARCHAR(100),
  PRIMARY KEY (attachment_id),
  FOREIGN KEY (transaction_id) REFERENCES dacs_fe.transaction (transaction_id)
);

CREATE TABLE dacs_fe.audit_event
(
  audit_event_id SERIAL,
  user_id INTEGER NOT NULL,
  PRIMARY KEY (audit_event_id)
);


CREATE TABLE dacs_fe.audit_log
(
  audit_log_id SERIAL,
  audit_event_id INTEGER NOT NULL,
  record_id INTEGER NOT NULL,
  record_type VARCHAR(100),
  PRIMARY KEY (audit_log_id),
  FOREIGN KEY (audit_event_id) REFERENCES dacs_fe.audit_event (audit_event_id)
);


CREATE TABLE dacs_fe.audit_detail
(
  audit_detail_id SERIAL,
  audit_log_id INTEGER NOT NULL,
  attribute_name VARCHAR(100),
  previous_value VARCHAR(100),
  new_value VARCHAR(100),
  attribute_type VARCHAR(100),
  PRIMARY KEY (audit_detail_id),
  FOREIGN KEY (audit_log_id) REFERENCES dacs_fe.audit_log (audit_log_id)
);
