WITH frequency AS ( 
    INSERT INTO dacs_fe.frequency (period_length, valid_from, valid_until, offset_from_start)
    VALUES (3, '2020-01-01', '2020-11-01', 0) 
    RETURNING frequency_id AS id
)
INSERT INTO dacs_fe.category_definition
    (name, code, description, budget, frequency_id)
SELECT
    'Food',
    'FOO',
    'Budget intented for food purchase for the office',
    50000,
    frequency.id
FROM frequency;

WITH frequency AS ( 
    INSERT INTO dacs_fe.frequency (period_length, valid_from, valid_until, offset_from_start)
    VALUES (2, '2020-06-01', '2020-06-30', 0) 
    RETURNING frequency_id AS id
)
INSERT INTO dacs_fe.category_definition
    (name, code, description, budget, frequency_id)
SELECT
    'Fun',
    'FU',
    'Budget intented for have fun',
    5000,
    frequency.id
FROM frequency;