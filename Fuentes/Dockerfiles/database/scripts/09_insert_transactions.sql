
INSERT INTO dacs_fe.transaction 
    (
        account_id,
        category_id,
        title,
        comment,
        expenditure, 
        amount,
        occurrence,
        recurrent_transaction_id
    )
VALUES 
     (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-03-01') AND (valid_until >= '2020-03-01')),
        'Office equipment',
        'Laptop, desk, printer',
        true,
        3500,
        '2020-03-01',
        null
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-03-01') AND (valid_until >= '2020-03-01')),
        'Comida',
        'Comida del mes 3',
        true,
        2000,
        '2020-03-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-04-01') AND (valid_until >= '2020-04-01')),
        'Comida',
        'Comida del mes 4',
        true,
        2000,
        '2020-04-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-05-01') AND (valid_until >= '2020-05-01')),
        'Comida',
        'Comida del mes 5',
        true,
        2000,
        '2020-05-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-06-01') AND (valid_until >= '2020-06-01')),
        'Comida',
        'Comida del mes 6',
        true,
        2000,
        '2020-06-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-07-01') AND (valid_until >= '2020-07-01')),
        'Comida',
        'Comida del mes 7',
        true,
        2000,
        '2020-07-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Caja chica'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=1 AND (valid_from <= '2020-08-01') AND (valid_until >= '2020-08-01')),
        'Comida',
        'Comida del mes 8',
        true,
        2000,
        '2020-08-01',
        (SELECT recurrent_transaction_id FROM dacs_fe.recurrent_transaction WHERE title='Comida mensual')
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Viaticos'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=2 AND (valid_from <= '2020-06-03') AND (valid_until >= '2020-06-03')),
        'Juegos',
        'Juegos de la primera semana de junio',
        true,
        800,
        '2020-06-03',
        null
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Viaticos'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=2 AND (valid_from <= '2020-06-12') AND (valid_until >= '2020-06-12')),
        'Juegos',
        'Juegos de la segunda semana de junio',
        true,
        800,
        '2020-06-12',
        null
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Viaticos'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=2 AND (valid_from <= '2020-06-16') AND (valid_until >= '2020-06-16')),
        'Juegos',
        'Juegos de la tercera semana de junio',
        true,
        800,
        '2020-06-16',
        null
    ),
    (
        (SELECT account_id FROM dacs_fe.account WHERE name='Viaticos'),
        (SELECT category_id FROM dacs_fe.category WHERE category_definition_id=2 AND (valid_from <= '2020-06-25') AND (valid_until >= '2020-06-25')),
        'Juegos',
        'Juegos de la cuarta semana de junio',
        true,
        800,
        '2020-06-25',
        null
    );