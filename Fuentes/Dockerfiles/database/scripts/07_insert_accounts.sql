

INSERT INTO dacs_fe.account(department_id, currency_id, name, code, description)
VALUES 
	(1, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Sueldos', 'SU', 'Cuenta para pagar los sueldos'),
	(1, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Proveedores', 'PR', 'Cuenta para pagar a los proveedores'),
	(2, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Caja chica', 'CC', 'Cuenta para pequeños gastos'),
	(2, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Comedor', 'CO', 'Cuenta para pagar a gastos de comida'),
	(3, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Equipamiento', 'EQ', 'Cuenta para pagar gastos de equipamiento'),
	(4, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Publicidad', 'PU', 'Cuenta para pagar gastos de publicidad'),
	(4, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Web', 'WE', 'Cuenta para pagar gastos de sitio web'),
	(5, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Viaticos', 'VI', 'Cuenta para pagar viaticos'),
	(1, (SELECT c.currency_id FROM dacs_fe.currency c WHERE c.iso_code = 'USD')  , 'Pago de Clientes', 'PC', 'Pago de los clientes de la organizacion');

