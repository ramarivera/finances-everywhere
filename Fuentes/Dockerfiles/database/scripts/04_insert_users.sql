INSERT INTO dacs_fe.user
    (
    department_id,
    username,
    first_name,
    last_name,
    email,
    gender,
    government_identification,
    government_identification_type
    )
VALUES
    (
        (SELECT department_id
        FROM dacs_fe.department d
        WHERE d.name = 'Finance'),
        'agusmane',
        'Agustina',
        'Mannise',
        'agusmn95@gmail.com',
        'female',
        '46182864',
        'CI'
    ),
    (
        (SELECT department_id
        from dacs_fe.department d
        WHERE d.name = 'IT'),
        'ramarivera',
        'Ramiro',
        'Rivera',
        'ramarivera@gmail.com',
        'male',
        '34568795',
        'DNI'
    ),
    (
        (SELECT department_id
        from dacs_fe.department d
        WHERE d.name = 'Production'),
        'martinnarrua',
        'Martín',
        'Arrúa',
        'martinnarrua94@gmail.com',
        'male',
        '36879684',
        'DNI'
    ),
    (
        (SELECT department_id
        from dacs_fe.department d
        WHERE d.name = 'HR'),
        'fermannise',
        'Fernanda',
        'Mannise',
        'fermannise1831@gmail.com',
        'female',
        '45677923',
        'CI'
    ),
    (
        (SELECT department_id
        from dacs_fe.department d
        WHERE d.name = 'Marketing'),
        'lucask',
        'Lucas',
        'Kloster',
        'lucaskloster@gmail.com',
        'male',
        '30254789',
        'DNI'
    );