
INSERT INTO dacs_fe.category 
    (
        category_definition_id,
        budget,
        valid_from,
        valid_until,
        notes
    )
VALUES 
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-01-01',
        '2020-01-31',
        'Food for JAN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-02-01',
        '2020-02-28',
        'Food for FEB 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-03-01',
        '2020-03-31',
        'Food for MAR 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-04-01',
        '2020-04-30',
        'Food for APR 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-05-01',
        '2020-05-31',
        'Food for MAY 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-06-01',
        '2020-06-30', 
        'Food for JUN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-07-01',
        '2020-07-31', 
        'Food for JUL 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-08-01',
        '2020-08-31', 
        'Food for AUG 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-09-01',
        '2020-09-30', 
        'Food for SET 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-10-01',
        '2020-10-31', 
        'Food for OCT 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FOO'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FOO'),
        '2020-01-01',
        '2020-01-30', 
        'Food for NOV 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FU'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FU'),
        '2020-06-01',
        '2020-06-07',
        'Fun for first week JUN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FU'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FU'),
        '2020-06-08',
        '2020-06-14',
        'Fun for second week JUN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FU'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FU'),
        '2020-06-15',
        '2020-06-21',
        'Fun for third week JUN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FU'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FU'),
        '2020-06-22',
        '2020-06-28',
        'Fun for fourth week JUN 20'
    ),
    (
        (SELECT category_definition_id FROM dacs_fe.category_definition WHERE code='FU'),
        (SELECT budget FROM dacs_fe.category_definition WHERE code='FU'),
        '2020-06-29',
        '2020-06-30',
        'Fun for fifth week JUN 20'
    );
