
INSERT INTO dacs_fe.setting
    (setting_key, setting_value)
VALUES
    (
        'default-currency',
        (SELECT currency_id
        FROM dacs_fe.currency c
        WHERE c.iso_code = 'USD')
);