using System;
using System.Collections.Generic;

namespace Servidor.Models
{
    public class Category : BaseEntity
    {
        public Category()
        {
            Transactions = new List<Transaction>();
        }

        public decimal Budget { get; set; }
        public string Notes { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public bool Active { get; set; }
        public CategoryDefinition CategoryDefinition { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }
}