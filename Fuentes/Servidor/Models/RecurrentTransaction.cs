using System;
using System.Collections.Generic;

namespace Servidor.Models
{
    public class RecurrentTransaction : BaseEntity
    {
        public RecurrentTransaction()
        {
            Transactions = new List<Transaction>();
        }

        public string Title { get; set; }
        public bool Expenditure { get; set; }
        public decimal Amount { get; set; }
        public bool Active { get; set; }
        public Account Account { get; set; }
        public CategoryDefinition CategoryDefinition { get; set; }
        public Frequency Frequency { get; set; }
        public IList<Transaction> Transactions { get; set; }
        public void AddTransaction(Transaction transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            Transactions.Add(transaction);
            //category.CategoryDefinition = this;
        }

    }
}