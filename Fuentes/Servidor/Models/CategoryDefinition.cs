using System;
using System.Collections.Generic;

namespace Servidor.Models
{
    public class CategoryDefinition : BaseEntity
    {
        public CategoryDefinition()
        {
            Categories = new List<Category>();
            RecurrentTransactions = new List<RecurrentTransaction>();
        }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public bool Active { get; set; }
        public Frequency Frequency { get; set; }
        public IList<Category> Categories { get; set; }
        public IList<RecurrentTransaction> RecurrentTransactions { get; set; }

        public void AddCategory(Category category)
        {
            if (category == null) throw new ArgumentNullException(nameof(category));
            Categories.Add(category);
            category.CategoryDefinition = this;
        }
    }
}