using System;

namespace Servidor.Models
{
    public class User : BaseEntity
    {
        public Department Department { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string GovernmentIdentification { get; set; }
        public string GovernmentIdentificationType { get; set; }
        public string Username { get; set; }
        public bool Active { get; set; }
        public bool Locked { get; set; }
    }
}