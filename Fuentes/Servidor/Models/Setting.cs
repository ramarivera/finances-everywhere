namespace Servidor.Models
{
    public class Setting : BaseEntity
    {
        public string SettingKey { get; set; }
        public string SettingValue { get; set; }
    }
}