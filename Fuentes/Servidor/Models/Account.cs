using System.Collections.Generic;

namespace Servidor.Models
{
    public class Account : BaseEntity
    {
        public Account()
        {
            Transactions = new List<Transaction>();
            RecurrentTransactions = new List<RecurrentTransaction>();
        }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public Department Department { get; set; }
        public Currency Currency { get; set; }
        public IList<Transaction> Transactions { get; set; }
        public IList<RecurrentTransaction> RecurrentTransactions { get; set; }
    }
}