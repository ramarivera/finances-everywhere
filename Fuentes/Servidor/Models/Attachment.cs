namespace Servidor.Models
{
    public class Attachment : BaseEntity
    {
        public string Name { get; set; }

        public Transaction Transaction { get; set; }

        // Ef Core one on one relation issues
        public long TransactionId { get; set; }
    }
}