using System;
using System.Collections.Generic;

namespace Servidor.Models.Reports
{
    public class ReportFilter
    {
        public ReportFilter() 
        {
            AccountIds = new List<long>();
            CategoryDefinitionIds = new List<long>();    
        }

        public DateTime? From { get; set; }
        public DateTime? Until { get; set; }
        public IList<long> AccountIds { get; set; }
        public IList<long> CategoryDefinitionIds { get; set; }
        public long? CurrencyId { get; set; }
    }
}