using System;
using Servidor.DataTransfer;

namespace Servidor.Models.Reports
{
    public class ReportTransaction
    {
        public long TransactionId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public decimal Amount { get; set; }
        public bool Expenditure { get; set; }
        public DateTime Occurrence {get; set;}
        public ValueAndDescriptionDTO CategoryDefinition { get; set; }
        public ValueAndDescriptionDTO Account { get; set; }
    }
}