using System.Collections.Generic;

namespace Servidor.Models.Reports
{
    public class ReportResult
    {
        public ReportResult()
        {
            ReportTransactions = new List<ReportTransaction>();
        }

        public IList<ReportTransaction> ReportTransactions { get; set; }
        public TransactionsSummary Summary { get; set; }
    }
}