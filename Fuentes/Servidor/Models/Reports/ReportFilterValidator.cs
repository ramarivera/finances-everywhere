using System;
using FluentValidation;

namespace Servidor.Models.Reports
{
    public class ReportFilterValidator : AbstractValidator<ReportFilter>
    {
        public ReportFilterValidator()
        {
            RuleFor(x => x.From)
                .LessThanOrEqualTo(DateTime.Now)
                .LessThanOrEqualTo(x => x.Until);

            RuleFor(ReportFilter => ReportFilter.Until)
                .LessThanOrEqualTo(DateTime.Now)
                .GreaterThanOrEqualTo(x => x.From);
        }
    }
}