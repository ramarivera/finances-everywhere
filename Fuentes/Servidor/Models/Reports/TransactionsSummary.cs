namespace Servidor.Models.Reports
{
    public class TransactionsSummary
    {
        public decimal Income { get; set; }
        public decimal Expense { get; set; }
        public decimal Difference { get; set; }
        public int TransactionsCount { get; set; }
    }
}