using System.Collections.Generic;
using Servidor.Models.Enums;

namespace Servidor.Models
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}