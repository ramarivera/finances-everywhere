using System.Collections.Generic;

namespace Servidor.Models
{
    public class Department : BaseEntity
    {
        public Department()
        {
            Accounts = new List<Account>();
            Users = new List<User>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public IList<Account> Accounts { get; set; }
        public IList<User> Users { get; set; }
    }
}