using System.Collections.Generic;

namespace Servidor.Models.Enums
{
    public static class PeriodExtensions
    {
        private static readonly IDictionary<Period, int> DaysByPeriod;

        static PeriodExtensions()
        {
            DaysByPeriod = new Dictionary<Period, int>() {
                { Period.Daily, 1},
                { Period.Weekly, 7},
                { Period.Monthly,30},
                { Period.Bimonthly, 60},
                { Period.FourMonthly, 120},
                { Period.Annual, 360},
            };
        }

        public static int ToDays(this Period period)
        {
            return DaysByPeriod[period];
        }
    }
}