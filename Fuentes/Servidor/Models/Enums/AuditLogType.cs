namespace Servidor.Models.Enums
{
    public enum AuditLogType
    {
        Creation = 1,
        Deletion = 2,
        Update = 3,
    }
}