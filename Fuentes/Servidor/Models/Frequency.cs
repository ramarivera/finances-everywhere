using System;
using System.Collections.Generic;
using Servidor.Models.Enums;

namespace Servidor.Models
{
    public class Frequency : BaseEntity
    {
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public int Offset { get; set; }
        public Period PeriodLength { get; set; }
    }
}