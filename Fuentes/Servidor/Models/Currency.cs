using System.Collections.Generic;

namespace Servidor.Models
{
    public class Currency : BaseEntity
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string IsoCode { get; set; }
        public bool Active { get; set; }
    }
}