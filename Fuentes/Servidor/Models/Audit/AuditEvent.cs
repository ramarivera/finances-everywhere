using System;
using System.Collections.Generic;

namespace Servidor.Models.Audit
{
    public class AuditEvent : BaseEntity
    {
        public AuditEvent()
        {
            AuditLogs = new List<AuditLog>();
        }

        public User User { get; set; }
        public DateTime TimeStamp { get; set; }
        public IList<AuditLog> AuditLogs { get; set; }
    }
}