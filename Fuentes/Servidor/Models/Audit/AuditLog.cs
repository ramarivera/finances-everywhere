using System.Collections.Generic;
using Servidor.Models.Enums;

namespace Servidor.Models.Audit
{
    public class AuditLog : BaseEntity
    {
        public AuditLog()
        {
            AuditDetails = new List<AuditDetail>();
        }

        public AuditEvent AuditEvent { get; set; }
        public AuditLogType Type { get; set; }
        public long RecordId { get; set; }
        public string RecordType { get; set; }
        public IList<AuditDetail> AuditDetails { get; set; }
    }
}