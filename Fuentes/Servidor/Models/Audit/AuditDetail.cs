namespace Servidor.Models.Audit
{
    public class AuditDetail : BaseEntity
    {
        public string AttributeName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string AttributeType { get; set; }
        public AuditLog AuditLog { get; set; }
    }
}