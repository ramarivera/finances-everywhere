using System;
using System.Collections.Generic;

namespace Servidor.Models
{
    public class Transaction : BaseEntity
    {
        public string Title { get; set; }

        public string Comment { get; set; }

        public DateTime Occurrence { get; set; }

        public bool Expenditure { get; set; }

        public decimal Amount { get; set; }

        public bool Active { get; set; }

        public Account Account { get; set; }

        public Category Category { get; set; }

        public RecurrentTransaction RecurrentTransaction { get; set; }

        public Attachment Attachment { get; set; }

        public bool IsEffective()
        {
            return (Occurrence <= DateTime.Today);
        }
    }
}