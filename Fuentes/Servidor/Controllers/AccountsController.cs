using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.DataTransfer;
using AutoMapper;
using Servidor.Services.Interfaces;
using Servidor.Models.Reports;
using Servidor.Services;

namespace Servidor.Controllers
{
    [Route("api/accounts")]
    [ApiController]
    public class AccountsController : BaseApiController
    {
        private readonly IAccountsService _accountsService;
        public AccountsController(
            IMapper mapper,
            IAccountsService accountsService)
            : base(mapper)
        {
            _accountsService = accountsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AccountHeaderDTO>> GetAll()
        {
            var accounts = _accountsService.GetAll();
            var result = Mapper.Map<IList<AccountHeaderDTO>>(accounts);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetAccount")]
        public ActionResult<AccountDTO> GetById(long id)
        {
            var item = _accountsService.GetById(id);
            var result = Mapper.Map<AccountDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<AccountHeaderDTO> Create([FromBody] AccountCreationDTO accountCreationData)
        {
            if (accountCreationData == null)
            {
                return BadRequest();
            }

            var account = _accountsService.Create(accountCreationData);

            return GetHeaderById(account.Id);
        }

        [HttpPut("{id}")]
        public ActionResult<AccountHeaderDTO> Update(long id, [FromBody] AccountUpdateDTO accountUpdateData)
        {
            if (accountUpdateData == null)
            {
                return BadRequest();
            }

            var account = _accountsService.Update(id, accountUpdateData);

            return GetHeaderById(account.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _accountsService.Delete(id);

            return new NoContentResult();
        }

        [HttpGet("{id}/transactions")]
        public ActionResult<IList<TransactionHeaderDTO>> GetTransactionsByAccountId(long id)
        {
            var transactions = _accountsService.GetTransactionsByAccountId(id);
            if (transactions == null)
            {
                return NotFound();
            }

            var result = Mapper.Map<IList<TransactionHeaderDTO>>(transactions);

            return Ok(result);
        }

        [HttpGet("{id}/recurrent-transactions")]
        public ActionResult<IList<RecurrentTransactionHeaderDTO>> GetRecurrentTransactionsByAccountId(long id)
        {
            var recurrentTransactions = _accountsService.GetRecurrentTransactionsByAccountId(id);
            if (recurrentTransactions == null)
            {
                return NotFound();
            }

            var result = Mapper.Map<IList<RecurrentTransactionHeaderDTO>>(recurrentTransactions);

            return Ok(result);
        }

        private AccountHeaderDTO GetHeaderById(long id)
        {
            var account = _accountsService.GetById(id);
            
            return Mapper.Map<AccountHeaderDTO>(account);
        }
    }
}