using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.Services;
using AutoMapper;
using Servidor.DataTransfer;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [ApiController]
    public class BaseApiController : Controller
    {
        private readonly IMapper _mapper;
        public BaseApiController(IMapper mapper)
        {
            _mapper = mapper;
        }

        protected IMapper Mapper => _mapper;
    }
}