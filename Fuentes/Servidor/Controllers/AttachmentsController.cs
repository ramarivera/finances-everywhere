using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.DataTransfer;
using Servidor.Services;
using AutoMapper;

namespace Servidor.Controllers
{
    [Route("api/attachments")]
    [ApiController]
    public class AttachmentsController : Controller
    {
        IGenericService<Attachment> _service;
        private readonly IMapper _mapper;
        public AttachmentsController(IGenericService<Attachment> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IList<Attachment>> GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpGet("{id}", Name = "GetAttachment")]
        public ActionResult<Attachment> GetById(long id)
        {
            var item = _service.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Attachment item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.Add(item);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Attachment item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.Update(item);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return new NoContentResult();
        }
    }
}