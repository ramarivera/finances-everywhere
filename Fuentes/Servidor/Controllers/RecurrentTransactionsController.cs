using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using Servidor.DataTransfer;
using Servidor.Services.Interfaces;
using AutoMapper;

namespace Servidor.Controllers
{
    [Route("api/recurrent-transactions")]
    [ApiController]
    public class RecurrentTransactionsController : BaseApiController
    {
        private readonly IRecurrentTransactionsService _recurrentTransactionsService;
        private readonly IAccountsService _accountsService;
        private readonly ICategoryDefinitionsService _categoriesService;
        private readonly IMapper _mapper;
        public RecurrentTransactionsController(
            IMapper mapper,
            IRecurrentTransactionsService recurrentTransactionsService,
            IAccountsService accountsService,
            ICategoryDefinitionsService categoriesService)
            : base(mapper)
        {
            _recurrentTransactionsService = recurrentTransactionsService;
            _accountsService = accountsService;
            _categoriesService = categoriesService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IList<RecurrentTransactionHeaderDTO>> GetAll()
        {
            var recurrentTransactions = _recurrentTransactionsService.GetAll();
            var result = _mapper.Map<IList<RecurrentTransactionHeaderDTO>>(recurrentTransactions);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetRecurrentTransaction")]
        public ActionResult<RecurrentTransactionDTO> GetById(long id)
        {
            var item = _recurrentTransactionsService.GetById(id);
            var result = _mapper.Map<RecurrentTransactionDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<RecurrentTransactionHeaderDTO> Create([FromBody] RecurrentTransactionCreationDTO recurrentTransactionCreationData)
        {
            if (recurrentTransactionCreationData == null)
            {
                return BadRequest();
            }

            var recurrentTransaction = _recurrentTransactionsService.Create(recurrentTransactionCreationData);

            return GetHeaderById(recurrentTransaction.Id);
        }

        [HttpPut("{id}")]
        public ActionResult<RecurrentTransactionHeaderDTO> Update(long id, [FromBody] RecurrentTransactionUpdateDTO recurrentTransactionUpdateData)
        {
            if (recurrentTransactionUpdateData == null)
            {
                return BadRequest();
            }

            var recurrentTransaction = _recurrentTransactionsService.Update(id, recurrentTransactionUpdateData);

            return GetHeaderById(recurrentTransaction.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _recurrentTransactionsService.Delete(id);
            return new NoContentResult();
        }

        private RecurrentTransactionHeaderDTO GetHeaderById(long id)
        {
            var recurrentTransaction = _recurrentTransactionsService.GetById(id);
            return _mapper.Map<RecurrentTransactionHeaderDTO>(recurrentTransaction);
        }
    }
}