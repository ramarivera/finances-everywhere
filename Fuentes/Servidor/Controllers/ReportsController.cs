using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Services.Interfaces;
using Servidor.Models.Reports;
using Servidor.DataTransfer;
using Servidor.Utilities.WebApi;

namespace Servidor.Controllers
{
    [Route("api/reports")]
    [ApiController]
    public class ReportsController : Controller
    {
        ITransactionsService _transactionsService;
        IAccountsService _accountsService;
        ICategoryDefinitionsService _categoryDefinitionsService;
        IReportsService _reportsService;

        public ReportsController(
            ITransactionsService transactionsService,
            IAccountsService accountsService,
            ICategoryDefinitionsService categoryDefinitionsService,
            IReportsService reportsService)
        {
            _transactionsService = transactionsService;
            _accountsService = accountsService;
            _categoryDefinitionsService = categoryDefinitionsService;
            _reportsService = reportsService;
        }

        [HttpPost("details")]
        public IEnumerable<ReportTransaction> TransactionsDetails(ReportFilter input)
        {
            var reportTransactions = _reportsService.FindReportTransactionsMatchingFilter(input);
           
            return reportTransactions;
        }

        [HttpPost("summary")]
        public TransactionsSummary TransactionsSummary(ReportFilter input)
        {
            var result = _reportsService.GenerateTransactionsSummaryMatchingFilter(input);
           
            return result;
        }

        [HttpPost("download")]
        public IActionResult TransactionsReportDownload(ReportFilter input)
        {
            byte[] stream = _reportsService.BuildTransactionsReportExport(input);

            return File(stream, "application/octet-stream", "Transaction Report.xlsx");
        }
    }
}