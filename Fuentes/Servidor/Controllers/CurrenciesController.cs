using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.DataTransfer;
using AutoMapper;
using Servidor.Services.Interfaces;
using Servidor.Services;

namespace Servidor.Controllers
{
    [Route("api/currencies")]
    [ApiController]
    public class CurrenciesController : Controller
    {
        private readonly ICurrenciesService _service;
        private readonly IMapper _mapper;

        public CurrenciesController(ICurrenciesService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CurrencyDTO>> GetAll()
        {
            var currencies = _service.GetAll();
            var result = _mapper.Map<IEnumerable<CurrencyDTO>>(currencies);
            return Ok(result);
        }

        [HttpGet("default")]
        public ActionResult<CurrencyDTO> GetDefaultCurrency() 
        {
            var defaultCurrency = _service.GetDefaultCurrency();
            var result = _mapper.Map<CurrencyDTO>(defaultCurrency);
            return Ok(result);
        }

    }
}