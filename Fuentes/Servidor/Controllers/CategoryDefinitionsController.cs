using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.DataTransfer;
using AutoMapper;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/category-definitions")]
    [ApiController]
    public class CategoryDefinitionsController : BaseApiController
    {
        private readonly ICategoryDefinitionsService _service;
        public CategoryDefinitionsController(
            IMapper mapper,
            ICategoryDefinitionsService service)
            : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CategoryDefinitionHeaderDTO>> GetAll()
        {
            var categories = _service.GetAll();
            var result = Mapper.Map<IList<CategoryDefinitionHeaderDTO>>(categories);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetCategory")]
        public ActionResult<CategoryDefinitionDTO> GetById(long id)
        {
            var item = _service.GetById(id);
            var result = Mapper.Map<CategoryDefinitionDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<CategoryDefinitionDTO> Create([FromBody] CategoryDefinitionCreationDTO categoryCreationData)
        {
            if (categoryCreationData == null)
            {
                return BadRequest();
            }

            var category = _service.Create(categoryCreationData);

            return GetDTOById(category.Id);
        }

        [HttpPut("{id}")]
        public ActionResult<CategoryDefinitionDTO> Update(long id, [FromBody] CategoryDefinitionUpdateDTO categoryDefinitionUpdateData)
        {
            if (categoryDefinitionUpdateData == null)
            {
                return BadRequest();
            }

            var category = _service.Update(id, categoryDefinitionUpdateData);

            return GetDTOById(category.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return new NoContentResult();
        }

        private CategoryDefinitionDTO GetDTOById(long id)
        {
            var categoryDefinition = _service.GetById(id);
            return Mapper.Map<CategoryDefinitionDTO>(categoryDefinition);
        }
    }
}