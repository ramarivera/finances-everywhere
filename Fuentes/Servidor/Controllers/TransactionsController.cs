using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.DataTransfer;
using Servidor.Services;
using AutoMapper;
using Servidor.Services.Interfaces;
using Servidor.Models.Reports;
using System;

namespace Servidor.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : BaseApiController
    {
        private readonly ITransactionsService _transactionsService;

        private readonly IAttachmentsService _attachmentsService;

        public TransactionsController(
            IMapper mapper,
            ITransactionsService transactionService,
            IAttachmentsService attachmentsService)
            : base(mapper)
        {
            _transactionsService = transactionService;
            _attachmentsService = attachmentsService;
        }

        [HttpGet]
        public ActionResult<IList<TransactionHeaderDTO>> GetAll()
        {
            var transactions = _transactionsService.GetAll();
            var result = Mapper.Map<IList<TransactionHeaderDTO>>(transactions);
            return Ok(result);
        }

        [HttpGet("efective")]
        public ActionResult<IList<TransactionHeaderDTO>> GetEfectiveTransactions()
        {
            var transactions = _transactionsService.GetEfectiveTransactions();
            var result = Mapper.Map<IList<TransactionHeaderDTO>>(transactions);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetTransaction")]
        public ActionResult<TransactionDTO> GetById(long id)
        {
            var item = _transactionsService.GetById(id);
            var result = Mapper.Map<TransactionDTO>(item);

            if (item.Attachment != null) {
                var contents = _attachmentsService.GetAttachmentContents(item.Attachment);
                result.Base64AttachmentImage = Convert.ToBase64String(contents);
            }

            return Ok(result);
        }

        [HttpPost]
        public ActionResult<TransactionHeaderDTO> Create([FromBody] TransactionCreationDTO transactionCreationData)
        {
            if (transactionCreationData == null)
            {
                return BadRequest();
            }

            var transaction = _transactionsService.Create(transactionCreationData);

            return GetHeaderById(transaction.Id);
        }

        [HttpPut("{id}")]
        public ActionResult<TransactionHeaderDTO> Update(long id, [FromBody] TransactionUpdateDTO transactionUpdateData)
        {
            if (transactionUpdateData == null)
            {
                return BadRequest();
            }

            var transaction = _transactionsService.Update(id, transactionUpdateData);

            return GetHeaderById(transaction.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _transactionsService.Delete(id);
            return new NoContentResult();
        }

        private TransactionHeaderDTO GetHeaderById(long id)
        {
            var transaction = _transactionsService.GetById(id);
            return Mapper.Map<TransactionHeaderDTO>(transaction);
        }
    }
}