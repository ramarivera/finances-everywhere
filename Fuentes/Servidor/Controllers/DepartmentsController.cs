using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.Services;
using AutoMapper;
using Servidor.DataTransfer;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/departments")]
    [ApiController]
    public class DepartmentsController : BaseApiController
    {
        IDepartmentsService _service;
        public DepartmentsController(
            IMapper mapper,
            IDepartmentsService service)
            : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<IList<DepartmentHeaderDTO>> GetAll()
        {
            var departments = _service.GetAll();
            var result = Mapper.Map<IList<DepartmentHeaderDTO>>(departments);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetDepartment")]
        public ActionResult<DepartmentDTO> GetById(long id)
        {
            var item = _service.GetById(id);
            var result = Mapper.Map<DepartmentDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<DepartmentHeaderDTO> Create([FromBody] DepartmentCreationDTO DepartmentCreationData)
        {
            if (DepartmentCreationData == null)
            {
                return BadRequest();
            }

            var department = _service.Create(DepartmentCreationData);

            return GetHeaderById(department.Id);
        }

        [HttpPut("{id}")]
        public ActionResult<DepartmentHeaderDTO> Update(long id, [FromBody] DepartmentUpdateDTO departmentUpdateData)
        {
            if (departmentUpdateData == null)
            {
                return BadRequest();
            }

            var department =_service.Update(id, departmentUpdateData);

            return GetHeaderById(department.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return new NoContentResult();
        }

        [HttpGet("{id}/users")]
        public ActionResult<IList<UserHeaderDTO>> GetUsersByDepartmentId(long id)
        {
            var users = _service.GetUsersByDepartmentId(id);
            if (users == null)
            {
                return NotFound();
            }
            var result = Mapper.Map<IList<UserHeaderDTO>>(users);
            return Ok(result);
        }

        private DepartmentHeaderDTO GetHeaderById(long id)
        {
            var department = _service.GetById(id);
            return Mapper.Map<DepartmentHeaderDTO>(department);
        }
    }
}