using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using Servidor.DataTransfer;
using AutoMapper;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/settings")]
    public class SettingsController : Controller
    {
        private readonly ISettingsService _service;
        private readonly IMapper _mapper;
        public SettingsController(ISettingsService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var settings = _service.GetAll();
            var result = _mapper.Map<IList<SettingDTO>>(settings);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetSetting")]
        public IActionResult GetById(long id)
        {
            var item = _service.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            var result = _mapper.Map<SettingDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Setting item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.Add(item);
            return Ok();
        }

        // [HttpPut("{id}")]
        // public IActionResult Update(long id, [FromBody] JsonPatchDocument<Setting> item)
        // {
        //     if (item == null)
        //     {
        //         return BadRequest();
        //     }

        //     var settingToUpdate = _service.GetById(id);
        //     item.ApplyTo(settingToUpdate);

        //     _service.Update(settingToUpdate);
        //     return new NoContentResult();
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return new NoContentResult();
        }
    }
}