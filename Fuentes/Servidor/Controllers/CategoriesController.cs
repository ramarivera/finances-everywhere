using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.DataTransfer;
using AutoMapper;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : BaseApiController
    {
        private readonly ICategoriesService _categoriesService;
        
        public CategoriesController(
            IMapper mapper,
            ICategoriesService service)
            : base(mapper)
        {
            _categoriesService = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CategoryDTO>> GetAll()
        {
            var categories = _categoriesService.GetAll();
            var result = Mapper.Map<IList<CategoryDTO>>(categories);
            return Ok(result);
        }

        [HttpPut("{id}")]
        public ActionResult<CategoryDTO> Update(long id, [FromBody] CategoryUpdateDTO categoryUpdateData)
        {
            if (categoryUpdateData == null)
            {
                return BadRequest();
            }

            var category = _categoriesService.Update(id, categoryUpdateData);

            return GetHeaderById(category.Id);
        }

        private CategoryDTO GetHeaderById(long id)
        {
            var category = _categoriesService.GetById(id);
            return Mapper.Map<CategoryDTO>(category);
        }
    }
}