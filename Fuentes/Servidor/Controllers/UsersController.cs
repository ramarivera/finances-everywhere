using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Servidor.Models;
using System.Linq;
using Servidor.Persistence;
using Servidor.Services;
using AutoMapper;
using Servidor.DataTransfer;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/users")]
    public class UsersController : BaseApiController
    {
        private readonly IUsersService _service;
        
        public UsersController(
            IMapper mapper,
            IUsersService service)
            : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<IList<UserHeaderDTO>> GetAll()
        {
            var users = _service.GetAll();
            var result = Mapper.Map<IList<UserHeaderDTO>>(users);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<UserDTO> GetById(long id)
        {
            var item = _service.GetById(id);
            var result = Mapper.Map<UserDTO>(item);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<UserHeaderDTO> Create([FromBody] UserCreationDTO userCreationData)
        {
            if (userCreationData == null)
            {
                return BadRequest();
            }

            var user = _service.Create(userCreationData);

            return GetHeaderById(user.Id);;
        }

        [HttpPut("{id}")]
        public ActionResult<UserHeaderDTO> Update(long id, [FromBody] UserUpdateDTO userUpdateData)
        {
            if (userUpdateData == null)
            {
                return BadRequest();
            }

            var user = _service.Update(id, userUpdateData);

            return GetHeaderById(user.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return new NoContentResult();
        }

        private UserHeaderDTO GetHeaderById(long id)
        {
            var user = _service.GetById(id);
            return Mapper.Map<UserHeaderDTO>(user);
        }
    }
}