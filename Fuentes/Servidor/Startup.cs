﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services;
using Servidor.Services.Interfaces;
using Servidor.Utilities;
using Swashbuckle.AspNetCore.Swagger;

namespace Servidor
{
    public class Startup
    {

        private const string DEVELOPMENT_OR_STAGING_POLICY_NAME = "DevelopmentOrStaging";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<FinancesEverywhereContext>(options =>
                {
                    var connectionString = Configuration.GetConnectionString("FinancesEverywhereContext");
                    options.UseNpgsql(connectionString);
                });

            services.AddScoped<IAccountsService, AccountsService>();
            services.AddScoped<IAttachmentsService, AttachmentsService>();
            services.AddScoped<ICategoryDefinitionsService, CategoryDefinitionsService>();
            services.AddScoped<ICategoriesService, CategoriesService>();
            services.AddScoped<CategoriesService>();
            services.AddScoped<IDepartmentsService, DepartmentsService>();
            services.AddScoped<IRecurrentTransactionsService, RecurrentTransactionsService>();
            services.AddScoped<ISettingsService, SettingsService>();
            services.AddScoped<ICurrenciesService, CurrenciesService>();
            services.AddScoped<ITransactionsService, TransactionsService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IReportsService, ReportsService>();

            services.AddAutoMapper(typeof(Startup).Assembly);

            // Register named policy for development
            services.AddCors(options =>
            {
                options.AddPolicy(DEVELOPMENT_OR_STAGING_POLICY_NAME, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            services.AddControllers();

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "DACS 2017 FE API v1",
                    Version = "v1"
                });
            });

            services.Configure<AttachmentOptions>(Configuration.GetSection(AttachmentOptions.ConfigPath));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Read https://aregcode.com/blog/2019/dotnetcore-understanding-aspnet-endpoint-routing/
            app.UseRouting();

            if (env.IsDevelopment() || env.IsStaging())
            {
                app.UseDeveloperExceptionPage();

                // Allow all CORS from localhost when developing / deployed in amazon
                app.UseCors(DEVELOPMENT_OR_STAGING_POLICY_NAME);
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "dacs/swagger/{documentName}/swagger.json";
            });

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/dacs/swagger/v1/swagger.json", "DACS 2017 FE API v1");
                c.RoutePrefix = "dacs/swagger";
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
