using System.Configuration;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.Models;
using Servidor.Persistence.Mappings;

namespace Servidor.Persistence
{
    public class FinancesEverywhereContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryDefinition> CategoryDefinitions { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Frequency> Frequencies { get; set; }
        public DbSet<RecurrentTransaction> RecurrentTransactions { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<User> Users { get; set; }

        public FinancesEverywhereContext(DbContextOptions<FinancesEverywhereContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            AddEntityMappings(modelBuilder);
            OverrideConventions(modelBuilder);
        }

        private void OverrideConventions(ModelBuilder builder)
        {
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                // This should be later unhardcoded by taking info from startup and configuration files on that order.
                entity.SetSchema("dacs_fe");

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var foreignKey in entity.GetForeignKeys())
                {
                    foreignKey.SetConstraintName(foreignKey.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetName().ToSnakeCase());
                }
            }
        }

        private void AddEntityMappings(ModelBuilder builder)
        {
            // TODO: scan assemblies
            builder.ApplyConfiguration(new AccountMapping());
            builder.ApplyConfiguration(new AttachmentMapping());
            builder.ApplyConfiguration(new CategoryDefinitionMapping());
            builder.ApplyConfiguration(new CurrencyMapping());
            builder.ApplyConfiguration(new DepartmentMapping());
            builder.ApplyConfiguration(new FrequencyMapping());
            builder.ApplyConfiguration(new CategoryMapping());
            builder.ApplyConfiguration(new RecurrentTransactionMapping());
            builder.ApplyConfiguration(new SettingMapping());
            builder.ApplyConfiguration(new TransactionMapping());
            builder.ApplyConfiguration(new UserMapping());
        }
    }
}