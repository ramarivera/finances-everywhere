using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class SettingMapping : BaseEntityMapping<Setting>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.SettingKey)
                .IsRequired();

            Property(x => x.SettingValue)
                .IsRequired();
        }
    }
}