using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class RecurrentTransactionMapping : BaseEntityMapping<RecurrentTransaction>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Title)
                .IsRequired();

            Property(x => x.Expenditure)
                .IsRequired();

            Property(x => x.Amount)
                .IsRequired();

            Property(x => x.Active)
                .IsRequired();
            // QRAR: Is this required?

            HasOne(x => x.Frequency)
                .WithOne()
                .HasForeignKey<RecurrentTransaction>(CreateShadowKeyFor(x => x.Frequency));

            HasOne(x => x.Account)
                .WithMany(x => x.RecurrentTransactions)
                .HasForeignKey(CreateShadowKeyFor(x => x.Account));

            HasOne(x => x.CategoryDefinition)
                .WithMany(x => x.RecurrentTransactions)
                .HasForeignKey(CreateShadowKeyFor(x => x.CategoryDefinition));

            HasMany(x => x.Transactions);
        }
    }
}