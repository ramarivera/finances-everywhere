using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class AccountMapping : BaseEntityMapping<Account>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Name);
            // QRAR: is this required?

            Property(x => x.Code);
            // QRAR: is this required?

            Property(x => x.Description);
            // QRAR: is this required?

            Property(x => x.Active);
            // QRAR: Is this required?

            HasMany(x => x.Transactions);

            HasMany(x => x.RecurrentTransactions);

            HasOne(x => x.Department)
                .WithMany(x => x.Accounts)
                .HasForeignKey(CreateShadowKeyFor(x => x.Department));

            HasOne(x => x.Currency)
                .WithMany()
                .HasForeignKey(CreateShadowKeyFor(x => x.Currency));
        }
    }
}