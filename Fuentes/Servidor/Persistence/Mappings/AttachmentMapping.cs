using Servidor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Servidor.Persistence.Mappings
{
    public class AttachmentMapping : BaseEntityMapping<Attachment>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Name);

            HasOne(x => x.Transaction)
                .WithOne(x => x.Attachment)
                .HasForeignKey<Attachment>(x => x.TransactionId);
        }
    }
}