using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class CurrencyMapping : BaseEntityMapping<Currency>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Name);
            // QRAR: Is this required?

            Property(x => x.Symbol);
            // QRAR: Is this required?

            Property(x => x.IsoCode);
            // QRAR: Is this required?

            Property(x => x.Active);
            // QRAR: Is this required?
        }
    }
}