using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;
using Servidor.Utilities;

namespace Servidor.Persistence.Mappings
{
    public abstract class BaseEntityMapping<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : BaseEntity
    {
        protected EntityTypeBuilder<TEntity> builder;

        public void Configure(EntityTypeBuilder<TEntity> builder)
        {
            this.builder = builder;
            ConfigureEntity();
        }

        protected abstract void ConfigureEntity();

        protected EntityTypeBuilder<TEntity> ToTable(string tableName, string schemaName = null)
        {
            return builder.ToTable(tableName, schema: schemaName);
        }

        protected KeyBuilder HasKey(
            Expression<Func<TEntity, object>> keyExpression,
            string keyName)
        {
            return builder.HasKey(keyExpression).HasName(keyName);
        }

        protected virtual PropertyBuilder<TProperty> Property<TProperty>(
            Expression<Func<TEntity, TProperty>> propertyExpression,
            string columnName = null)
        {
            columnName = columnName ?? ReflectHelper<TEntity>.NameOf(propertyExpression);
            return builder.Property(propertyExpression)
                          .HasColumnName(columnName);
        }

        protected virtual ReferenceNavigationBuilder<TEntity, TRelatedEntity> HasOne<TRelatedEntity>(
            Expression<Func<TEntity, TRelatedEntity>> navigationExpression)
            where TRelatedEntity : class
        {
            return builder.HasOne(navigationExpression);
        }

        protected virtual CollectionNavigationBuilder<TEntity, TRelatedEntity> HasMany<TRelatedEntity>(
            Expression<Func<TEntity, IEnumerable<TRelatedEntity>>> navigationExpression)
            where TRelatedEntity : class
        {
            return builder.HasMany(navigationExpression);
        }

        protected EntityTypeBuilder<TEntity> ToDefaultTable()
        {
            return ToTable(ReflectHelper<TEntity>.Name, "dacs_fe");
        }

        protected void HasDefaultKey()
        {
            HasKey(x => x.Id, GetClassDefaultId());
            Property(x => x.Id, GetClassDefaultId());
        }

        protected virtual string CreateShadowKeyFor<TTarget>(
            Expression<Func<TEntity, TTarget>> propertyExpression,
            bool isNullable = false)
            where TTarget : class
        {
            string shadowKeyName = GetIdFor(propertyExpression);
            var propertyType = isNullable ? typeof(Nullable<long>) : typeof(long);
            builder.Property(propertyType, shadowKeyName);
            return shadowKeyName;
        }

        protected string GetIdFor<TTarget>(Expression<Func<TEntity, TTarget>> navigationExpression)
            where TTarget : class
        {
            return $"{ReflectHelper<TTarget>.Name}Id";
        }

        private string GetClassDefaultId()
        {
            return $"{ReflectHelper<TEntity>.Name}Id";
        }
    }
}