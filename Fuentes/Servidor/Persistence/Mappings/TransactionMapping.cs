using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class TransactionMapping : BaseEntityMapping<Transaction>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Title)
               .IsRequired();

            Property(x => x.Comment);
            // QRAR: Is this required?

            Property(x => x.Active)
                .IsRequired();

            Property(x => x.Occurrence)
                .IsRequired();

            Property(x => x.Expenditure)
                .IsRequired();

            Property(x => x.Amount)
                .IsRequired();

            HasOne(x => x.Account)
                .WithMany(x => x.Transactions)
                .HasForeignKey(CreateShadowKeyFor(x => x.Account));

            HasOne(x => x.Category)
                .WithMany(x => x.Transactions)
                .HasForeignKey(CreateShadowKeyFor(x => x.Category));

            HasOne(x => x.Attachment)
                .WithOne(x => x.Transaction)
                .IsRequired(false);

            HasOne (x => x.RecurrentTransaction)
                .WithMany(x => x.Transactions)
                .HasForeignKey(CreateShadowKeyFor(x => x.RecurrentTransaction, isNullable: true))
                .IsRequired(false);
        }
    }
}