using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class UserMapping : BaseEntityMapping<User>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Username)
                .IsRequired();

            Property(x => x.LastName)
                .IsRequired();

            Property(x => x.FirstName)
                .IsRequired();

            Property(x => x.Email)
                .IsRequired();

            Property(x => x.BirthDate)
                .IsRequired();

            Property(x => x.Gender)
                .IsRequired();

            Property(x => x.GovernmentIdentification)
                .IsRequired();

            Property(x => x.GovernmentIdentificationType)
                .IsRequired();

            Property(x => x.Active)
                .IsRequired()
                .HasDefaultValue(true);

            Property(x => x.Locked)
                .IsRequired()
                .HasDefaultValue(false);

            HasOne(x => x.Department)
                .WithMany(x => x.Users)
                .HasForeignKey(CreateShadowKeyFor(x => x.Department));
        }
    }
}