using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class DepartmentMapping : BaseEntityMapping<Department>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Name);
            // QRAR: Is this required?

            Property(x => x.Description);
            // QRAR: Is this required?

            Property(x => x.Active);
            // QRAR: Is this required?

            HasMany(x => x.Accounts);

            HasMany(x => x.Users);
        }
    }
}