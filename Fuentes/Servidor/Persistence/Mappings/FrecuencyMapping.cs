using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class FrequencyMapping : BaseEntityMapping<Frequency>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.ValidFrom)
                .IsRequired();

            Property(x => x.ValidUntil)
                .IsRequired();

            Property(x => x.Offset, "OffsetFromStart")
                .IsRequired();

            Property(x => x.PeriodLength)
                .IsRequired();
        }
    }
}