using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class CategoryDefinitionMapping : BaseEntityMapping<CategoryDefinition>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Name);
            // QRAR: Is this required?

            Property(x => x.Code);
            // QRAR: Is this required?

            Property(x => x.Description);
            // QRAR: Is this required?

            Property(x => x.Active);
            // QRAR: Is this required?

            Property(x => x.Budget)
                .IsRequired();

            HasOne(x => x.Frequency)
                .WithOne()
                .HasForeignKey<CategoryDefinition>(CreateShadowKeyFor(x => x.Frequency));

            HasMany(x => x.Categories);

            HasMany(x => x.RecurrentTransactions);
        }
    }
}