using Servidor.Models;

namespace Servidor.Persistence.Mappings
{
    public class CategoryMapping : BaseEntityMapping<Category>
    {
        protected override void ConfigureEntity()
        {
            ToDefaultTable();

            HasDefaultKey();

            Property(x => x.Notes);

            Property(x => x.Active)
                .IsRequired();

            Property(x => x.Budget)
                .IsRequired();

            Property(x => x.ValidFrom)
                .IsRequired();

            Property(x => x.ValidUntil)
                .IsRequired();

            HasOne(x => x.CategoryDefinition)
                .WithMany(x => x.Categories)
                .HasForeignKey(CreateShadowKeyFor(x => x.CategoryDefinition));

            HasMany(x => x.Transactions);
        }
    }
}