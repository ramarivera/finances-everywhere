using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Servidor.Utilities;

public static class EntityFrameworkCoreExtensions
{
    public static ReferenceCollectionBuilder<TEntity, TRelatedEntity> HasShadowForeignKey<TEntity, TRelatedEntity>(
        this ReferenceCollectionBuilder<TEntity, TRelatedEntity> input,
        string shadowForeignKeyName = null)
        where TEntity : class
        where TRelatedEntity : class
    {
        shadowForeignKeyName = shadowForeignKeyName ?? $"{ReflectHelper<TEntity>.Name}Id";
        return input.HasForeignKey(shadowForeignKeyName);
    }
}