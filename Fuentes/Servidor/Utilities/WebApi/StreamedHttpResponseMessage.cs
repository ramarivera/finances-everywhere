using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Servidor.Utilities.WebApi
{
    public class StreamedHttpResponseMessage : HttpResponseMessage
    {
        // https://www.iana.org/assignments/media-types/media-types.xhtml
        private const string MediaType = "application/octet-stream";

        // https://www.iana.org/assignments/cont-disp/cont-disp.xhtml
        private const string DispositionType = "attachment";

        public StreamedHttpResponseMessage(string fileName, byte[] fileContents)
            : base()
        {
            StatusCode = HttpStatusCode.OK;
            Content = BuildContent(fileName, fileContents);
        }

        private HttpContent BuildContent(string fileName, byte[] content)
        {
            var httpContent = new ByteArrayContent(content);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue(MediaType);
            httpContent.Headers.ContentDisposition = new ContentDispositionHeaderValue(DispositionType)
            {
                FileName = fileName
            };

            return httpContent;
        }
    }
}