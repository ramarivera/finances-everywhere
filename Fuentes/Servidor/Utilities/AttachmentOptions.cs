using System;

namespace Servidor.Utilities
{
    public class AttachmentOptions
    {
        public static readonly string ConfigPath = "Attachments";
        
        public string SystemPath { get; set; }
    }
}