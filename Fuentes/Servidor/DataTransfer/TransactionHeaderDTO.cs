using System;

namespace Servidor.DataTransfer
{
    public class TransactionHeaderDTO
    {
        public long TransactionId { get; set; }

        public long AccountId { get; set; }

        public long CategoryId { get; set; }

        public bool HasAttachment { get; set; }

        public string Title { get; set; }

        public DateTime Occurrence { get; set; }

        public bool Expenditure { get; set; }

        public decimal Amount { get; set; }
    }
}