using System.Collections;
using System.Collections.Generic;

namespace Servidor.DataTransfer
{
    public class RecurrentTransactionDTO
    {
        public long RecurrentTransactionId { get; set; }
        public AccountHeaderDTO Account { get; set; }
        public CategoryDefinitionHeaderDTO CategoryDefinition { get; set; }
        public FrequencyDTO Frequency { get; set; }
        public string Title { get; set; }
        public bool Expenditure { get; set; }
        public decimal Amount { get; set; }
        public bool Active { get; set; }
    }
}