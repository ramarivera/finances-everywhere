using System;
using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class UserCreationDTO
    {
        [StringLength(100)]
        public string Username { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Gender { get; set; }

        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime BirthDate { get; set; }

        [StringLength(100)]
        public string GovernmentIdentification { get; set; }

        [StringLength(100)]
        public string GovernmentIdentificationType { get; set; }

        [Required]
        public long DepartmentId { get; set; }
    }
}