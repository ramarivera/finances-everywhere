using System;
using System.ComponentModel.DataAnnotations;
using Servidor.Models.Enums;

namespace Servidor.DataTransfer
{
    public class CategoryDefinitionUpdateDTO
    {
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        [StringLength(512)]
        public string Description { get; set; }
    }
}