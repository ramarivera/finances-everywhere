using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class DepartmentCreationDTO
    {
        public long ParentDepartmentId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }
    }
}