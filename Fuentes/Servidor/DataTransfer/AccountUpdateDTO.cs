using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class AccountUpdateDTO
    {
        public long CurrencyId { get; set; }
        public long DepartmentId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        [StringLength(512)]
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}