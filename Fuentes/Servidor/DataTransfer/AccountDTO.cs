using System.Collections;
using System.Collections.Generic;

namespace Servidor.DataTransfer
{
    public class AccountDTO
    {
        public long AccountId { get; set; }
        public DepartmentHeaderDTO Department { get; set; }
        public CurrencyDTO Currency { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public IList<TransactionDTO> Transactions { get; set; }
        public IList<RecurrentTransactionDTO> RecurrentTransactions { get; set; }
    }
}