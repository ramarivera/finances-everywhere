using System;
using System.ComponentModel.DataAnnotations;
using Servidor.Models.Enums;

namespace Servidor.DataTransfer
{
    public class CategoryDefinitionCreationDTO
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        [Required]
        [Range(1, 100000000)]
        public decimal Budget { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidFrom { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidUntil { get; set; }

        [Required]
        public int Offset { get; set; }

        [Required]
        public Period Period { get; set; }
    }
}