using System.Collections;
using System.Collections.Generic;

namespace Servidor.DataTransfer
{
    public class AttachmentCreationDTO
    {
        public string Name { get; set; }
        public string Reference { get; set; }
        public IList<string> ByteArray { get; set; }
    }
}