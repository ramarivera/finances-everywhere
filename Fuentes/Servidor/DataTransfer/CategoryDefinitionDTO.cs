using System;
using System.Collections;
using System.Collections.Generic;
using Servidor.Models.Enums;

namespace Servidor.DataTransfer
{
    public class CategoryDefinitionDTO
    {
        public long CategoryDefinitionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public int Offset { get; set; }
        public Period Period { get; set; }
        public IList<CategoryDTO> Categories { get; set; }
    }
}