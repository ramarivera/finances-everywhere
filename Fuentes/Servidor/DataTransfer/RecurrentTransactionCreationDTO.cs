using System;
using System.ComponentModel.DataAnnotations;
using Servidor.Models.Enums;

namespace Servidor.DataTransfer
{
    public class RecurrentTransactionCreationDTO
    {
        [Required]
        public long AccountId { get; set; }

        [Required]
        public long CategoryDefinitionId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        public bool Expenditure { get; set; }

        [Required]
        [Range(1, 100000000)]
        public decimal Amount { get; set; }
        
        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidFrom { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidUntil { get; set; }

        [Required]
        public int Offset { get; set; }

        [Required]
        public Period Period { get; set; }
    }
}