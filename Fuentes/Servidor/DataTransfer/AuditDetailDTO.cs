namespace Servidor.DataTransfer
{
    public class AuditDetailDTO
    {
        public long AuditLogId { get; set; }
        public long AuditDetailId { get; set; }
        public string AttributeName { get; set; }
        public ValueAndDescriptionDTO AtributeType { get; set; }
        public string NewValue { get; set; }
        public string OldValue { get; set; }
    }
}