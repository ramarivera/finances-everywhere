namespace Servidor.DataTransfer
{
    public class EffectiveRecurrentTransactionDTO
    {
        public TransactionHeaderDTO Transaction { get; set; }
        public RecurrentTransactionHeaderDTO RecurrentTransaction { get; set; }
        public FrequencyDTO Frequency { get; set; }
    }
}