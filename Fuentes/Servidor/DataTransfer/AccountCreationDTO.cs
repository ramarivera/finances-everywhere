using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class AccountCreationDTO
    {
        [Required]
        public long DepartmentId { get; set; }
        
        public long? CurrencyId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        [StringLength(512)]
        public string Description { get; set; }
    }
}