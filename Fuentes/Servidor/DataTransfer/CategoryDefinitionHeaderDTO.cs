namespace Servidor.DataTransfer
{
    public class CategoryDefinitionHeaderDTO
    {
        public long CategoryDefinitionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Budget { get; set; }
    }
}