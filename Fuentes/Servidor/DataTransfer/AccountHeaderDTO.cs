namespace Servidor.DataTransfer
{
    public class AccountHeaderDTO
    {
        public long AccountId { get; set; }
        public long DepartmentId { get; set; }
        public long CurrencyId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TransactionsCount { get; set; }
        public int RecurrentTransactionsCount { get; set; }
    }
}