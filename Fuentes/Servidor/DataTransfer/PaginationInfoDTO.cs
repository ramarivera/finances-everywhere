namespace Servidor.DataTransfer
{
    public class PaginationInfoDTO
    {
        public int Offset { get; set; }
        public int Count { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
        public string Previous { get; set; }
        public string Next { get; set; }
    }
}