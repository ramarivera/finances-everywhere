using System;

namespace Servidor.DataTransfer
{
    public class FrequencyDTO
    {
        public long FrequencyId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public int Offset { get; set; }
    }
}