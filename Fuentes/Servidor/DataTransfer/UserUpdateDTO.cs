using System;
using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class UserUpdateDTO
    {

        [StringLength(100)]
        public string Username { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Gender { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime BirthDate { get; set; }

        [StringLength(100)]
        public string GovernmentIdentification { get; set; }

        [StringLength(100)]
        public string GovernmentIdentificationType { get; set; }
        public bool Active { get; set; }
        public bool Locked { get; set; }
    }
}