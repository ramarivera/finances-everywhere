using System;

namespace Servidor.DataTransfer
{
    public class TransactionDTO
    {
        public long TransactionId { get; set; }

        public AccountHeaderDTO Account { get; set; }

        public TransactionCategoryDTO Category { get; set; }

        public string Comment { get; set; }

        public string Title { get; set; }

        public DateTime Occurrence { get; set; }

        public bool Expenditure { get; set; }

        public decimal Amount { get; set; }

        public string Base64AttachmentImage { get; set; }

        public bool IsRecurrent { get; set; }
    }
}