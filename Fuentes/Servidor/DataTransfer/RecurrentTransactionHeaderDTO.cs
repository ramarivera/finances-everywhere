namespace Servidor.DataTransfer
{
    public class RecurrentTransactionHeaderDTO
    {
        public long RecurrentTransactionId { get; set; }
        public long AccountId { get; set; }
        public long CategoryDefinitionId { get; set; }
        public string Title { get; set; }
        public bool Expenditure { get; set; }
        public decimal Amount { get; set; }
        public bool Active { get; set; }
    }
}