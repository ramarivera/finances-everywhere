using System;
using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class TransactionCreationDTO
    {
        [Required]
        public long AccountId { get; set; }

        [Required]
        public long CategoryDefinitionId { get; set; }

        [StringLength(100)]
        public string Comment { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime Occurrence { get; set; }

        [Required]
        public bool Expenditure { get; set; }

        [Required]
        [Range(1, 100000000)]
        public decimal Amount { get; set; }

        public string Base64ImageAttachment {get;set;}
    }
}