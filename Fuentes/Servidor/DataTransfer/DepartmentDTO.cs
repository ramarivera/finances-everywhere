using System.Collections;
using System.Collections.Generic;

namespace Servidor.DataTransfer
{
    public class DepartmentDTO
    {
        public long DepartmentId { get; set; }
        public DepartmentDTO ParentDepartment { get; set; }
        public IList<DepartmentDTO> ChildDepartments { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public IList<UserDTO> Users { get; set; }
    }
}