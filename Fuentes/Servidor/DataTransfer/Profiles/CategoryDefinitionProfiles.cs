using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class CategoryDefinitionProfile : Profile
    {
        public CategoryDefinitionProfile()
        {
            CreateMap<CategoryDefinition, CategoryDefinitionDTO>()
                .ForMember(dest => dest.CategoryDefinitionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Budget, opt => opt.MapFrom(src => src.Budget))
                .ForMember(dest => dest.Period, opt => opt.MapFrom(src => src.Frequency.PeriodLength))
                .ForMember(dest => dest.ValidFrom, opt => opt.MapFrom(src => src.Frequency.ValidFrom))
                .ForMember(dest => dest.ValidUntil, opt => opt.MapFrom(src => src.Frequency.ValidUntil))
                .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories));
        }
    }

    public class CategoryDefinitionHeaderProfile : Profile
    {
        public CategoryDefinitionHeaderProfile()
        {
            CreateMap<CategoryDefinition, CategoryDefinitionHeaderDTO>()
                .ForMember(dest => dest.CategoryDefinitionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Budget, opt => opt.MapFrom(src => src.Budget));
        }
    }
}