using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class SettingProfile : Profile
    {
        public SettingProfile()
        {
            CreateMap<Setting, SettingDTO>()
                .ForMember(dest => dest.SettingId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.SettingKey))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.SettingValue));
        }
    }
}