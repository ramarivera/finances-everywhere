using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate))
                .ForMember(dest => dest.GovernmentIdentification, opt => opt.MapFrom(src => src.GovernmentIdentification))
                .ForMember(dest => dest.GovernmentIdentificationType, opt => opt.MapFrom(src => src.GovernmentIdentificationType))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Locked, opt => opt.MapFrom(src => src.Locked))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department));
        }
    }

    public class UserHeaderProfile : Profile
    {
        public UserHeaderProfile()
        {
            CreateMap<User, UserHeaderDTO>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Locked, opt => opt.MapFrom(src => src.Locked))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.Department.Id));
        }
    }
}