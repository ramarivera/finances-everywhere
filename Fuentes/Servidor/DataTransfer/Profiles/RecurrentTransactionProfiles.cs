using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class RecurrentTransactionProfile : Profile
    {
        public RecurrentTransactionProfile()
        {
            CreateMap<RecurrentTransaction, RecurrentTransactionDTO>()
                .ForMember(dest => dest.RecurrentTransactionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                .ForMember(dest => dest.CategoryDefinition, opt => opt.MapFrom(src => src.CategoryDefinition))
                .ForMember(dest => dest.Frequency, opt => opt.MapFrom(src => src.Frequency))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Expenditure, opt => opt.MapFrom(src => src.Expenditure))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active));
        }
    }

    public class RecurrentTransactionHeaderProfile : Profile
    {
        public RecurrentTransactionHeaderProfile()
        {
            CreateMap<RecurrentTransaction, RecurrentTransactionHeaderDTO>()
                .ForMember(dest => dest.RecurrentTransactionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => src.Account.Id))
                .ForMember(dest => dest.CategoryDefinitionId, opt => opt.MapFrom(src => src.CategoryDefinition.Id))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Expenditure, opt => opt.MapFrom(src => src.Expenditure))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active));
        }
    }
}