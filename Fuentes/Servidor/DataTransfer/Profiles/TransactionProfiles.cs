using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile()
        {
            CreateMap<Transaction, TransactionDTO>()
                .ForMember(dest => dest.TransactionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category))
                .ForMember(dest => dest.Comment, opt => opt.MapFrom(src => src.Comment))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Occurrence, opt => opt.MapFrom(src => src.Occurrence))
                .ForMember(dest => dest.Expenditure, opt => opt.MapFrom(src => src.Expenditure))
                .ForMember(dest => dest.IsRecurrent, opt => opt.MapFrom(src => src.RecurrentTransaction != null))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount));
        }
    }

    public class TransactionHeaderProfile : Profile
    {
        public TransactionHeaderProfile()
        {
            CreateMap<Transaction, TransactionHeaderDTO>()
                .ForMember(dest => dest.TransactionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => src.Account.Id))
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.Category.Id))
                .ForMember(dest => dest.HasAttachment, opt => opt.MapFrom(src => src.Attachment != null))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Occurrence, opt => opt.MapFrom(src => src.Occurrence))
                .ForMember(dest => dest.Expenditure, opt => opt.MapFrom(src => src.Expenditure))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount));
        }
    }
}