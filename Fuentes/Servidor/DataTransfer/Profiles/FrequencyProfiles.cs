using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class FrequencyProfile : Profile
    {
        public FrequencyProfile()
        {
            CreateMap<Frequency, FrequencyDTO>()
                .ForMember(dest => dest.FrequencyId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ValidFrom, opt => opt.MapFrom(src => src.ValidFrom))
                .ForMember(dest => dest.ValidUntil, opt => opt.MapFrom(src => src.ValidUntil))
                .ForMember(dest => dest.Offset, opt => opt.MapFrom(src => src.Offset));
        }
    }
}