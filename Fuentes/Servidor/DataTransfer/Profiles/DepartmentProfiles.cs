using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class DepartmentProfile : Profile
    {
        public DepartmentProfile()
        {
            CreateMap<Department, DepartmentDTO>()
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Users, opt => opt.MapFrom(src => src.Users));
        }
    }

    public class DepartmentHeaderProfile : Profile
    {
        public DepartmentHeaderProfile()
        {
            CreateMap<Department, DepartmentHeaderDTO>()
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.UsersCount, opt => opt.MapFrom(src => src.Users.Count));
        }
    }
}