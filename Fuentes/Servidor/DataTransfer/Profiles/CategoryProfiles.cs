using AutoMapper;
using Servidor.Models;

namespace Servidor.DataTransfer.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDTO>()
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CategoryDefinitionId, opt => opt.MapFrom(src => src.CategoryDefinition.Id))
                .ForMember(dest => dest.Budget, opt => opt.MapFrom(src => src.Budget))
                .ForMember(dest => dest.ValidFrom, opt => opt.MapFrom(src => src.ValidFrom))
                .ForMember(dest => dest.ValidUntil, opt => opt.MapFrom(src => src.ValidUntil))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active));
        }
    }

    public class TransactionCategoryProfile : Profile
    {
        public TransactionCategoryProfile()
        {
            CreateMap<Category, TransactionCategoryDTO>()
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CategoryDefinitionId, opt => opt.MapFrom(src => src.CategoryDefinition.Id))
                .ForMember(dest => dest.Budget, opt => opt.MapFrom(src => src.Budget))
                .ForPath(dest => dest.Name, opt => opt.MapFrom(src => src.CategoryDefinition.Name))
                .ForPath(dest => dest.CategoryCode, opt => opt.MapFrom(src => src.CategoryDefinition.Code))
                .ForPath(dest => dest.CategoryDescription, opt => opt.MapFrom(src => src.CategoryDefinition.Description));
        }
    }

}