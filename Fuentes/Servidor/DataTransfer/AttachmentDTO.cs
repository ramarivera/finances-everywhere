namespace Servidor.DataTransfer
{
    public class AttachmentDTO
    {
        public long AttachmentId { get; set; }
        public long TransactionId { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public bool Active { get; set; }
    }
}