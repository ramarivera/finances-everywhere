namespace Servidor.DataTransfer
{
    public class UserHeaderDTO
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public long DepartmentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public bool Locked { get; set; }
    }
}