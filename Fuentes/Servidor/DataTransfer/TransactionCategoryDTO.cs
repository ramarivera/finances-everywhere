namespace Servidor.DataTransfer
{
    //fusion del category y period category
    public class TransactionCategoryDTO
    {
        public long CategoryId { get; set; }
        public long CategoryDefinitionId { get; set; }
        public decimal Budget { get; set; }
        public string Name { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDescription { get; set; }
    }
}