using System;

namespace Servidor.DataTransfer
{
    public class CategoryDTO
    {
        public long CategoryId { get; set; }
        public long CategoryDefinitionId { get; set; }
        public decimal Budget { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public string Notes {get; set;}
        public bool Active { get; set; }
    }
}