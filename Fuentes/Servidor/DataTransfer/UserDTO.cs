using System;

namespace Servidor.DataTransfer
{
    public class UserDTO
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public string GovernmentIdentification { get; set; }
        public string GovernmentIdentificationType { get; set; }
        public bool Active { get; set; }
        public bool Locked { get; set; }
        public DateTime JoinDate { get; set; }
        public DepartmentHeaderDTO Department { get; set; }
    }
}