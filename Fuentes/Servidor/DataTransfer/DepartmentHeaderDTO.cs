namespace Servidor.DataTransfer
{
    public class DepartmentHeaderDTO
    {
        public long DepartmentId { get; set; }
        public long ParentDepartmentId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int UsersCount { get; set; }
        public int ChildDepartmentsCount { get; set; }
    }
}