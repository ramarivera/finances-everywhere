using System;
using Servidor.Models.Enums;

namespace Servidor.DataTransfer
{
    public class CategoryUpdateDTO
    {
        public decimal Budget { get; set; }
        public string Notes {get; set;}
        public bool Active { get; set; }
    }
}