namespace Servidor.DataTransfer
{
    public class SettingUpdateDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}