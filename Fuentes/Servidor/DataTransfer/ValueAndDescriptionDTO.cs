namespace Servidor.DataTransfer
{
    public class ValueAndDescriptionDTO
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }
}