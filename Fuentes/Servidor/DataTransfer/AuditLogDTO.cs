using System;
using System.Collections;

namespace Servidor.DataTransfer
{
    public class AuditLogDTO
    {
        public long AuditEventId { get; set; }
        public long UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public long AuditLogId { get; set; }
        public ValueAndDescriptionDTO Type { get; set; }
        public long RecordId { get; set; }
        public string RecordType { get; set; }
        public IList[] Details { get; set; }
    }
}