namespace Servidor.DataTransfer
{
    public class CurrencyDTO
    {
        public long CurrencyId { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string IsoCode { get; set; }
        public bool Active { get; set; }
    }
}