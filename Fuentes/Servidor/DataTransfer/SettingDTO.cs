namespace Servidor.DataTransfer
{
    public class SettingDTO
    {
        public long SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}