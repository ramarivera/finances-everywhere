using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class DepartmentUpdateDTO
    {
        public long ParentDepartmentId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}