using System;
using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class TransactionUpdateDTO
    {
        [StringLength(100)]
        public string Comment { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime Occurrence { get; set; }
        public bool Expenditure { get; set; }

        [Range(1, 100000000)]
        public decimal Amount { get; set; }
        public bool Active { get; set; }
    }
}