using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class RecurrentTransactionUpdateDTO
    {
        [StringLength(100)]
        public string Title { get; set; }
        public bool Active { get; set; }
    }
}