using System;
using System.ComponentModel.DataAnnotations;

namespace Servidor.DataTransfer
{
    public class FrequencyCreationDTO
    {

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidFrom { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "The input must be a DateTime.")]
        public DateTime ValidUntil { get; set; }
        
        public int Offset { get; set; }

        [Required]
        public ValueAndDescriptionDTO Period { get; set; }
    }
}