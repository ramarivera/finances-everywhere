using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class UsersService : IUsersService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly IDepartmentsService _departmentsService;

        public UsersService(
            FinancesEverywhereContext context,
            IDepartmentsService departmentService)
        {
            _context = context;
            _departmentsService = departmentService;
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users
                        .Include(u => u.Department)
                        .OrderBy(x => x.Id);
        }

        public User GetById(long userId)
        {
            var user = GetAll().FirstOrDefault(t => t.Id == userId);

            if (user == null)
            {
                throw new ArgumentException($"User with Id {userId} was not found");
            }

            return user;
        }

        public User Create(UserCreationDTO creationData)
        {
            var department = _departmentsService.GetById(creationData.DepartmentId);

            var user = new User
            {
                Username = creationData.Username,
                FirstName = creationData.FirstName,
                LastName = creationData.LastName,
                Gender = creationData.Gender,
                Email = creationData.Email,
                BirthDate = creationData.BirthDate,
                GovernmentIdentification = creationData.GovernmentIdentification,
                GovernmentIdentificationType = creationData.GovernmentIdentificationType,
                Active = true,
                Department = department
            };

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public User Update(long userId, UserUpdateDTO updateData)
        {
            var user =  GetById(userId);
            
            user.Username = updateData.Username;
            user.FirstName = updateData.FirstName;
            user.LastName = updateData.LastName;
            user.Gender = updateData.Gender;
            user.Email = updateData.Email;
            user.BirthDate = updateData.BirthDate;
            user.GovernmentIdentification = updateData.GovernmentIdentification;
            user.GovernmentIdentificationType = updateData.GovernmentIdentificationType;
            user.Active = updateData.Active;
            user.Locked = updateData.Locked;

            _context.Users.Update(user);
            _context.SaveChanges();

            return user;
        }

        public void Delete(long userId)
        {
            var user =  GetById(userId);
          
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}