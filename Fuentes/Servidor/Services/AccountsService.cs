using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly ICurrenciesService _currenciesService;
        private readonly IDepartmentsService _departmentsService;

        public AccountsService(
            FinancesEverywhereContext context,
            ICurrenciesService currenciesService,
            IDepartmentsService departmentsService)
        {
            _context = context;
            _currenciesService = currenciesService;
            _departmentsService = departmentsService;
        }

        public IQueryable<Account> GetAll()
        {
            return _context.Accounts
                        .Include(a => a.Currency)
                        .Include(a => a.Department)
                        .Include(a => a.Transactions)
                        .Include(a => a.RecurrentTransactions)
                        .OrderBy(x => x.Id);;
        }

        public Account GetById(long accountId)
        {
            var account = GetAll()
                            .FirstOrDefault(t => t.Id == accountId);

            if (account == null)
            {
                throw new ArgumentException($"Account with Id {accountId} was not found");
            }

            return account;
        }

        public Account Create(AccountCreationDTO creationData)
        {
            var currency = creationData.CurrencyId.HasValue ?
                _currenciesService.GetById(creationData.CurrencyId.Value) :
                _currenciesService.GetDefaultCurrency();

            var department = _departmentsService.GetById(creationData.DepartmentId);

            var account = new Account
            {
                Name = creationData.Name,
                Code = creationData.Code,
                Description = creationData.Description,
                Active = true,
                Currency = currency,
                Department = department
            };

            _context.Accounts.Add(account);

            _context.SaveChanges();

            return account;
        }

        public Account Update(long accountId, AccountUpdateDTO updateData)
        {
            var account = GetById(accountId);

            account.Name = updateData.Name;
            account.Code = updateData.Code;
            account.Description = updateData.Description;
            account.Currency = _currenciesService.GetById(updateData.CurrencyId);
            account.Department = _departmentsService.GetById(updateData.DepartmentId);
            account.Active = updateData.Active;

            _context.Accounts.Update(account);
            _context.SaveChanges();

            return account;
        }

        public void Delete(long accountId)
        {
            var account = GetById(accountId);

            _context.Accounts.Remove(account);
            _context.SaveChanges();
        }

        public IQueryable<Transaction> GetTransactionsByAccountId(long accountId)
        {
            return GetById(accountId).Transactions.AsQueryable();
        }

        public IQueryable<RecurrentTransaction> GetRecurrentTransactionsByAccountId(long accountId)
        {
            return GetById(accountId).RecurrentTransactions.AsQueryable();
        }
    }
}