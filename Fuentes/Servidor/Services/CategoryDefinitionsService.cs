using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class CategoryDefinitionsService : ICategoryDefinitionsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly CategoriesService _categoriesService;

        public CategoryDefinitionsService(FinancesEverywhereContext context, CategoriesService categoriesService)
        {
            _context = context;
            _categoriesService = categoriesService;
        }

        public IQueryable<CategoryDefinition> GetAll()
        {
            return _context.CategoryDefinitions
                .Include(c => c.Frequency)
                .OrderBy(x => x.Id);
        }

        public CategoryDefinition GetById(long categoryDefinitionId)
        {
            var categoryDefinition = GetAll()
                .Include(c => c.Categories)
                .FirstOrDefault(t => t.Id == categoryDefinitionId);

            if (categoryDefinition == null)
            {
                throw new ArgumentException($"CategoryDefinition with Id {categoryDefinitionId} was not found");
            }

            return categoryDefinition;
        }

        public CategoryDefinition Create(CategoryDefinitionCreationDTO creationData)
        {
            var frequency = new Frequency
            {
                ValidFrom = creationData.ValidFrom,
                ValidUntil = creationData.ValidUntil,
                Offset = creationData.Offset,
                PeriodLength = creationData.Period
            };

            var categoryDefinition = new CategoryDefinition
            {
                Name = creationData.Name,
                Code = creationData.Code,
                Description = creationData.Description,
                Budget = creationData.Budget,
                Frequency = frequency,
                Active = true
            };

            var categories = _categoriesService.CreateCategoriesForCategoryDefinition(categoryDefinition);

            foreach (var category in categories)
            {
                categoryDefinition.AddCategory(category);
            }

            _context.CategoryDefinitions.Add(categoryDefinition);
            _context.SaveChanges();

            return categoryDefinition;
        }

        public CategoryDefinition Update(long categoryDefinitionId, CategoryDefinitionUpdateDTO updateData)
        {
            var categoryDefinition = GetById(categoryDefinitionId);

            categoryDefinition.Name = updateData.Name;
            categoryDefinition.Code = updateData.Code;
            categoryDefinition.Description = updateData.Description;

            _context.CategoryDefinitions.Update(categoryDefinition);
            _context.SaveChanges();

            return categoryDefinition;
        }

        public void Delete(long categoryId)
        {
            var category = GetById(categoryId);

            _context.CategoryDefinitions.Remove(category);
            _context.SaveChanges();
        }

        public Category FindCategoryByDate(long categoryDefinitionId, DateTime occurrence)
        {
            var category = GetAll()
                .Where(x => x.Id == categoryDefinitionId)
                .SelectMany(x => x.Categories)
                .Where(x => x.ValidFrom <= occurrence && x.ValidUntil >= occurrence)
                .FirstOrDefault();

            if (category == null)
            {
                throw new ArgumentException($"Category for {occurrence} with categoryDefinitionId {categoryDefinitionId} was not found");
            }

            return category;
        }
    }
}