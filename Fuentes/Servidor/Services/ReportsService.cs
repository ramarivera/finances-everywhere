using System.IO;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Servidor.Models.Reports;
using Servidor.Persistence;
using Servidor.Services.Interfaces;
using System.Collections.Generic;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Servidor.Models;
using Servidor.DataTransfer;

namespace Servidor.Services
{
    public class ReportsService : IReportsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly ITransactionsService _transactionsService;
        private readonly IAccountsService _accountsService;
        private readonly ICategoryDefinitionsService _categoryDefinitionsService;
        public ReportsService(
          FinancesEverywhereContext context,
          ITransactionsService transactionsService,
          IAccountsService accountsService,
          ICategoryDefinitionsService categoryDefinitionsService)
        {
            _context = context;
            _transactionsService = transactionsService;
            _accountsService = accountsService;
            _categoryDefinitionsService = categoryDefinitionsService;
        }


        public IEnumerable<ReportTransaction> FindReportTransactionsMatchingFilter(ReportFilter filter)
        {
            var reportTransactions = new List<ReportTransaction>();
            var transactions = FindTransactionsMatchingFilter(filter);

            foreach (var item in transactions)
            {
                var categoryDefinition = _categoryDefinitionsService.GetById(item.Category.CategoryDefinition.Id);

                var categoryDefinitionData = new ValueAndDescriptionDTO
                {
                    Id = categoryDefinition.Id,
                    Description = categoryDefinition.Name
                };

                var account = this._accountsService.GetById(item.Account.Id);

                var accountData = new ValueAndDescriptionDTO
                {
                    Id = account.Id,
                    Description = account.Name
                };

                var reportTransaction = new ReportTransaction
                {
                    TransactionId = item.Id,
                    Title = item.Title,
                    Comment = item.Comment,
                    Amount = item.Amount,
                    Expenditure = item.Expenditure,
                    Occurrence = item.Occurrence,
                    CategoryDefinition = categoryDefinitionData,
                    Account = accountData
                };

                reportTransactions.Add(reportTransaction);
            }

            return reportTransactions;
        }

        public TransactionsSummary GenerateTransactionsSummaryMatchingFilter(ReportFilter filter)
        {
            var reportTransactions = FindReportTransactionsMatchingFilter(filter);

            var result = new TransactionsSummary
            {
                Expense = reportTransactions.Where(x => x.Expenditure).Sum(x => x.Amount),
                Income = reportTransactions.Where(x => !x.Expenditure).Sum(x => x.Amount),
                TransactionsCount = reportTransactions.Count()
            };

            result.Difference = result.Income - result.Expense;

            return result;
        }

        public byte[] BuildTransactionsReportExport(ReportFilter filter)
        {
            var transactionsSummary = GenerateTransactionsSummaryMatchingFilter(filter);
            var reportTransactions = FindReportTransactionsMatchingFilter(filter);

            using (var exportStream = new MemoryStream())
            {
                // Create a new workbook and a sheet named "Transactions Report"
                var workbook = new XSSFWorkbook();
                var sheet = workbook.CreateSheet("Transactions Report");

                // Create bold style
                var boldFontStyle = workbook.CreateCellStyle();
                var boldFont = workbook.CreateFont();
                boldFont.Boldweight = (short)FontBoldWeight.Bold;
                boldFontStyle.SetFont(boldFont);
                boldFontStyle.BorderTop = BorderStyle.Thin;
                boldFontStyle.BorderBottom = BorderStyle.Thin;
                boldFontStyle.BorderLeft = BorderStyle.Thin;
                boldFontStyle.BorderRight = BorderStyle.Thin;

                // Create header style
                var headerCellStyle = workbook.CreateCellStyle();
                headerCellStyle.SetFont(boldFont);
                headerCellStyle.Alignment = HorizontalAlignment.Center;
                headerCellStyle.BorderTop = BorderStyle.Medium;
                headerCellStyle.BorderBottom = BorderStyle.Medium;
                headerCellStyle.BorderLeft = BorderStyle.Medium;
                headerCellStyle.BorderRight = BorderStyle.Medium;

                // Create items style
                var itemCellStyle = workbook.CreateCellStyle();
                itemCellStyle.BorderTop = BorderStyle.Thin;
                itemCellStyle.BorderBottom = BorderStyle.Thin;
                itemCellStyle.BorderLeft = BorderStyle.Thin;
                itemCellStyle.BorderRight = BorderStyle.Thin;

                // Create currency style
                var currencyCellStyle = workbook.CreateCellStyle();
                currencyCellStyle.CloneStyleFrom(itemCellStyle);
                currencyCellStyle.Alignment = HorizontalAlignment.Right;
                // Get / create the data format string
                var formatId = HSSFDataFormat.GetBuiltinFormat("$#,##0.00");
                if (formatId == -1)
                {
                    var newDataFormat = workbook.CreateDataFormat();
                    currencyCellStyle.DataFormat = newDataFormat.GetFormat("$#,##0.00");
                }
                else
                    currencyCellStyle.DataFormat = formatId;

                // Add header labels
                var rowIndex = 0;
                var row = sheet.CreateRow(rowIndex);
                var cell = row.CreateCell(0);
                cell.SetCellValue("Transaction ID"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(1);
                cell.SetCellValue("Occurrence"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(2);
                cell.SetCellValue("Title"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(3);
                cell.SetCellValue("Comment"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(4);
                cell.SetCellValue("Amount"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(5);
                cell.SetCellValue("Expenditure"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(6);
                cell.SetCellValue("Category Definition"); cell.CellStyle = headerCellStyle;
                cell = row.CreateCell(7);
                cell.SetCellValue("Account"); cell.CellStyle = headerCellStyle;
                rowIndex++;

                // Add data rows
                foreach (var transaction in reportTransactions)
                {
                    row = sheet.CreateRow(rowIndex);
                    cell = row.CreateCell(0);
                    cell.SetCellValue(transaction.TransactionId); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(1);
                    cell.SetCellValue(transaction.Occurrence.ToShortDateString()); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(2);
                    cell.SetCellValue(transaction.Title); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(3);
                    cell.SetCellValue(transaction.Comment); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(4);
                    cell.SetCellValue((double)transaction.Amount); cell.CellStyle = currencyCellStyle;
                    cell = row.CreateCell(5);
                    cell.SetCellValue(transaction.Expenditure); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(6);
                    cell.SetCellValue(transaction.CategoryDefinition.Description); cell.CellStyle = itemCellStyle;
                    cell = row.CreateCell(7);
                    cell.SetCellValue(transaction.Account.Description); cell.CellStyle = itemCellStyle;
                    rowIndex++;
                }

                rowIndex++;

                // Add Summary

                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue("Summary"); cell.CellStyle = boldFontStyle;
                rowIndex++;

                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue("Income"); cell.CellStyle = boldFontStyle;
                cell = row.CreateCell(1);
                cell.SetCellValue((double)transactionsSummary.Income); cell.CellStyle = currencyCellStyle;
                rowIndex++;

                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue("Expense"); cell.CellStyle = boldFontStyle;
                cell = row.CreateCell(1);
                cell.SetCellValue((double)transactionsSummary.Expense); cell.CellStyle = currencyCellStyle;
                rowIndex++;

                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue("Difference"); cell.CellStyle = boldFontStyle;
                cell = row.CreateCell(1);
                cell.SetCellValue((double)transactionsSummary.Difference); cell.CellStyle = currencyCellStyle;
                rowIndex++;

                row = sheet.CreateRow(rowIndex);
                cell = row.CreateCell(0);
                cell.SetCellValue("Number of Transactions"); cell.CellStyle = boldFontStyle;
                cell = row.CreateCell(1);
                cell.SetCellValue(transactionsSummary.TransactionsCount); cell.CellStyle = itemCellStyle;
                rowIndex++;

                AdjustColumnSizes(sheet);   

                //workbook.Write(fs);
                workbook.Write(exportStream);

                return exportStream.ToArray();
            }
        }

        private IList<Transaction> FindTransactionsMatchingFilter(ReportFilter input)
        {
            var validator = new ReportFilterValidator();
            validator.ValidateAndThrow(input);

            var transactions = _transactionsService.GetEfectiveTransactions()
                        .Include(t => t.Account.Currency)
                        .Include(t => t.Category.CategoryDefinition)
                        .AsQueryable();

            //se aplican filtros de fecha
            if (input.From != null || input.Until != null)
            {
                transactions = transactions.Where(t => t.Occurrence >= input.From
                                    && t.Occurrence <= input.Until);
            }

            //se aplica filtro de moneda
            if (input.CurrencyId != null)
            {
                transactions = transactions.Where(t => t.Account.Currency.Id == input.CurrencyId);
            }

            //se aplica filtro de accounts
            if (input.AccountIds.Any())
            {
                transactions = transactions.Where(t => input.AccountIds.Contains(t.Account.Id));
            }

            //se aplica filtro de category
            if (input.CategoryDefinitionIds.Any())
            {
                transactions = transactions.Where(t => input.CategoryDefinitionIds.Contains(t.Category.CategoryDefinition.Id));
            }

            return transactions.ToList();
        }

        private void AdjustColumnSizes(ISheet sheet)
        {
            // Due to bugs on autosize, this workaround was found.
            // see https://github.com/dotnetcore/NPOI/issues/77

            // Column base size
            const int n = 256;

            sheet.SetColumnWidth(0, 22 * n);
            sheet.SetColumnWidth(1, 12 * n);
            sheet.SetColumnWidth(2, 24 * n);
            sheet.SetColumnWidth(3, 70 * n);
            sheet.SetColumnWidth(4, 12 * n);
            sheet.SetColumnWidth(5, 12 * n);
            sheet.SetColumnWidth(6, 19 * n);
            sheet.SetColumnWidth(7, 13 * n);
        }


    }
}