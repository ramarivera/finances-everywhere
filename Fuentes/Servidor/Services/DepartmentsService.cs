using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class DepartmentsService : IDepartmentsService
    {
        private readonly FinancesEverywhereContext _context;

        public DepartmentsService(FinancesEverywhereContext context)
        {
            _context = context;
        }

        public IQueryable<Department> GetAll()
        {
            return _context.Departments
                        .Include(u => u.Users)
                        .OrderBy(x => x.Id);
        }

        public Department GetById(long departmentId)
        {
            var department = GetAll()
                                .FirstOrDefault(t => t.Id == departmentId);

            if (department == null)
            {
                throw new ArgumentException($"Department with Id {departmentId} was not found");
            }
            
            return department;
        }

        public Department Create(DepartmentCreationDTO creationData)
        {
            var department = new Department
            {
                Name = creationData.Name,
                Description = creationData.Description,
                Active = true
            };

            _context.Departments.Add(department);
            _context.SaveChanges();

            return department;
        }

        public Department Update(long departmentId, DepartmentUpdateDTO updateData)
        {
            var department = GetById(departmentId);
            
            department.Name = updateData.Name;
            department.Description = updateData.Description;
            department.Active = updateData.Active;

            _context.Departments.Update(department);
            _context.SaveChanges();

            return department;
        }

        public void Delete(long departmentId)
        {
            var department = GetById(departmentId);
        
            _context.Departments.Remove(department);
            _context.SaveChanges();
        }

        public IQueryable<User> GetUsersByDepartmentId(long departmentId)
        {
            return GetById(departmentId).Users.AsQueryable();
        }
    }
}