using System.Collections.Generic;
using Servidor.Models;
using Servidor.Models.Reports;

namespace Servidor.Services.Interfaces
{
    public interface IReportsService
    {
        byte[] BuildTransactionsReportExport (ReportFilter filter);
        IEnumerable<ReportTransaction> FindReportTransactionsMatchingFilter(ReportFilter filter);
        TransactionsSummary GenerateTransactionsSummaryMatchingFilter(ReportFilter filter);
    }
}