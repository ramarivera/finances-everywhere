using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface IAccountsService
    {
        IQueryable<Account> GetAll();
        Account GetById(long id);
        Account Create(AccountCreationDTO creationData);
        Account Update(long accountId, AccountUpdateDTO updateData);
        void Delete(long id);
        IQueryable<Transaction> GetTransactionsByAccountId(long id);
        IQueryable<RecurrentTransaction> GetRecurrentTransactionsByAccountId(long id);
    }
}