using System.Collections.Generic;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface ISettingsService : IGenericService<Setting>
    {
        Setting GetByKey(string settingKey);
    }
}