using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface IRecurrentTransactionsService
    {
        IQueryable<RecurrentTransaction> GetAll();
        RecurrentTransaction GetById(long id);
        RecurrentTransaction Create(RecurrentTransactionCreationDTO creationData);
        RecurrentTransaction Update(long recurrentTransactionId, RecurrentTransactionUpdateDTO updateData);
        void Delete(long id);
    }
}