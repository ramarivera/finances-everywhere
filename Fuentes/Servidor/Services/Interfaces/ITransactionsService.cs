using System;
using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Models.Reports;

namespace Servidor.Services.Interfaces
{
    public interface ITransactionsService
    {
        IQueryable<Transaction> GetAll();
        IQueryable<Transaction> GetEfectiveTransactions();
        Transaction GetById(long id);
        Transaction Create(TransactionCreationDTO creationData);
        Transaction Update(long id, TransactionUpdateDTO updateData);
        void Delete(long id);
        IEnumerable<TransactionCreationDTO> CreateTransactionsForRecurrentTransactions(RecurrentTransaction recurrentTransaction);
    }
}