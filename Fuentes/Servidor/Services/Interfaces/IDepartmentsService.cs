using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface IDepartmentsService
    {
        IQueryable<Department> GetAll();
        Department GetById(long id);
        Department Create(DepartmentCreationDTO creationData);
        Department Update(long id, DepartmentUpdateDTO updateData);
        void Delete(long id);
        IQueryable<User> GetUsersByDepartmentId(long id);
    }
}