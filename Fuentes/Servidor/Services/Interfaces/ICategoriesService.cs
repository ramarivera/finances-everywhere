using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface ICategoriesService
    {
        IQueryable<Category> GetAll();
        Category GetById(long categoryId);
        Category Update(long id, CategoryUpdateDTO updateData); 
    }
}