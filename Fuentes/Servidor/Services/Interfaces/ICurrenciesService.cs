using System.Collections.Generic;
using System.Linq;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface ICurrenciesService
    {
        IQueryable<Currency> GetAll();

        Currency GetById(long id);

        Currency GetDefaultCurrency();
    }
}