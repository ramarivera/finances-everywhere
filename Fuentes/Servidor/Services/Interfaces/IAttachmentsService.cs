

using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface IAttachmentsService
    {
        Attachment CreateAttachment(string base64Image);

        byte[] GetAttachmentContents(Attachment attachment);

        void DeleteAttachment(Attachment attachment);
    }
}