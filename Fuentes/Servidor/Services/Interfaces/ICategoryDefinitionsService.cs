using System;
using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface ICategoryDefinitionsService
    {
        IQueryable<CategoryDefinition> GetAll();
        CategoryDefinition GetById(long id);
        CategoryDefinition Create(CategoryDefinitionCreationDTO creationData);
        CategoryDefinition Update(long id, CategoryDefinitionUpdateDTO updateData);        
        void Delete(long id);
        Category FindCategoryByDate(long categoryDefinitionId, DateTime occurrence);   
    }
}