using System.Collections.Generic;
using System.Linq;
using Servidor.DataTransfer;
using Servidor.Models;

namespace Servidor.Services.Interfaces
{
    public interface IUsersService
    {
        IQueryable<User> GetAll();
        User GetById(long id);
        User Create(UserCreationDTO creationData);
        User Update(long id, UserUpdateDTO updateData);
        void Delete(long id);
         
    }
}