using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Models.Enums;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly ICategoryDefinitionsService _categoryDefinitionsService;

        private readonly IAccountsService _accountsService;
        private readonly IAttachmentsService _attachmentsService;

        public TransactionsService(
            FinancesEverywhereContext context,
            IAttachmentsService attachmentsService,
            ICategoryDefinitionsService categoryDefinitionsService,
            IAccountsService accountsService)
        {
            _context = context;
            _categoryDefinitionsService = categoryDefinitionsService;
            _attachmentsService = attachmentsService;
            _accountsService = accountsService;
        }

        public IQueryable<Transaction> GetAll()
        {
            return _context.Transactions
                .Include(t => t.Account)
                .Include(t => t.Category)
                .Include(t => t.Attachment)
                .OrderBy(x => x.Id);
        }

        public IQueryable<Transaction> GetEfectiveTransactions()
        {
            return GetAll().Where(t => t.IsEffective());
        }

        public Transaction GetById(long transactionId)
        {
            var transaction = GetAll()
                    .Include(t => t.RecurrentTransaction)
                    .Include(t => t.Attachment)
                    .Include(t => t.Category.CategoryDefinition)
                    .FirstOrDefault(t => t.Id == transactionId);

            if (transaction == null)
            {
                throw new ArgumentException($"Transaction with Id {transactionId} was not found");
            }

            return transaction;
        }

        public Transaction Create(TransactionCreationDTO creationData)
        {
            var account = _accountsService.GetById(creationData.AccountId);
            var category = _categoryDefinitionsService.FindCategoryByDate(creationData.CategoryDefinitionId, creationData.Occurrence);

            var transaction = new Transaction
            {
                Title = creationData.Title,
                Comment = creationData.Comment,
                Occurrence = creationData.Occurrence,
                Expenditure = creationData.Expenditure,
                Amount = creationData.Amount,
                Active = true,
                Account = account,
                Category = category
            };

            _context.Transactions.Add(transaction);
            _context.SaveChanges();

            if (!string.IsNullOrEmpty(creationData.Base64ImageAttachment)) {
                var attachment = _attachmentsService.CreateAttachment(creationData.Base64ImageAttachment);
                attachment.Transaction = transaction;
                attachment.TransactionId = transaction.Id;
                _context.SaveChanges();
            }

            return transaction;
        }

        public Transaction Update(long transactionId, TransactionUpdateDTO updateData)
        {
            var transaction = GetById(transactionId);

            transaction.Title = updateData.Title;
            transaction.Comment = updateData.Comment;
            transaction.Active = updateData.Active;

            if (transaction.RecurrentTransaction == null)
            {
                transaction.Expenditure = updateData.Expenditure;
                transaction.Amount = updateData.Amount;

                if (transaction.Occurrence != updateData.Occurrence)
                // en caso de cambio de fecha refresca la category asociada que podría haber cambiado
                {
                    transaction.Occurrence = updateData.Occurrence;
                    transaction.Category = _categoryDefinitionsService.FindCategoryByDate(transaction.Category.CategoryDefinition.Id, transaction.Occurrence);
                }
            }

            _context.Transactions.Update(transaction);
            _context.SaveChanges();

            return transaction;
        }

        public void Delete(long transactionId)
        {
            var transaction = GetById(transactionId);

            if (transaction.Attachment != null) {
                _attachmentsService.DeleteAttachment(transaction.Attachment);
            }
            _context.Transactions.Remove(transaction);

            _context.SaveChanges();
        }

        public IEnumerable<TransactionCreationDTO> CreateTransactionsForRecurrentTransactions(RecurrentTransaction recurrentTransaction)
        {
            var transactions = new List<TransactionCreationDTO>();

            DateTime occurrence = recurrentTransaction.Frequency.ValidFrom;

            while (occurrence < recurrentTransaction.Frequency.ValidUntil)
            {
                var newTransaction = new TransactionCreationDTO
                {
                    AccountId = recurrentTransaction.Account.Id,
                    CategoryDefinitionId = recurrentTransaction.CategoryDefinition.Id,
                    Title = recurrentTransaction.Title,
                    Occurrence = occurrence,
                    Comment = $"Recurrent Transaction for {occurrence.ToShortDateString()}",
                    Expenditure = recurrentTransaction.Expenditure,
                    Amount = recurrentTransaction.Amount,
                };

                transactions.Add(newTransaction);

                occurrence = occurrence.AddDays(recurrentTransaction.Frequency.PeriodLength.ToDays());
            }

            return transactions;
        }
    }
}