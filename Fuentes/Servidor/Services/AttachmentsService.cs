using System;
using System.IO;
using Microsoft.Extensions.Options;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;
using Servidor.Utilities;

namespace Servidor.Services
{
    public class AttachmentsService : IAttachmentsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly IOptions<AttachmentOptions> _attachmentOptions;

        public AttachmentsService(
            FinancesEverywhereContext context,
            IOptions<AttachmentOptions> attachmentOptions)
        {
            _context = context;
            _attachmentOptions = attachmentOptions;
        }

        public Attachment CreateAttachment(string base64Image)
        {
            if (string.IsNullOrEmpty(base64Image))
            {
                throw new ArgumentException("The provided attachment is invalid", nameof(base64Image));
            }

            var fileName = Path.GetRandomFileName();
            var filePath = Path.Combine(_attachmentOptions.Value.SystemPath, fileName);
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            File.WriteAllBytes(filePath, imageBytes);

            var attachment = new Attachment()
            {
                Name = fileName
            };

            _context.Attachments.Add(attachment);

            return attachment;
        }

        public byte[] GetAttachmentContents(Attachment attachment)
        {
            if (attachment == null)
            {
                throw new NullReferenceException(nameof(attachment));
            }

            var fileName = attachment.Name;
            var filePath = Path.Combine(_attachmentOptions.Value.SystemPath, fileName);

            return File.ReadAllBytes(filePath);
        }

        public void DeleteAttachment(Attachment attachment)
        {
            if (attachment == null)
            {
                throw new NullReferenceException(nameof(attachment));
            }

            var fileName = attachment.Name;
            var filePath = Path.Combine(_attachmentOptions.Value.SystemPath, fileName);

            File.Delete(filePath);

            _context.Attachments.Remove(attachment);
        }
    }
}