using System.Collections.Generic;
using Servidor.Persistence;

namespace Servidor.Services
{
    public interface IGenericService<T> where T : class
    {
        IEnumerable<T> GetAll();

        T GetById(long id, bool asNoTracking = true);

        void Add(T entity);

        void Update(T entity);

        void Delete(long id);
    }
}