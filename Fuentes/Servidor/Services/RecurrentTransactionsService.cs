using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class RecurrentTransactionsService : IRecurrentTransactionsService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly ICategoryDefinitionsService _categoryDefinitionsService;
        private readonly ICategoriesService _categoriesService;
        private readonly IAccountsService _accountsService;
        private readonly ITransactionsService _transactionsService;

        public RecurrentTransactionsService(
            FinancesEverywhereContext context,
            ICategoryDefinitionsService categoryDefinitionsService,
            ICategoriesService categoriesService,
            IAccountsService accountsService,
            ITransactionsService transactionsService)
        {
            _context = context;
            _categoryDefinitionsService = categoryDefinitionsService;
            _categoriesService = categoriesService;
            _accountsService = accountsService;
            _transactionsService = transactionsService;
        }

        public IQueryable<RecurrentTransaction> GetAll()
        {
            return _context.RecurrentTransactions
                        .Include(r => r.Account)
                        .Include(r => r.CategoryDefinition)
                        .OrderBy(x => x.Id);
        }

        public RecurrentTransaction GetById(long recurrentTransactionId)
        {
            var recurrentTransaction = GetAll().FirstOrDefault(t => t.Id == recurrentTransactionId);

            if (recurrentTransaction == null)
            {
                throw new ArgumentException($"Recurrent Transaction with Id {recurrentTransactionId} was not found");
            }
            return recurrentTransaction;
        }

        public RecurrentTransaction Create(RecurrentTransactionCreationDTO creationData)
        {
            var account = _accountsService.GetById(creationData.AccountId);
            var categoryDefinition = _categoryDefinitionsService.GetById(creationData.CategoryDefinitionId);

            var frequency = new Frequency
            {
                ValidFrom = creationData.ValidFrom,
                ValidUntil = creationData.ValidUntil,
                Offset = creationData.Offset,
                PeriodLength = creationData.Period
            };

            var recurrentTransaction = new RecurrentTransaction
            {
                Title = creationData.Title,
                Expenditure = creationData.Expenditure,
                Amount = creationData.Amount,
                Active = true,
                Account = account,
                Frequency = frequency,
                CategoryDefinition = categoryDefinition,
            };

            var transactions = _transactionsService.CreateTransactionsForRecurrentTransactions(recurrentTransaction);

            foreach (var transaction in transactions)
            {
                recurrentTransaction.AddTransaction(_transactionsService.Create(transaction));
            }

            _context.RecurrentTransactions.Add(recurrentTransaction);
            _context.SaveChanges();
            
            return recurrentTransaction;
        }

        public RecurrentTransaction Update(long recurrentTransactionId, RecurrentTransactionUpdateDTO updateData)
        {
            var recurrentTransaction = GetById(recurrentTransactionId);

            recurrentTransaction.Title = updateData.Title;
            recurrentTransaction.Active = updateData.Active;

            _context.RecurrentTransactions.Update(recurrentTransaction);
            _context.SaveChanges();

            return recurrentTransaction;
        }

        public void Delete(long recurrentTransactionId)
        {
            var recurrentTransaction = GetById(recurrentTransactionId);
            
            _context.RecurrentTransactions.Remove(recurrentTransaction);
            _context.SaveChanges();
        }
    }
}