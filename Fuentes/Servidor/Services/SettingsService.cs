using System.Collections.Generic;
using System.Linq;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Servidor.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly FinancesEverywhereContext _context;

        public SettingsService(FinancesEverywhereContext context)
        {
            _context = context;
        }

        public void Add(Setting entity)
        {
            _context.Settings.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(long id)
        {
            var setting = _context.Settings.FirstOrDefault(t => t.Id == id);
            if (setting != null)
            {
                _context.Settings.Remove(setting);
                _context.SaveChanges();
            }
        }

        public IEnumerable<Setting> GetAll()
        {
            return _context.Settings.ToList();
        }

        public Setting GetById(long id, bool asNoTracking = true)
        {
            var settings = _context.Settings.AsQueryable();

            if (asNoTracking)
            {
                settings = settings.AsNoTracking().AsQueryable();
            }
            return settings.FirstOrDefault(t => t.Id == id);
        }

        public Setting GetByKey(string settingKey)
        {
            return _context.Settings.FirstOrDefault(x => x.SettingKey == settingKey);
        }

        public void Update(Setting entity)
        {
            var setting = _context.Settings.FirstOrDefault(t => t.Id == entity.Id);
            if (setting != null)
            {
                _context.Settings.Update(setting);
                _context.SaveChanges();
            }
        }
    }
}