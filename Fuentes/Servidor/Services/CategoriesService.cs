using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using Servidor.DataTransfer;
using Servidor.Models;
using Servidor.Models.Enums;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class CategoriesService : ICategoriesService
    {
        private readonly FinancesEverywhereContext _context;

        public CategoriesService(
            FinancesEverywhereContext context
            )
        {
            _context = context;
        }

        public IQueryable<Category> GetAll()
        {
            return _context.Categories
                .Include(x => x.CategoryDefinition)
                .ThenInclude(x => x.Frequency)
                .OrderBy(x => x.Id);
        }

        public Category GetById(long categoryId)
        {
            var category = GetAll()
                .FirstOrDefault(t => t.Id == categoryId);

            if (category == null)
            {
                throw new ArgumentException($"Category with Id {category} was not found");
            }

            return category;
        }

        public Category Update(long categoryId, CategoryUpdateDTO updateData)
        {
            var category = GetById(categoryId);

            category.Budget = updateData.Budget;
            category.Notes = updateData.Notes;
            category.Active = updateData.Active;

            _context.Categories.Update(category);
            _context.SaveChanges();

            return category;
        }

        public IEnumerable<Category> CreateCategoriesForCategoryDefinition(CategoryDefinition categoryDefinition)
        {
            var categories = new List<Category>();

            var daysSpan = (categoryDefinition.Frequency.ValidUntil - categoryDefinition.Frequency.ValidFrom).TotalDays;

            // Redondear hacia arriba el numero de periodos asi podemos hacer un for
            var nOfPeriodCategories = Math.Truncate(daysSpan / categoryDefinition.Frequency.PeriodLength.ToDays());

            if (nOfPeriodCategories == 0)
            {
                var newCategory = new Category();

                newCategory.Budget = categoryDefinition.Budget;
                newCategory.ValidFrom = categoryDefinition.Frequency.ValidFrom;
                newCategory.ValidUntil = categoryDefinition.Frequency.ValidUntil;
                newCategory.Active = true;

                categories.Add(newCategory);
            }
            else
            {
                DateTime initialDate = categoryDefinition.Frequency.ValidFrom;
                DateTime finalDate = categoryDefinition.Frequency.ValidFrom.AddDays(categoryDefinition.Frequency.PeriodLength.ToDays() - 1);

                for (int i = 0; i < nOfPeriodCategories; i++)
                {
                    var newCategory = new Category();
                    newCategory.CategoryDefinition = categoryDefinition;
                    newCategory.Budget = categoryDefinition.Budget;
                    newCategory.ValidFrom = initialDate;
                    newCategory.ValidUntil = finalDate;
                    newCategory.Active = true;

                    initialDate = finalDate.AddDays(1);
                    finalDate = initialDate.AddDays(categoryDefinition.Frequency.PeriodLength.ToDays() - 1);

                    categories.Add(newCategory);
                }
            }

            return categories;
        }


    }
}