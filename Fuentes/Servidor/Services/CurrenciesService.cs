using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Servidor.Models;
using Servidor.Persistence;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class CurrenciesService : ICurrenciesService
    {
        private readonly FinancesEverywhereContext _context;
        private readonly ISettingsService _settingsService;

        public CurrenciesService(FinancesEverywhereContext context, ISettingsService settingService)
        {
            _context = context;
            _settingsService = settingService;
        }

        public IQueryable<Currency> GetAll()
        {
            return _context.Currencies;
        }

        public Currency GetById(long id)
        {
            Currency currency = _context.Currencies.Find(id);
            if (currency == null)
            {
                currency = GetDefaultCurrency();
            }
            return currency;
        }

        public Currency GetDefaultCurrency()
        {
            var defaultCurrencySetting = _settingsService.GetByKey("default-currency");

            if (defaultCurrencySetting != null)
            {
                if (long.TryParse(defaultCurrencySetting.SettingValue, out var currencyId))
                {
                    return GetById(currencyId);
                }
            }

            return null;
        }
    }
}