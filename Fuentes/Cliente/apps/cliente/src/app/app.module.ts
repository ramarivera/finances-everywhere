import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app-component/app.component';
import { AppDataAccessModule } from './app-data-access.module';
import { AppRoutingModule } from './app-routing.module';
import { AppUiModule } from './app-ui.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppDataAccessModule, AppRoutingModule, AppUiModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
