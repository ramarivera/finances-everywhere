import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SharedDataAccessAccountsModule } from '@dacs-fe/shared/data-access/accounts';
import { SharedDataAccessCategoriesModule } from '@dacs-fe/shared/data-access/categories';
import { SharedDataAccessCategoryDefinitionsModule } from '@dacs-fe/shared/data-access/category-definitions';
import { SharedDataAccessCurrenciesModule } from '@dacs-fe/shared/data-access/currencies';
import { SharedDataAccessDepartmentsModule } from '@dacs-fe/shared/data-access/departments';
import { SharedDataAccessTransactionsModule } from '@dacs-fe/shared/data-access/transactions';
import { BaseApiUrl } from '@dacs-fe/utils/environment';
import { EffectsModule } from '@ngrx/effects';
import { routerReducer, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NxModule } from '@nrwl/angular';
import { environment } from '../environments/environment';

@NgModule({
  imports: [
    HttpClientModule,
    SharedDataAccessCategoryDefinitionsModule,
    SharedDataAccessAccountsModule,
    SharedDataAccessCategoriesModule,
    SharedDataAccessCurrenciesModule,
    SharedDataAccessDepartmentsModule,
    SharedDataAccessTransactionsModule,
    NxModule.forRoot(),
    StoreModule.forRoot(
      {
        router: routerReducer,
      },
      {
        initialState: {},
        runtimeChecks: {
          // state cannot be inmutable due to using full router state AND ivy
          strictStateImmutability: false,
          // actions can be mutated due to it being not applied for router actions as per https://github.com/nrwl/nx/issues/1538
          strictActionImmutability: false,
          // state cannot be marked as unserializable when using full router state AND ivy, but we need full router serializer for NX Navigation
          strictStateSerializability: false, // Due to RouterState,
          // actions can be not serializable due to it being not applied for router actions as per https://github.com/nrwl/nx/issues/1538
          strictActionSerializability: false, // Due to RouterState,
        },
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Full,
      stateKey: 'router',
    }),
  ],
  providers: [{ provide: BaseApiUrl, useValue: environment.baseApiUrl }],
  exports: [
    HttpClientModule,
    SharedDataAccessCategoryDefinitionsModule,
    SharedDataAccessAccountsModule,
    SharedDataAccessCategoriesModule,
    SharedDataAccessCurrenciesModule,
    SharedDataAccessDepartmentsModule,
    SharedDataAccessTransactionsModule,
  ],
})
export class AppDataAccessModule {}
