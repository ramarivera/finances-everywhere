import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import {
  TransactionsFeatureShellComponent,
  TransactionsFeatureShellModule,
} from '@dacs-fe/transactions/feature-shell';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  {
    path: 'category-definitions',
    loadChildren: () =>
      import('@dacs-fe/category-definitions/feature-shell').then(
        (m) => m.CategoryDefinitionsFeatureShellModule
      ),
  },
  {
    path: 'departments',
    loadChildren: () =>
      import('@dacs-fe/departments/feature-shell').then((m) => m.DepartmentsFeatureShellModule),
  },
  {
    path: 'accounts',
    loadChildren: () =>
      import('@dacs-fe/accounts/feature-shell').then((m) => m.AccountsFeatureShellModule),
  },
  // // Transactions is not lazily imported cause the FAB button on the main shell leads right to it
  // {
  //   path: 'transactions',
  //   component: TransactionsFeatureShellComponent,
  // },
  {
    path: '**',
    loadChildren: () => import('@dacs-fe/shared/errors').then((x) => x.SharedErrorsModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes), TransactionsFeatureShellModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
