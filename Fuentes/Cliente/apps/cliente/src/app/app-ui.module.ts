import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedUiDialogsModule } from '@dacs-fe/shared/ui/dialogs';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { SharedUiSnackBarsModule } from '@dacs-fe/shared/ui/snackbars';
import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
  imports: [
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    SharedUiDialogsModule,
    SharedUiSnackBarsModule,
    BrowserAnimationsModule,
  ],
  declarations: [HomePageComponent],
  exports: [
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    SharedUiDialogsModule,
    SharedUiSnackBarsModule,
    BrowserAnimationsModule,
    HomePageComponent,
  ],
})
export class AppUiModule {}
