import { Component, OnInit } from '@angular/core';
import { FabButtonService } from '@dacs-fe/shared/ui/buttons';
import { TransactionsFeatureShellFacade } from '@dacs-fe/transactions/feature-shell';

@Component({
  selector: 'dacs-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  buttonSubscription!: () => void;

  constructor(
    private fabButtonService: FabButtonService,
    private facade: TransactionsFeatureShellFacade
  ) {}

  ngOnInit() {
    this.buttonSubscription = this.fabButtonService.addButton({
      icon: 'money',
      tooltip: 'New transaction',
      callback: () => this.facade.newTransaction(),
      color: 'primary',
    });
  }
}
