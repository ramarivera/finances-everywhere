module.exports = {
  addons: [
    '@storybook/addon-knobs',
    '@storybook/addon-actions',
    {
      name: '@storybook/addon-docs/preset',
      options: {
        configureJSX: true,
        babelOptions: {},
      },
    },
  ],
};
