import { addDecorator, addParameters } from '@storybook/angular';
import { withKnobs } from '@storybook/addon-knobs';
import { DocsPage } from '@storybook/addon-docs/blocks';

import { setCompodocJson } from '@storybook/addon-docs/angular';
import '@storybook/addon-console';
// import { centered } from '@storybook/addon-centered/angular';

import docJson from '../documentation.json';
import centered from './centered';

addDecorator(withKnobs);

addParameters({
  docs: DocsPage,
});

setCompodocJson(docJson);

// addDecorator(centered);
