import { makeDecorator, StoryFn } from '@storybook/addons';
import { IStory } from '@storybook/angular';

const parameters = {
  name: 'centered',
  parameterName: 'centered',
} as const;

const styles = {
  style: {
    position: 'fixed',
    top: '0',
    left: '0',
    bottom: '0',
    right: '0',
    display: 'flex',
    alignItems: 'center',
    overflow: 'auto',
  },
  innerStyle: {
    margin: 'auto',
    maxHeight: '100%', // Hack for centering correctly in IE11
  },
} as const;

function getComponentSelector(component: any) {
  // eslint-disable-next-line no-underscore-dangle
  return component.__annotations__[0].selector;
}

function getTemplate(metadata: IStory) {
  let tpl = '';
  if (metadata.component) {
    const selector = getComponentSelector(metadata.component);

    let props = '';

    if (metadata.props) {
      props = Object.keys(metadata.props).reduce((propsString, key) => {
        propsString += `[${key}]="${key}" `;
        return propsString;
      }, '');
    }

    tpl = `<${selector} ${props}></${selector}>`;
  }

  if (metadata.template) {
    tpl = metadata.template;
  }

  return `
      <div [ngStyle]="styles.style">
        <div [ngStyle]="styles.innerStyle">
          ${tpl}
        </div>
      </div>`;
}

function getModuleMetadata(metadata: IStory) {
  const { moduleMetadata, component } = metadata;

  if (component && !moduleMetadata) {
    return {
      declarations: [metadata.component],
    };
  }

  if (component && moduleMetadata) {
    return {
      ...moduleMetadata,
      declarations: [...moduleMetadata.declarations, metadata.component],
    };
  }

  return moduleMetadata;
}

function centered(metadataFn: StoryFn<IStory>) {
  const metadata = metadataFn();

  return {
    ...metadata,
    template: getTemplate(metadata),
    moduleMetadata: getModuleMetadata(metadata),
    props: {
      ...metadata.props,
      styles,
    },
  };
}

export default makeDecorator({
  ...parameters,
  wrapper: (getStory) => centered(getStory as StoryFn),
});

if (module && module.hot && module.hot.decline) {
  module.hot.decline();
}
