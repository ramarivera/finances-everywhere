import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportingFeatureShellComponent } from './reporting-feature-shell.component';

describe('ReportingFeatureShellComponent', () => {
  let component: ReportingFeatureShellComponent;
  let fixture: ComponentFixture<ReportingFeatureShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportingFeatureShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportingFeatureShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
