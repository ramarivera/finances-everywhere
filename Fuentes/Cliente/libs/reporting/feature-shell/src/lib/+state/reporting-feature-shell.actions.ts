import { createAction } from '@ngrx/store';

export const reportingFeatureActivated = createAction(
  '[Reporting/Feature] reporting feature activated'
);

export const newReport = createAction('[Reporting/Feature] new report');
