import { Injectable } from '@angular/core';
import { accountsQuery, fetchAccounts } from '@dacs-fe/shared/data-access/accounts';
import { categoriesQuery, fetchCategories } from '@dacs-fe/shared/data-access/categories';
import { currenciesQuery, fetchCurrencies } from '@dacs-fe/shared/data-access/currencies';
import { ReportingPartialState } from '@dacs-fe/shared/data-access/reporting';
import { fetchTransactions, transactionsQuery } from '@dacs-fe/shared/data-access/transactions';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { from } from 'rxjs';
import { ReportingFeatureShellComponent } from '../reporting-feature-shell/reporting-feature-shell.component';
import { reportingFeatureActivated } from './reporting-feature-shell.actions';

import {
  fetchCategoryDefinitions,
  categoryDefinitionsQuery,
} from '@dacs-fe/shared/data-access/category-definitions';

@Injectable()
export class ReportingFeatureShellEffects {
  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<ReportingPartialState>,
    private dialogService: DialogService,
    private snackbarService: SnackBarService
  ) {}

  reportingNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(ReportingFeatureShellComponent, {
      run: () => reportingFeatureActivated(),
    })
  );

  reportingFeatureActivated$ = createEffect(() =>
    this.dataPersistence.fetch(reportingFeatureActivated, {
      run: (_, state) => {
        if (!state) {
          return;
        }
        const actions = [];

        if (!transactionsQuery.getBatchLoading(state)) {
          actions.push(fetchTransactions());
        }

        if (!accountsQuery.getBatchLoading(state)) {
          actions.push(fetchAccounts());
        }

        if (!currenciesQuery.getBatchLoading(state)) {
          actions.push(fetchCurrencies());
        }

        if (!categoryDefinitionsQuery.getBatchLoading(state)) {
          actions.push(fetchCategoryDefinitions());
        }

        if (!categoriesQuery.getBatchLoading(state)) {
          actions.push(fetchCategories());
        }

        if (actions.length) {
          return from(actions);
        }
      },
    })
  );
}
