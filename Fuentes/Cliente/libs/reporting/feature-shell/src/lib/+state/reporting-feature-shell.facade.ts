import { Injectable } from '@angular/core';
import { ReportingFacade } from '@dacs-fe/shared/data-access/reporting';
  
@Injectable()
export class ReportingFeatureShellFacade extends ReportingFacade {}
