import { accountsQuery } from '@dacs-fe/shared/data-access/accounts';
import { categoriesQuery } from '@dacs-fe/shared/data-access/categories';
import { currenciesQuery } from '@dacs-fe/shared/data-access/currencies';
import { reportingQuery } from '@dacs-fe/shared/data-access/reporting';
import { transactionsQuery } from '@dacs-fe/shared/data-access/transactions';
import { createSelector } from '@ngrx/store';
import * as _ from 'lodash';
import {
  TransactionListViewModel,
  TransactionReportFilterViewModel,
  TransactionReportViewModel,
  ReportTransactionViewModel,
} from '@dacs-fe/types';

function hasValues(array: any[] | null | undefined) {
  return Boolean(array !== null && array !== undefined && array.length);
}

export const getTransactionReportFilterViewModels = createSelector(
  accountsQuery.getAllAccounts,
  categoriesQuery.getAllCategories,
  currenciesQuery.getAllCurrencies,
  reportingQuery.getAllFilters,
  (accounts, categories, currencies, filters) => {
    if (![accounts, currencies, categories, filters].every(hasValues)) {
      return [];
    }

    return filters.map(filter => {
      return {
        eventId: filter.eventId,
        from: filter.from,
        until: filter.until,
        accounts: (filter.accountIds || []).map(accountId => _.find(accounts, { accountId })),
        categories: (filter.categoryIds || []).map(categoryId =>
          _.find(categories, { categoryId })
        ),
        currency: _.find(currencies, { currencyId: filter.currencyId }),
      } as TransactionReportFilterViewModel;
    });
  }
);

export const getSelectedTransactionReportViewModel = createSelector(
  accountsQuery.getAllAccounts,
  currenciesQuery.getAllCurrencies,
  categoriesQuery.getAllCategories,
  reportingQuery.getSelectedReport,
  getTransactionReportFilterViewModels,
  (accounts, currencies, categories, selectedReport, reportFilterViewModels) => {
    if (![accounts, currencies, categories].every(hasValues) || !selectedReport) {
      return null;
    }

    const { eventId, summary, reportTransactions } = selectedReport;

    return {
      summary,
      reportTransactions: reportTransactions.map(
        ({ account, category, ...transaction }) =>
          ({
            ...transaction,
            account: _.find(accounts, { accountId: account.id }),
            category: _.find(categories, { categoryId: category.id }),
          } as ReportTransactionViewModel)
      ),
      filter: _.find(reportFilterViewModels, { eventId }),
    } as TransactionReportViewModel;
  }
);



export const getTransactionListViewModels = createSelector(
  transactionsQuery.getAllTransactions,
  accountsQuery.getAllAccounts,
  (transactions, accounts) => {
    return transactions.map(transaction => {
      return {
        ...transaction,
        account: accounts[transaction.accountId],
      } as TransactionListViewModel;
    });
  }
);
