import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportPageComponent } from './report-page/report-page.component';
import { ReportingFeatureShellComponent } from './reporting-feature-shell/reporting-feature-shell.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ReportPageComponent, ReportingFeatureShellComponent],
})
export class ReportingFeatureShellModule {}
