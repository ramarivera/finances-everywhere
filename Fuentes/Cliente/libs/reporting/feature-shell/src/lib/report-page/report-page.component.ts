import { Component, OnInit } from '@angular/core';
import { ReportingFeatureShellFacade } from '../+state/reporting-feature-shell.facade';

@Component({
  selector: 'dacs-fe-report-page',
  templateUrl: './report-page.component.html',
  styleUrls: ['./report-page.component.scss'],
})
export class ReportPageComponent implements OnInit {
  constructor(private reportingFeatureShellFacade: ReportingFeatureShellFacade) {}

  ngOnInit() {}
}
