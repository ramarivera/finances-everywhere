import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  buildField,
  buildGroup,
  buildOptionsFromObjects,
  buildDefaultEndDateValidator,
} from '@dacs-fe/shared/forms';
import {
  Account,
  Currency,
  Omit,
  TransactionReportFilter,
  CategoryDefinition,
} from '@dacs-fe/types';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  OnInit,
} from '@angular/core';

interface ReportFilterData extends Omit<TransactionReportFilter, 'from' | 'until'> {
  dates: Pick<TransactionReportFilter, 'from' | 'until'>;
}

@Component({
  selector: 'dacs-fe-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.scss'],
})
export class ReportFilterComponent implements OnInit, OnChanges {
  @Input()
  currencies: Currency[] | undefined;

  @Input()
  categoryDefinitions: CategoryDefinition[] | undefined;

  @Input()
  accounts: Account[] | undefined;

  @Output()
  save = new EventEmitter<TransactionReportFilter>();

  @Output()
  cancel = new EventEmitter<void>();

  reportFilterForm = new FormGroup({});

  reportFilterData: Partial<ReportFilterData> = {};

  fields!: FormlyFieldConfig[];

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  ngOnChanges(changes: SimpleChanges) {
    let shouldReload = false;
    if (changes.categoryDefinitions && !changes.categoryDefinitions.isFirstChange()) {
      shouldReload = true;
    }
    if (changes.accounts && !changes.accounts.isFirstChange()) {
      shouldReload = true;
    }
    if (changes.currencies && !changes.currencies.isFirstChange()) {
      shouldReload = true;
    }

    if (shouldReload) {
      this.fields = this.buildFieldsConfiguration();
    }
  }

  private buildFieldsConfiguration() {
    return [
      buildField<ReportFilterData>(
        { key: 'accountIds', label: 'Accounts', type: 'select', requiredName: 'Accounts' },
        {
          options: buildOptionsFromObjects(this.accounts, d => d.name, d => d.accountId),
        },
        {
          expressionProperties: {
            'templateOptions.disabled': () => !this.accounts || !this.accounts.length,
          },
        }
      ),
      buildField<ReportFilterData>(
        { key: 'categoryIds', label: 'Categories', type: 'select', requiredName: 'Categories' },
        {
          options: buildOptionsFromObjects(
            this.categoryDefinitions,
            d => d.name,
            d => d.categoryDefinitionId
          ),
        },
        {
          expressionProperties: {
            'templateOptions.disabled': () =>
              !this.categoryDefinitions || !this.categoryDefinitions.length,
          },
        }
      ),
      buildField<ReportFilterData>(
        {
          key: 'currencyId',
          label: 'Currency code',
          type: 'select',
        },
        {
          options: buildOptionsFromObjects(this.currencies, d => d.isoCode, d => d.currencyId),
        },
        {
          expressionProperties: {
            'templateOptions.disabled': () => !this.currencies || !this.currencies.length,
          },
        }
      ),
      buildGroup<ReportFilterData, 'dates'>(
        'dates',
        [
          {
            key: 'from',
            label: 'From',
            type: 'datepicker',
            requiredName: 'From',
          },
          {
            key: 'until',
            label: 'Until',
            type: 'datepicker',
            requiredName: 'Until',
          },
        ],
        {
          ...buildDefaultEndDateValidator({ validatorName: 'untilGreater' }),
        }
      ),
    ];
  }

  onSubmit() {
    if (this.reportFilterForm.valid) {
      const reportFilter: TransactionReportFilter = {
        ...this.reportFilterData,
        ...this.reportFilterData.dates,
      };
      this.save.emit(reportFilter);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
