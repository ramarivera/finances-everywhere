import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportFilterDescriptionComponent } from './report-filter-description.component';

describe('ReportFilterDescriptionComponent', () => {
  let component: ReportFilterDescriptionComponent;
  let fixture: ComponentFixture<ReportFilterDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportFilterDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportFilterDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
