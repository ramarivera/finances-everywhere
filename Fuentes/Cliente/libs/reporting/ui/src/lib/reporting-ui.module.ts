import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportDashboardComponent } from './report-dashboard/report-dashboard.component';
import { ReportDetailsComponent } from './report-details/report-details.component';
import { ReportFilterComponent } from './report-filter/report-filter.component';
import { ReportsListComponent } from './reports-list/reports-list.component';
import { ReportFilterDescriptionComponent } from './report-filter-description/report-filter-description.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ReportDashboardComponent, ReportDetailsComponent, ReportFilterComponent, ReportsListComponent, ReportFilterDescriptionComponent],
})
export class ReportingUiModule {}
