export interface Account {
  accountId: number;
  departmentId: number;
  currencyId: number;
  name: string;
  code: string;
  description: string;
}
