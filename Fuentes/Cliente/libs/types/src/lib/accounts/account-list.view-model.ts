import { Currency } from '../currencies/currency.model';
import { Department } from '../departments';
import { Account } from './account.model';

export type AccountListViewModel = Account & {
  department: Department;
  currency: Currency;
};
