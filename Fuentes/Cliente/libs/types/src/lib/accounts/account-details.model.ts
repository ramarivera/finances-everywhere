import { Currency } from '../currencies/currency.model';
import { Department } from '../departments';
import { Account } from './account.model';

export interface AccountDetails extends Account {
  currency: Currency;
  department: Department;
}
