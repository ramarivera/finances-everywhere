export interface AccountUpdateData {
  name: string;
  code: string;
  description: string;
  currencyId: number;
  departmentId: number;
}
