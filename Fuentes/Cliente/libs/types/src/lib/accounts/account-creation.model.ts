
export interface AccountCreationData {
  name: string;
  code?: string;
  description?: string;
  departmentId: number;
  currencyId: number;
}
