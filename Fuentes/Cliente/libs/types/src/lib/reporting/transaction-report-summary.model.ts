export interface TransactionReportSummary {
  income: number;
  expense: number;
  difference: number;
  nOfTransactions: number;
}
