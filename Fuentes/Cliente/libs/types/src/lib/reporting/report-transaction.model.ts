export interface ReportTransaction {
  transactionId: number;
  title: string;
  comment: string;
  amount: number;
  expenditure: boolean;
  occurrence: Date;
  category: {
    id: number;
    description: string;
  };
  account: {
    id: number;
    description: string;
  };
}
