import { Account } from '../../accounts/account.model';
import { Category } from '../../categories/category.model';
import { Currency } from '../../currencies/currency.model';

export interface TransactionReportFilterViewModel {
  eventId: string;
  from?: Date;
  until?: Date;
  accounts?: Account[];
  categories?: Category[];
  currency?: Currency;
}
