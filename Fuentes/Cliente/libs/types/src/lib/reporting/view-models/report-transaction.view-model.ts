import { Category } from '../../categories/category.model';
import { Account } from '../../accounts/account.model';

export interface ReportTransactionViewModel {
  transactionId: number;
  title: string;
  comment: string;
  amount: number;
  expenditure: boolean;
  occurrence: Date;
  category: Category;
  account: Account;
}
