import { TransactionReportSummary } from '../transaction-report-summary.model';
import { ReportTransactionViewModel } from './report-transaction.view-model';
import { TransactionReportFilterViewModel } from './transaction-report-filter.view-model';

export interface TransactionReportViewModel {
  reportTransactions: ReportTransactionViewModel[];
  summary: TransactionReportSummary;
  filter: TransactionReportFilterViewModel;
}
