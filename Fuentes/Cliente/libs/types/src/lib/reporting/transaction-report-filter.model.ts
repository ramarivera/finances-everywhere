export interface TransactionReportFilter {
  from?: Date;
  until?: Date;
  accountIds?: number[];
  categoryIds?: number[];
  currencyId?: number;
}
