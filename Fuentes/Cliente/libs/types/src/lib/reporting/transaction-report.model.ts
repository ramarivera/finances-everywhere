import { ReportTransaction } from './report-transaction.model';
import { TransactionReportSummary } from './transaction-report-summary.model';

export interface TransactionReport {
  reportTransactions: ReportTransaction[];
  summary: TransactionReportSummary;
}
