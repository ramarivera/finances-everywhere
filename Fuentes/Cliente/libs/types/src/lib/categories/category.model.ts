export interface Category {
  categoryId: number;
  categoryDefinitionId: number;
  budget: number;
  validFrom: Date;
  validUntil: Date;
  notes: string;
}
