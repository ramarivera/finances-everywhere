import { Category } from './category.model';

export interface NamedCategory extends Category {
  categoryName: string;
}
