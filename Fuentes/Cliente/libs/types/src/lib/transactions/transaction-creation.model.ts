export interface TransactionCreationData {
  accountId: number;
  categoryDefinitionId: number;
  comment: string;
  title: string;
  expenditure: boolean;
  amount: number;
  occurrence: Date;
  base64ImageAttachment?: string;
}
