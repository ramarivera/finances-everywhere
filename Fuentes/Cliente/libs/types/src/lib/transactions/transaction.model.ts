export interface Transaction {
  transactionId: number;
  accountId: number;
  categoryId: number;
  title: string;
  expenditure: boolean;
  amount: number;
  occurrence: Date;
  hasAttachment: boolean;
}
