import { Account } from './../accounts/account.model';
import { Transaction } from './transaction.model';

export interface TransactionDetails extends Transaction {
  account: Account;
  comment: string;
  base64AttachmentImage?: string;
  isRecurrent: boolean;
}
