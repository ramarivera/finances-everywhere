import { Account } from '../accounts';
import { NamedCategory } from '../categories/named-category.model';
import { Transaction } from './transaction.model';

export type TransactionListViewModel = Transaction & {
  account: Account;
  currencyCode: string;
  category: NamedCategory;
};
