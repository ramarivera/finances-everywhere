export interface TransactionUpdateData {
  comment: string;
  title: string;
  expenditure: boolean;
  amount: number;
  occurrence: Date;
}
