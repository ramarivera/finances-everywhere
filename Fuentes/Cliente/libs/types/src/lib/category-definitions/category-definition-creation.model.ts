import { Period } from '../enums';

export interface CategoryDefinitionCreationData {
  name: string;
  code?: string;
  description?: string;
  budget: number;
  validFrom: Date;
  validUntil: Date;
  offset: number;
  period: Period;
}
