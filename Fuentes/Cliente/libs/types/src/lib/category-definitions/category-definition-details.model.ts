import { Period } from '../enums';
import { CategoryDefinition } from './category-definition.model';

export interface CategoryDefinitionDetails extends CategoryDefinition {
  description?: string;
  validFrom: Date;
  validUntil: Date;
  offset: number;
  period: Period;
  categoryIds?: number[];
}
