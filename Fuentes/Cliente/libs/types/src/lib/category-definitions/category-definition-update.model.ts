export interface CategoryDefinitionUpdateData {
  name: string;
  code: string;
  description: string;
}
