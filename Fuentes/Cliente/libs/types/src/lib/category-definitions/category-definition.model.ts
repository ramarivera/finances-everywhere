export interface CategoryDefinition {
  categoryDefinitionId: number;
  name: string;
  code: string;
  budget: number;
}
