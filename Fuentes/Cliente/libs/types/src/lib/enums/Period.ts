export enum Period {
  None = 0,
  Daily = 1,
  Weekly = 2,
  Monthly = 3,
  Bimonthly = 4,
  FourMonthly = 5,
  Annual = 6,
}
