export interface DepartmentCreationData {
  name: string;
  description?: string;
  parentDepartmentId?: number;
}
