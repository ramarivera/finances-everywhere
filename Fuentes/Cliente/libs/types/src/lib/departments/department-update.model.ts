export interface DepartmentUpdateData {
  name: string;
  description?: string;
  parentDepartmentId?: number;
}
