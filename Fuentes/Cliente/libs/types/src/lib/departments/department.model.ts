export interface Department {
  departmentId: number;
  parentDepartmentId?: number;
  name: string;
  description: string;
}
