export enum PictureSourceTypes {
  None = 0,
  Photo = 1,
  Upload = 2,
  URL = 3,
}
