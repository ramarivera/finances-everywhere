import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AttachmentsUiModule } from '@dacs-fe/attachments/ui';
import { SharedFormsModule } from '@dacs-fe/shared/forms';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { SharedTablesModule } from '@dacs-fe/shared/ui/tables';
import { TransactionCreationComponent } from './transaction-creation/transaction-creation.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionUpdateComponent } from './transaction-update/transaction-update.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedFormsModule,
    SharedUiLoadingModule,
    SharedTablesModule,
    SharedUiLayoutModule,
    AttachmentsUiModule,
  ],
  declarations: [
    TransactionListComponent,
    TransactionCreationComponent,
    TransactionUpdateComponent,
  ],
  exports: [TransactionListComponent, TransactionCreationComponent, TransactionUpdateComponent],
})
export class TransactionsUiModule {}
