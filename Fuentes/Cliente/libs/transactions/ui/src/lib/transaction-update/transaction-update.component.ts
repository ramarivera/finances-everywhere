import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TransactionEntity } from '@dacs-fe/shared/data-access/transactions';
import { buildField } from '@dacs-fe/shared/forms';
import { Omit, TransactionUpdateData } from '@dacs-fe/types';
import { isDetailsEntity } from '@dacs-fe/utils/state';
import { FormlyFieldConfig } from '@ngx-formly/core';

interface TransactionUpdateFormData extends Omit<TransactionUpdateData, 'expenditure'> {
  expenditure: 1 | 2;
}

@Component({
  selector: 'dacs-fe-transaction-update',
  templateUrl: './transaction-update.component.html',
  styleUrls: ['./transaction-update.component.css'],
})
export class TransactionUpdateComponent implements OnChanges {
  @Input()
  transaction!: TransactionEntity;

  @Input()
  readonly = false;

  @Output()
  save = new EventEmitter<TransactionUpdateData>();

  @Output()
  cancel = new EventEmitter<void>();

  transactionUpdateForm = new FormGroup({});

  transactionUpdateData: Partial<TransactionUpdateFormData> = {
    title: '',
    comment: '',
  };

  fields: FormlyFieldConfig[] = [
    buildField<TransactionUpdateFormData>(
      { key: 'title', requiredName: 'Transaction title' },
      { maxLength: 100 }
    ),
    buildField<TransactionUpdateFormData>({ key: 'comment', type: 'textarea' }, { maxLength: 512 }),
    buildField<TransactionUpdateFormData>(
      {
        key: 'amount',
        label: 'Amount',
        type: 'input',
        requiredName: 'Amount',
      },
      {
        type: 'number',
        addonLeft: {
          icon: 'attach_money',
        },
      }
    ),
    buildField<TransactionUpdateFormData>({
      key: 'occurrence',
      label: 'Occurrence',
      type: 'datepicker',
      requiredName: 'Occurrence',
    }),
    buildField<TransactionUpdateFormData>(
      {
        key: 'expenditure',
        label: 'Type',
        type: 'radio',
        requiredName: 'Occurrence',
      },
      {
        options: [
          { value: 1, label: 'Expenditure' },
          { value: 2, label: 'Income' },
        ],
      }
    ),
  ];

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    if (changes.transaction && changes.transaction.currentValue) {
      this.updateForm(this.transaction);
    }
    if (changes.readonly && changes.readonly.currentValue != null) {
      this.setReadonly(this.readonly);
    }
  }

  updateForm(transaction: TransactionEntity) {
    if (isDetailsEntity(transaction)) {
      this.transactionUpdateForm.patchValue({
        title: transaction.title,
        comment: transaction.comment,
        occurrence: transaction.occurrence,
        amount: transaction.amount,
        expenditure: transaction.expenditure ? 1 : 2,
      });
    }
  }

  setReadonly(isReadonly: boolean) {
    if (isReadonly) {
      this.transactionUpdateForm.disable();
    } else {
      this.transactionUpdateForm.enable();
    }
  }

  onSubmit() {
    if (this.transactionUpdateForm.valid) {
      const transactionUpdate = {
        ...this.transactionUpdateData,
        expenditure: this.transactionUpdateData.expenditure === 1,
      };

      this.save.emit(transactionUpdate as TransactionUpdateData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
