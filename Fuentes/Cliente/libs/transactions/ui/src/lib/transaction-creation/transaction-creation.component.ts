import { FormGroup } from '@angular/forms';
import { buildField, buildOptionsField, buildOptionsFromObjects } from '@dacs-fe/shared/forms';
import { Account, CategoryDefinition, Omit, TransactionCreationData } from '@dacs-fe/types';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';

interface TransactionCreationFormData extends Omit<TransactionCreationData, 'expenditure'> {
  expenditure: 1 | 2;
}

@Component({
  selector: 'dacs-fe-transaction-creation',
  templateUrl: './transaction-creation.component.html',
  styleUrls: ['./transaction-creation.component.css'],
  exportAs: 'transactionCreation',
})
export class TransactionCreationComponent implements OnInit {
  @ViewChild('saveButton', { static: true, read: TemplateRef })
  saveButton!: TemplateRef<any>;

  @ViewChild('cancelButton', { static: true, read: TemplateRef })
  cancelButton!: TemplateRef<any>;

  @Input()
  accounts: Account[] | undefined;

  @Input()
  categoryDefinitions: CategoryDefinition[] | undefined;

  @Input()
  displayButtons = true;

  @Output()
  save = new EventEmitter<TransactionCreationData>();

  @Output()
  cancel = new EventEmitter<void>();

  activePanel: 'left' | 'right' = 'left';

  transactionCreationForm = new FormGroup({});

  // Made a partial in order to handle not available values
  transactionCreationData: Partial<TransactionCreationFormData>;
  private attachedBase64Image = '';

  fields!: FormlyFieldConfig[];

  constructor() {
    this.transactionCreationData = {
      title: '',
      comment: '',
      expenditure: 1,
    };
  }

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  private buildFieldsConfiguration() {
    return [
      buildField<TransactionCreationFormData>(
        { key: 'title', requiredName: 'Title' },
        { maxLength: 100 }
      ),
      buildField<TransactionCreationFormData>(
        { key: 'comment', type: 'textarea' },
        { maxLength: 512 }
      ),
      buildOptionsField<TransactionCreationFormData, CategoryDefinition>(
        {
          key: 'categoryDefinitionId',
          label: 'Category',
          requiredName: 'Category',
        },
        () => this.categoryDefinitions,
        (d) => d.name,
        (d) => d.categoryDefinitionId
      ),
      buildOptionsField<TransactionCreationFormData, Account>(
        {
          key: 'accountId',
          label: 'Account',
          requiredName: 'Account',
        },
        () => this.accounts,
        (d) => d.name,
        (d) => d.accountId
      ),
      buildField<TransactionCreationFormData>(
        {
          key: 'amount',
          label: 'Amount',
          type: 'input',
          requiredName: 'Amount',
        },
        {
          type: 'number',
          addonLeft: {
            icon: 'attach_money',
          },
        }
      ),
      buildField<TransactionCreationFormData>({
        key: 'occurrence',
        label: 'Occurrence',
        type: 'datepicker',
        requiredName: 'Occurrence',
      }),
      buildField<TransactionCreationFormData>(
        {
          key: 'expenditure',
          label: 'Type',
          type: 'radio',
          requiredName: 'Occurrence',
        },
        {
          options: [
            { value: 1, label: 'Expenditure' },
            { value: 2, label: 'Income' },
          ],
        }
      ),
    ];
  }

  onSubmit() {
    if (this.transactionCreationForm.valid) {
      const transactionCreation = {
        ...this.transactionCreationData,
        expenditure: this.transactionCreationData.expenditure === 1,
      };

      if (this.attachedBase64Image) {
        transactionCreation.base64ImageAttachment = this.attachedBase64Image;
      }

      this.save.emit(transactionCreation as TransactionCreationData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }

  onAttachImageClicked() {
    this.activePanel = 'right';
  }

  onAttachImageBackClicked() {
    this.activePanel = 'left';
  }

  onImageConfirmed(base64Image: string) {
    this.activePanel = 'left';
    this.attachedBase64Image = base64Image;
  }
}
