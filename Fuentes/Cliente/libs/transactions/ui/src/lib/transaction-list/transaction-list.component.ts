import { CommandColumnDef, SimpleColumnDef } from '@dacs-fe/shared/ui/tables';
import { TransactionListViewModel } from '@dacs-fe/types';
import * as moment from 'moment';
import {
  Component,
  Input,
  TemplateRef,
  ViewChild,
  Output,
  EventEmitter,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css'],
})
export class TransactionListComponent implements OnInit {
  @Input()
  transactions?: TransactionListViewModel[];

  @ViewChild('commandsTemplate', { static: true })
  commandsColumn!: TemplateRef<any>;

  @Output()
  delete = new EventEmitter<TransactionListViewModel>();

  @Output()
  update = new EventEmitter<TransactionListViewModel>();

  @Output()
  viewAttachment = new EventEmitter<TransactionListViewModel>();

  columns: (SimpleColumnDef<TransactionListViewModel> | CommandColumnDef)[] = [];

  constructor() {}

  ngOnInit() {
    this.columns = [
      {
        header: 'Title',
        property: 'title',
      },
      {
        header: 'Account',
        property: (transactionViewModel) =>
          `${transactionViewModel.account.code} - ${transactionViewModel.account.name}`,
      },
      {
        header: 'Category',
        property: 'category.categoryName',
      },
      {
        header: 'Occurrence',
        property: 'occurrence',
        format: (ocurrence) => moment(ocurrence).format('MM Do YY, h:mm a'),
      },
      {
        header: 'Amount',
        property: (transactionViewModel) =>
          `(${transactionViewModel.currencyCode}) ${transactionViewModel.amount}`,
      },
      {
        header: 'Actions',
        template: this.commandsColumn,
      },
    ];
  }

  onUpdateClicked(data: TransactionListViewModel) {
    this.update.emit(data);
  }

  onDeleteClicked(data: TransactionListViewModel) {
    this.delete.emit(data);
  }

  onOpenAttachmentClicked(data: TransactionListViewModel) {
    this.viewAttachment.emit(data);
  }
}
