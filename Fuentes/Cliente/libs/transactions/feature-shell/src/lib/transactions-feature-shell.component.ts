import { Component } from '@angular/core';

@Component({
  selector: 'dacs-fe-transactions',
  template: '<router-outlet></router-outlet>',
})
export class TransactionsFeatureShellComponent {
  constructor() {}
}
