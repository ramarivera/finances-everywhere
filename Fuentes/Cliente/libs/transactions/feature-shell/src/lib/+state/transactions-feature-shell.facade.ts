import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { displayTransactionAttachment, newTransaction } from './transactions-feature-shell.actions';
import { getTransactionListViewModels } from './transactions-feature-shell.selectors';
import {
  TransactionsFacade,
  TransactionsPartialState,
} from '@dacs-fe/shared/data-access/transactions';

@Injectable({ providedIn: 'root' })
export class TransactionsFeatureShellFacade extends TransactionsFacade {
  transactionListViewModels$ = this.store.pipe(select(getTransactionListViewModels));

  constructor(protected store: Store<TransactionsPartialState>) {
    super(store);
  }

  newTransaction() {
    this.dispatch(newTransaction());
  }

  displayTransactionAttachment(transactionId: number) {
    this.dispatch(displayTransactionAttachment({ transactionId }));
  }
}
