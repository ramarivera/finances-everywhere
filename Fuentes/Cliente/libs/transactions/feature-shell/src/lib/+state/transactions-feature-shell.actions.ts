import { createAction, props } from '@ngrx/store';

export const transactionsFeatureActivated = createAction(
  '[Transactions/Feature] transactions feature activated'
);

export const loadTransactionForUpdate = createAction(
  '[Transactions/Feature] load transaction for update',
  props<{ transactionId: number }>()
);

export const displayTransactionAttachment = createAction(
  '[Transactions/Feature] display transaction attachment',
  props<{ transactionId: number }>()
);

export const newTransaction = createAction('[Transactions/Feature] new transaction');

export const openTransactionAttachmentDialog = createAction(
  '[Transactions/Feature] open transaction attachment dialog'
);
