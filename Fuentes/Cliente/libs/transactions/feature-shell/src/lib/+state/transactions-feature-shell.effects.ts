import { Injectable } from '@angular/core';
import { accountsQuery, fetchAccounts } from '@dacs-fe/shared/data-access/accounts';
import { categoriesQuery, fetchCategories } from '@dacs-fe/shared/data-access/categories';
import { currenciesQuery, fetchCurrencies } from '@dacs-fe/shared/data-access/currencies';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { TransactionAttachmentViewerComponent } from '../transaction-attachment-viewer/transaction-attachment-viewer.component';
import { TransactionsCreationPageComponent } from '../transactions-creation-page/transactions-creation-page.component';
import { TransactionsFeatureShellComponent } from '../transactions-feature-shell.component';
import { TransactionsUpdatePageComponent } from '../transactions-update-page/transactions-update-page.component';
import {
  transactionsFeatureActivated,
  loadTransactionForUpdate,
  newTransaction,
  displayTransactionAttachment,
  openTransactionAttachmentDialog,
} from './transactions-feature-shell.actions';
import {
  TransactionsPartialState,
  transactionsQuery,
  fetchTransactions,
  createTransactionSuccess,
  selectTransaction,
} from '@dacs-fe/shared/data-access/transactions';
import {
  fetchCategoryDefinitions,
  categoryDefinitionsQuery,
} from '@dacs-fe/shared/data-access/category-definitions';

@Injectable()
export class TransactionsFeatureShellEffects implements OnInitEffects {
  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<TransactionsPartialState>,
    private dialogService: DialogService,
    private snackbarService: SnackBarService
  ) {}

  /** When navigation is triggered at the Shell level, fire the feature activation action */
  transactionsNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(TransactionsFeatureShellComponent, {
      run: () => transactionsFeatureActivated(),
    })
  );

  transactionsFeatureActivated$ = createEffect(() =>
    this.dataPersistence.fetch(transactionsFeatureActivated, {
      run: (_, state) => {
        if (!state) {
          return;
        }
        const actions = [];

        if (!transactionsQuery.getBatchLoading(state)) {
          actions.push(fetchTransactions());
        }

        if (!accountsQuery.getBatchLoading(state)) {
          actions.push(fetchAccounts());
        }

        if (!categoryDefinitionsQuery.getBatchLoading(state)) {
          actions.push(fetchCategoryDefinitions());
        }

        if (!categoriesQuery.getBatchLoading(state)) {
          actions.push(fetchCategories());
        }

        if (!currenciesQuery.getBatchLoading(state)) {
          actions.push(fetchCurrencies());
        }

        if (actions.length) {
          return from(actions);
        }
      },
    })
  );

  transactionsUpdateNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(TransactionsUpdatePageComponent, {
      run: (route) => loadTransactionForUpdate({ transactionId: route.params.id }),
    })
  );

  loadTransactionForUpdate$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof loadTransactionForUpdate>>(
      loadTransactionForUpdate,
      {
        run: ({ transactionId }) => selectTransaction({ transactionId }),
      }
    )
  );

  /** Fires the selection action so the transaction gets loaded and offloads to another thread to open the dialog */
  displayTransactionAttachment$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof displayTransactionAttachment>>(
      displayTransactionAttachment,
      {
        run: ({ transactionId }) =>
          from([selectTransaction({ transactionId }), openTransactionAttachmentDialog()]),
      }
    )
  );

  openTransactionAttachmentDialog$ = createEffect(
    () =>
      this.dataPersistence.fetch(openTransactionAttachmentDialog, {
        run: () => {
          this.dialogService.openDialog(TransactionAttachmentViewerComponent);
        },
      }),
    { dispatch: false }
  );

  newTransaction$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(newTransaction),
        map(() => this.dialogService.openDialog(TransactionsCreationPageComponent))
      ),
    { dispatch: false }
  );

  transactionSuccessfullyCreated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createTransactionSuccess),
        map((action) => {
          this.snackbarService.openSnackBar('success', 'Transaction created');
        })
      ),
    { dispatch: false }
  );

  ngrxOnInitEffects(): import('@ngrx/store').Action {
    return transactionsFeatureActivated();
  }
}
