import { accountsQuery } from '@dacs-fe/shared/data-access/accounts';
import { categoriesQuery } from '@dacs-fe/shared/data-access/categories';
import { currenciesQuery } from '@dacs-fe/shared/data-access/currencies';
import { transactionsQuery } from '@dacs-fe/shared/data-access/transactions';
import { TransactionListViewModel } from '@dacs-fe/types';
import { createSelector } from '@ngrx/store';

export const getTransactionListViewModels = createSelector(
  transactionsQuery.getAllTransactions,
  accountsQuery.getAllAccounts,
  categoriesQuery.getCategoriesWithName,
  currenciesQuery.getAllCurrencies,
  (transactions, accounts, namedCategories, currencies) => {
    if (!accounts?.length || !namedCategories?.length || !currencies?.length) return [];

    return transactions.map((transaction) => {
      const account = accounts[transaction.accountId];
      return {
        ...transaction,
        account: account,
        category: namedCategories[transaction.categoryId],
        currencyCode: currencies.find((x) => x.currencyId === account.currencyId)?.isoCode,
      } as TransactionListViewModel;
    });
  }
);
