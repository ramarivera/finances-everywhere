import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AccountsFacade } from '@dacs-fe/shared/data-access/accounts';
import { CategoryDefinitionsFacade } from '@dacs-fe/shared/data-access/category-definitions';
import { createTransaction, TransactionsFacade } from '@dacs-fe/shared/data-access/transactions';
import { Transaction, TransactionCreationData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-transactions-creation-page',
  templateUrl: './transactions-creation-page.component.html',
  styleUrls: ['./transactions-creation-page.component.scss'],
})
export class TransactionsCreationPageComponent implements OnInit {
  createAnotherTransaction = false;
  accounts$ = this.accountsFacade.allAccounts$;
  categoryDefinitions$ = this.categoryDefinitionsFacade.allCategoryDefinitions$;

  loading$ = combineLatest(
    this.categoryDefinitionsFacade.batchLoading$,
    this.accountsFacade.batchLoading$
  ).pipe(map(loading => loading.some(Boolean)));

  constructor(
    private transactionsFacade: TransactionsFacade,
    private categoryDefinitionsFacade: CategoryDefinitionsFacade,
    private accountsFacade: AccountsFacade,
    private dialogRef: MatDialogRef<TransactionsCreationPageComponent>
  ) {}

  ngOnInit() {}

  onCancel() {
    this.closeDialog();
  }

  onTransactionSave($event: TransactionCreationData) {
    const eventId = this.transactionsFacade.dispatch(createTransaction({ creationData: $event }));

    if (eventId) {
      this.transactionsFacade.allTransactions$
        .pipe(entityByEventId(eventId))
        .subscribe(transaction => this.handleSuccessfulCreation(transaction));
    }
  }

  private handleSuccessfulCreation(transaction: Transaction) {
    if (!this.createAnotherTransaction) {
      this.closeDialog(transaction);
    }
  }

  private closeDialog(transaction?: Transaction) {
    this.dialogRef.close(transaction);
  }
}
