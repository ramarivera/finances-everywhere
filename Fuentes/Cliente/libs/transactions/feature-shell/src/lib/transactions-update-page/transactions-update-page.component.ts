import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionsFacade, updateTransaction } from '@dacs-fe/shared/data-access/transactions';
import { TransactionUpdateData } from '@dacs-fe/types';
import { entityByEventId, isDefined } from '@dacs-fe/utils/state';
import { of } from 'rxjs';
import { filter, map, switchMap, take, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-transactions-update-page',
  templateUrl: './transactions-update-page.component.html',
  styleUrls: ['./transactions-update-page.component.sass'],
})
export class TransactionsUpdatePageComponent {
  loading$ = this.facade.singleLoading$;
  selected$ = this.facade.selectedTransaction$;

  constructor(
    private facade: TransactionsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  onCancel() {
    this.navigateBack();
  }

  onTransactionSave($event: TransactionUpdateData) {
    this.activatedRoute.params
      .pipe(
        take(1),
        withLatestFrom(of($event)),
        map(([params, updateData]) =>
          this.facade.dispatch(updateTransaction({ transactionId: params.id, updateData }))
        ),
        filter(isDefined),
        switchMap((eventId) => this.facade.allTransactions$.pipe(entityByEventId(eventId)))
      )
      .subscribe((updatedTransaction) => {
        this.navigateBack();
      });
  }

  navigateBack() {
    this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
  }
}
