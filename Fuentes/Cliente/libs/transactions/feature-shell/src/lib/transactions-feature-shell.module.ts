import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AttachmentsUiModule } from '@dacs-fe/attachments/ui';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { TransactionsUiModule } from '@dacs-fe/transactions/ui';
import { EffectsModule } from '@ngrx/effects';
import { TransactionAttachmentViewerComponent } from './transaction-attachment-viewer/transaction-attachment-viewer.component';
import { TransactionsCreationPageComponent } from './transactions-creation-page/transactions-creation-page.component';
import { TransactionsDetailsPageComponent } from './transactions-details-page/transactions-details-page.component';
import { TransactionsFeatureShellComponent } from './transactions-feature-shell.component';
import { TransactionsListPageComponent } from './transactions-list-page/transactions-list-page.component';
import { TransactionsUpdatePageComponent } from './transactions-update-page/transactions-update-page.component';
import { TransactionsFeatureShellEffects } from './+state/transactions-feature-shell.effects';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    SharedUiLoadingModule,
    AttachmentsUiModule,
    TransactionsUiModule,
    RouterModule.forChild([
      {
        path: 'transactions',
        component: TransactionsFeatureShellComponent,
        children: [
          { path: '', component: TransactionsListPageComponent },
          { path: 'new', component: TransactionsCreationPageComponent },
          { path: ':id', component: TransactionsDetailsPageComponent },
          {
            path: ':id/edit',
            component: TransactionsUpdatePageComponent,
          },
        ],
      },
    ]),
    EffectsModule.forFeature([TransactionsFeatureShellEffects]),
  ],
  declarations: [
    TransactionsFeatureShellComponent,
    TransactionsListPageComponent,
    TransactionsDetailsPageComponent,
    TransactionsUpdatePageComponent,
    TransactionsCreationPageComponent,
    TransactionAttachmentViewerComponent,
  ],
  entryComponents: [TransactionsCreationPageComponent, TransactionAttachmentViewerComponent],
})
export class TransactionsFeatureShellModule {}
