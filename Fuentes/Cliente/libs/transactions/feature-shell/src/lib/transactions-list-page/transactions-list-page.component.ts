import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { deleteTransaction } from '@dacs-fe/shared/data-access/transactions';
import { FabButtonService } from '@dacs-fe/shared/ui/buttons';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { TransactionListViewModel } from '@dacs-fe/types';
import { filter } from 'rxjs/operators';
import { TransactionsFeatureShellFacade } from '../+state/transactions-feature-shell.facade';

@Component({
  selector: 'dacs-fe-transaction-list-page',
  templateUrl: './transactions-list-page.component.html',
})
export class TransactionsListPageComponent implements OnInit {
  loaded$ = this.facade.hasLoaded$;
  isLoading$ = this.facade.batchLoading$;
  transactions$ = this.facade.transactionListViewModels$;

  constructor(
    private facade: TransactionsFeatureShellFacade,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService
  ) {}

  ngOnInit() {}

  onViewAttachmentClicked(data: TransactionListViewModel) {
    this.facade.displayTransactionAttachment(data.transactionId);
  }

  onUpdate(data: TransactionListViewModel) {
    this.router.navigate([data.transactionId, 'edit'], { relativeTo: this.route.parent });
  }

  onDelete(data: TransactionListViewModel) {
    this.dialogService
      .openConfirmation(
        'Confirm transaction deletion',
        `Are you sure you want to delete the '${data.title}' transaction? This operation cannot be undone`
      )
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.facade.dispatch(deleteTransaction({ transactionId: data.transactionId }));
      });
  }
}
