import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isDefined } from '@dacs-fe/utils/state';
import { filter } from 'rxjs/operators';
import { TransactionsFeatureShellFacade } from '../+state/transactions-feature-shell.facade';

@Component({
  selector: 'dacs-fe-transaction-attachment-viewer',
  templateUrl: './transaction-attachment-viewer.component.html',
  styleUrls: ['./transaction-attachment-viewer.component.css'],
})
export class TransactionAttachmentViewerComponent {
  loading$ = this.transactionsFacade.singleLoading$;
  selectedTransaction$ = this.transactionsFacade.selectedTransaction$.pipe(filter(isDefined));

  constructor(
    private transactionsFacade: TransactionsFeatureShellFacade,
    private dialogRef: MatDialogRef<TransactionAttachmentViewerComponent>
  ) {}

  closeDialog() {
    this.dialogRef.close();
  }

  onCloseClicked() {
    this.closeDialog();
  }
}
