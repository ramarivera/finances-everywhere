import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedFormsModule } from '@dacs-fe/shared/forms';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { SharedTablesModule } from '@dacs-fe/shared/ui/tables';
import { CategoryDefinitionCreationComponent } from './category-definition-creation/category-definition-creation.component';
import { CategoryDefinitionListComponent } from './category-definition-list/category-definition-list.component';
import { CategoryDefinitionUpdateComponent } from './category-definition-update/category-definition-update.component';

@NgModule({
  imports: [CommonModule, SharedUiMaterialModule, SharedFormsModule, SharedUiLoadingModule, SharedTablesModule],
  declarations: [
    CategoryDefinitionListComponent,
    CategoryDefinitionCreationComponent,
    CategoryDefinitionUpdateComponent,
  ],
  exports: [CategoryDefinitionListComponent, CategoryDefinitionCreationComponent, CategoryDefinitionUpdateComponent],
})
export class CategoryDefinitionsUiModule {}
