import { formatCurrency } from '@angular/common';
import { CommandColumnDef, SimpleColumnDef } from '@dacs-fe/shared/ui/tables';
import { CategoryDefinition } from '@dacs-fe/types';
import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'dacs-category-definition-list',
  templateUrl: './category-definition-list.component.html',
  styleUrls: ['./category-definition-list.component.css'],
})
export class CategoryDefinitionListComponent implements AfterViewInit, OnChanges {
  @Input()
  categoryDefinitions?: CategoryDefinition[];

  @ViewChild('commandsTemplate')
  commandsColumn!: TemplateRef<any>;

  @Output()
  delete = new EventEmitter<CategoryDefinition>();

  @Output()
  update = new EventEmitter<CategoryDefinition>();

  simpleColumns: SimpleColumnDef<CategoryDefinition>[] = [
    {
      header: 'Name',
      property: 'name',
    },
    {
      header: 'Code',
      property: 'code',
    },
    {
      header: 'Budget',
      property: catDef => formatCurrency(catDef.budget, 'en-US', '$', 'USD'),
    },
  ];

  columns: (SimpleColumnDef | CommandColumnDef)[] = [];

  constructor() {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.columns = [
        ...this.simpleColumns,
        {
          header: 'Actions',
          template: this.commandsColumn,
        },
      ];
    });
  }

  ngOnChanges(changes: SimpleChanges) {}

  onUpdateClicked(data: CategoryDefinition) {
    this.update.emit(data);
  }

  onDeleteClicked(data: CategoryDefinition) {
    this.delete.emit(data);
  }
}
