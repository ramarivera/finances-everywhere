import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CategoryDefinitionEntity } from '@dacs-fe/shared/data-access/category-definitions';
import { buildField } from '@dacs-fe/shared/forms';
import { CategoryDefinitionUpdateData } from '@dacs-fe/types';
import { isDetailsEntity } from '@dacs-fe/utils/state';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'dacs-fe-category-definition-update',
  templateUrl: './category-definition-update.component.html',
  styleUrls: ['./category-definition-update.component.css'],
})
export class CategoryDefinitionUpdateComponent implements OnChanges {
  @Input()
  categoryDefinition!: CategoryDefinitionEntity;

  @Input()
  readonly = false;

  @Output()
  save = new EventEmitter<CategoryDefinitionUpdateData>();

  @Output()
  cancel = new EventEmitter<void>();

  categoryDefinitionUpdateForm = new FormGroup({});

  categoryDefinitionUpdateData: CategoryDefinitionUpdateData = {
    name: '',
    code: '',
    description: '',
  };

  fields: FormlyFieldConfig[] = [
    buildField<CategoryDefinitionUpdateData>({ key: 'name', requiredName: 'Category name' }, { maxLength: 100 }),
    buildField<CategoryDefinitionUpdateData>({ key: 'code' }, { maxLength: 100 }),
    buildField<CategoryDefinitionUpdateData>({ key: 'description', type: 'textarea' }, { maxLength: 512 }),
  ];

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.categoryDefinition && changes.categoryDefinition.currentValue) {
      this.updateForm(this.categoryDefinition);
    }
    if (changes.readonly && changes.readonly.currentValue != null) {
      this.setReadonly(this.readonly);
    }
  }

  updateForm(categoryDefinition: CategoryDefinitionEntity) {
    if (isDetailsEntity(categoryDefinition)) {
      this.categoryDefinitionUpdateForm.patchValue({
        name: categoryDefinition.name,
        code: categoryDefinition.code,
        description: categoryDefinition.description,
      });
    }
  }

  setReadonly(isReadonly: boolean) {
    if (isReadonly) {
      this.categoryDefinitionUpdateForm.disable();
    }
    else {
      this.categoryDefinitionUpdateForm.enable();
    }
  }

  onSubmit() {
    if (this.categoryDefinitionUpdateForm.valid) {
      this.save.emit(this.categoryDefinitionUpdateData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
