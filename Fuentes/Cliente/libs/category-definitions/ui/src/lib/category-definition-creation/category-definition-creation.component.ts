import { Component, EventEmitter, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { buildField, buildGroup, getOptionsFromEnum } from '@dacs-fe/shared/forms';
import { CategoryDefinitionCreationData, Omit, Period } from '@dacs-fe/types';
import { FormlyFieldConfig } from '@ngx-formly/core';
import * as moment from 'moment';

interface CatDefCreationFormData
  extends Omit<CategoryDefinitionCreationData, 'validFrom' | 'validUntil'> {
  dates: Pick<CategoryDefinitionCreationData, 'validFrom' | 'validUntil'>;
}

@Component({
  selector: 'dacs-fe-category-definition-creation',
  templateUrl: './category-definition-creation.component.html',
  styleUrls: ['./category-definition-creation.component.css'],
})
export class CategoryDefinitionCreationComponent {
  @Output()
  save = new EventEmitter<CategoryDefinitionCreationData>();

  @Output()
  cancel = new EventEmitter<void>();

  categoryDefinitionCreationForm = new FormGroup({});

  categoryDefinitionCreationData: CatDefCreationFormData;

  fields: FormlyFieldConfig[] = [
    buildField<CatDefCreationFormData>(
      { key: 'name', requiredName: 'Category name' },
      { maxLength: 100 }
    ),
    buildField<CatDefCreationFormData>({ key: 'code' }, { maxLength: 100 }),
    buildField<CatDefCreationFormData>(
      { key: 'budget', requiredName: 'Budget' },
      {
        type: 'number',
        max: 100000000,
        min: 1,
        addonLeft: {
          icon: 'attach_money',
        },
      },
      {
        validation: {
          messages: {
            min: () => 'Budget should be between 1 and 100000000',
            max: () => 'Budget should be between 1 and 100000000',
          },
        },
      }
    ),
    buildField<CatDefCreationFormData>(
      { key: 'description', type: 'textarea' },
      { maxLength: 512 }
    ),
    buildField<CatDefCreationFormData>(
      { key: 'period', label: 'Periodicity', type: 'select', requiredName: 'Periodicity' },
      {
        options: getOptionsFromEnum(Period),
      }
    ),
    buildField<CatDefCreationFormData>(
      { key: 'offset', label: 'Offset (days)', requiredName: 'Offset' },
      {
        type: 'number',
        max: 60,
      }
    ),
    buildGroup<CatDefCreationFormData, 'dates'>(
      'dates',
      [
        {
          key: 'validFrom',
          label: 'Valid from',
          type: 'datepicker',
          requiredName: 'Valid from',
        },
        {
          key: 'validUntil',
          label: 'Valid until',
          type: 'datepicker',
          requiredName: 'Valid until',
        },
      ],
      {
        endDateGreater: {
          expression: (control: AbstractControl) => {
            const datesGroup = control.value;
            if (!datesGroup) {
              return true;
            }
            const startDate = datesGroup.validFrom;
            const endDate = datesGroup.validUntil;
            return !startDate || !endDate || startDate <= endDate;
          },
          message: 'Valid until should be smaller than valid from',
          errorPath: 'validUntil',
        },
      }
    ),
  ];

  constructor() {
    const now = moment();
    this.categoryDefinitionCreationData = {
      name: '',
      code: '',
      budget: 0,
      description: '',
      offset: 0,
      period: Period.Monthly,
      dates: {
        validFrom: now.toDate(),
        validUntil: now.add(1, 'month').toDate(),
      },
    };
  }

  onSubmit() {
    if (this.categoryDefinitionCreationForm.valid) {
      const catDefCreationData: CategoryDefinitionCreationData = {
        ...this.categoryDefinitionCreationData,
        ...this.categoryDefinitionCreationData.dates,
      };
      this.save.emit(catDefCreationData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
