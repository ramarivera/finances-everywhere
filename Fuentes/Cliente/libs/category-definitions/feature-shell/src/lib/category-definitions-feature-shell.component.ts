import { Component } from '@angular/core';

@Component({
  selector: 'dacs-fe-category-definitions',
  template: '<router-outlet></router-outlet>',
})
export class CategoryDefinitionsFeatureShellComponent {
  constructor() {}
}
