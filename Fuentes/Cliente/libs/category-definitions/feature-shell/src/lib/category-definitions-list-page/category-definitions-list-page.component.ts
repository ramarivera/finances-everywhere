import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FabButtonService } from '@dacs-fe/shared/ui/buttons';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { filter } from 'rxjs/operators';
import {
  CategoryDefinitionEntity,
  CategoryDefinitionsFacade,
  deleteCategoryDefinition,
} from '@dacs-fe/shared/data-access/category-definitions';

@Component({
  selector: 'dacs-fe-category-definition-list-page',
  templateUrl: './category-definitions-list-page.component.html',
})
export class CategoryDefinitionsListPageComponent implements OnInit, OnDestroy {
  loaded$ = this.facade.hasLoaded$;
  isLoading$ = this.facade.batchLoading$;
  categoryDefinitions$ = this.facade.allCategoryDefinitions$;

  buttonSubscription!: () => void;

  constructor(
    private facade: CategoryDefinitionsFacade,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private fabButtonService: FabButtonService
  ) {}

  ngOnInit() {
    this.buttonSubscription = this.fabButtonService.addButton({
      tooltip: 'New category',
      icon: 'category',
      color: 'primary',
      callback: () => this.onNewDefinitionClicked(),
    });
  }

  ngOnDestroy() {
    this.buttonSubscription();
  }

  onNewDefinitionClicked() {
    this.router.navigate(['./new'], { relativeTo: this.route.parent });
  }

  onUpdate(data: CategoryDefinitionEntity) {
    this.router.navigate([data.categoryDefinitionId, 'edit'], { relativeTo: this.route.parent });
  }

  onDelete(data: CategoryDefinitionEntity) {
    this.dialogService
      .openConfirmation(
        'Confirm category definition deletion',
        `Are you sure you want to delete the '${data.name}' category definition? This operation cannot be undone`
      )
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.facade.dispatch(
          deleteCategoryDefinition({ categoryDefinitionId: data.categoryDefinitionId })
        );
      });
  }
}
