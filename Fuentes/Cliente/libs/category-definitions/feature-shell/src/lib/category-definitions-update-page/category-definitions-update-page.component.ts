import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryDefinitionsFacade, updateCategoryDefinition } from '@dacs-fe/shared/data-access/category-definitions';
import { CategoryDefinitionUpdateData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { take } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-category-definitions-update-page',
  templateUrl: './category-definitions-update-page.component.html',
  styleUrls: ['./category-definitions-update-page.component.sass'],
})
export class CategoryDefinitionsUpdatePageComponent {
  loading$ = this.facade.singleLoading$;
  selected$ = this.facade.selectedCategoryDefinition$;

  constructor(
    private facade: CategoryDefinitionsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  onCancel() {
    this.navigateBack();
  }

  async onCategoryDefinitionSave($event: CategoryDefinitionUpdateData) {
    const params = await this.activatedRoute.params.pipe(take(1)).toPromise();
    const eventId = this.facade.dispatch(updateCategoryDefinition({ categoryDefinitionId: params.id, updateData: $event }));

    if (!eventId) {
      return;
    }

    const updatedCategory = await this.facade.allCategoryDefinitions$.pipe(entityByEventId(eventId)).toPromise();

    if (updatedCategory) {
      this.navigateBack();
    }
  }

  navigateBack() {
    this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
  }
}
