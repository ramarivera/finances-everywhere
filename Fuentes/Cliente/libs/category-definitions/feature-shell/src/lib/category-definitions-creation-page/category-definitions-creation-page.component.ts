import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryDefinitionsFacade, createCategoryDefinition } from '@dacs-fe/shared/data-access/category-definitions';
import { CategoryDefinitionCreationData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';

@Component({
  selector: 'dacs-fe-category-definitions-creation-page',
  templateUrl: './category-definitions-creation-page.component.html',
  styleUrls: ['./category-definitions-creation-page.component.sass'],
})
export class CategoryDefinitionsCreationPageComponent implements OnInit {
  constructor(
    private facade: CategoryDefinitionsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {}

  onCancel() {
    this.navigateBack();
  }

  async onCategoryDefinitionSave($event: CategoryDefinitionCreationData) {
    const eventId = this.facade.dispatch(createCategoryDefinition({ creationData: $event }));

    if (!eventId) {
      return;
    }

    const newCategory = await this.facade.allCategoryDefinitions$.pipe(entityByEventId(eventId)).toPromise();

    if (newCategory) {
      this.navigateBack();
    }
  }

  navigateBack() {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }
}
