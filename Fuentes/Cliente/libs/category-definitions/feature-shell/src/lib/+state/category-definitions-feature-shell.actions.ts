import { createAction, props } from '@ngrx/store';

export const categoryDefinitionsFeatureActivated = createAction(
  '[Category Definitions/Feature] category definitions activated'
);

export const loadCategoryDefinitionForUpdate = createAction(
  '[Category Definitions/Feature] load category definition for update',
  props<{ categoryDefinitionId: number }>()
);
