import { Injectable } from '@angular/core';
import { fetchCategories } from '@dacs-fe/shared/data-access/categories';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryDefinitionsFeatureShellComponent } from '../category-definitions-feature-shell.component';
import { CategoryDefinitionsUpdatePageComponent } from '../category-definitions-update-page/category-definitions-update-page.component';
import {
  loadCategoryDefinitionForUpdate,
  categoryDefinitionsFeatureActivated,
} from './category-definitions-feature-shell.actions';
import {
  CategoryDefinitionsPartialState,
  fetchCategoryDefinitions,
  selectCategoryDefinition,
  createCategoryDefinitionSuccess,
  updateCategoryDefinitionSuccess,
  deleteCategoryDefinitionSuccess,
} from '@dacs-fe/shared/data-access/category-definitions';

@Injectable()
export class CategoryDefinitionsFeatureShellEffects {
  constructor(
    private dataPersistence: DataPersistence<CategoryDefinitionsPartialState>,
    private actions$: Actions,
    private snackbarService: SnackBarService
  ) {}

  categoryDefinitionsNavigationEffect$ = createEffect(() =>
    this.dataPersistence.navigation(CategoryDefinitionsFeatureShellComponent, {
      run: () => categoryDefinitionsFeatureActivated(),
    })
  );

  // Yes, this effect just runs a redirection for the previous action :P
  // and then it forwards it to the "internal" store
  categoryDefinitionsLoadEffect$ = createEffect(() =>
    this.dataPersistence.fetch(categoryDefinitionsFeatureActivated.type, {
      run: () => from([fetchCategoryDefinitions(), fetchCategories()]),
    })
  );

  categoryDefinitionsUpdateNavigationEffect$ = createEffect(() =>
    this.dataPersistence.navigation(CategoryDefinitionsUpdatePageComponent, {
      run: a => {
        if (a.params.id) {
          return loadCategoryDefinitionForUpdate({ categoryDefinitionId: a.params.id });
        }
      },
    })
  );

  categoryDefinitionsUpdateLoadEffect$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof loadCategoryDefinitionForUpdate>>(
      loadCategoryDefinitionForUpdate,
      {
        run: ({ categoryDefinitionId }) => selectCategoryDefinition({ categoryDefinitionId }),
      }
    )
  );

  categoryDefinitionSuccessfullyCreated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createCategoryDefinitionSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Category definition created'))
      ),
    { dispatch: false }
  );

  categoryDefinitionSuccessfullyUpdated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateCategoryDefinitionSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Category definition updated'))
      ),
    { dispatch: false }
  );

  categoryDefinitionSuccessfullyDeleted$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteCategoryDefinitionSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Category definition deleted'))
      ),
    { dispatch: false }
  );
}
