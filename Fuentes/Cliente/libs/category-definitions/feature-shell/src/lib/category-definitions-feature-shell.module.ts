import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CategoryDefinitionsUiModule } from '@dacs-fe/category-definitions/ui';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { EffectsModule } from '@ngrx/effects';
import { CategoryDefinitionsCreationPageComponent } from './category-definitions-creation-page/category-definitions-creation-page.component';
import { CategoryDefinitionsDetailsPageComponent } from './category-definitions-details-page/category-definitions-details-page.component';
import { CategoryDefinitionsFeatureShellComponent } from './category-definitions-feature-shell.component';
import { CategoryDefinitionsListPageComponent } from './category-definitions-list-page/category-definitions-list-page.component';
import { CategoryDefinitionsUpdatePageComponent } from './category-definitions-update-page/category-definitions-update-page.component';
import { CategoryDefinitionsFeatureShellEffects } from './+state/category-definitions-feature-shell.effects';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    CategoryDefinitionsUiModule,
    RouterModule.forChild([
      {
        path: '',
        component: CategoryDefinitionsFeatureShellComponent,
        children: [
          { path: '', component: CategoryDefinitionsListPageComponent },
          { path: 'new', component: CategoryDefinitionsCreationPageComponent },
          { path: ':id', component: CategoryDefinitionsDetailsPageComponent },
          {
            path: ':id/edit',
            component: CategoryDefinitionsUpdatePageComponent,
          },
        ],
      },
    ]),
    EffectsModule.forFeature([CategoryDefinitionsFeatureShellEffects]),
  ],
  declarations: [
    CategoryDefinitionsFeatureShellComponent,
    CategoryDefinitionsListPageComponent,
    CategoryDefinitionsDetailsPageComponent,
    CategoryDefinitionsUpdatePageComponent,
    CategoryDefinitionsCreationPageComponent,
  ],
})
export class CategoryDefinitionsFeatureShellModule {}
