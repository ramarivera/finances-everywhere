import { async, TestBed } from '@angular/core/testing';
import { AttachmentsFeatureShellModule } from './attachments-feature-shell.module';

describe('AttachmentsFeatureShellModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AttachmentsFeatureShellModule],
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AttachmentsFeatureShellModule).toBeDefined();
  });
});
