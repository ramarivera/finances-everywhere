module.exports = {
  ...require('../../../../.storybook/main'),
  stories: ['../src/lib/**/*.stories.(tsx?|mdx)'],
};
