import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ImageUtilsService } from '../image-utils.service';

@Component({
  selector: 'dacs-fe-image-uploader',
  templateUrl: './image-uploader.component.html',
})
export class ImageUploaderComponent implements OnInit {
  @ViewChild('uploader', { static: false })
  uploaderInput!: ElementRef<HTMLInputElement>;

  @Output()
  imageUploaded = new EventEmitter<string>();

  previewSize$ = this.imageUtilsService.imageSize$;

  selectedImageBase64Src = '';
  imageUploadError = '';

  constructor(private imageUtilsService: ImageUtilsService) {}

  ngOnInit(): void {}

  onChooseAnotherClicked() {
    this.uploaderInput.nativeElement.click();
  }

  onConfirmClicked() {
    this.imageUploaded.emit(this.imageUtilsService.toBase64(this.selectedImageBase64Src));
  }

  onFileChange(event: any) {
    this.imageUtilsService.getImageFromUpload(this.uploaderInput.nativeElement.files).subscribe({
      next: (value) => {
        this.selectedImageBase64Src = value.base64Image;
      },
      error: (error) => {
        this.imageUploadError = error;
      },
    });
  }

  onSelectFileClicked() {
    this.uploaderInput.nativeElement.click();
  }
}
