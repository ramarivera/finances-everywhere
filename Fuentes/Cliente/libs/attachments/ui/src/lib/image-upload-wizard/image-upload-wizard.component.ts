import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { PictureSourceTypes } from '@dacs-fe/types';

@Component({
  selector: 'dacs-fe-image-upload-wizard',
  templateUrl: './image-upload-wizard.component.html',
})
export class ImageUploadWizardComponent implements OnInit {
  @ViewChild('stepper', { static: true })
  private myStepper!: MatStepper;

  /** Event fired when the image has been selected and confirmed.*/
  @Output()
  selectedPicture = new EventEmitter<string>();

  /** Event fired when the image source has been set to {@link PictureSourceTypes.URL} and the URL has been provided and preview confirmed.*/
  @Output()
  selectedUrlPicture = new EventEmitter<string>();

  /** Event fired when the image source has been set to {@link PictureSourceTypes.Photo} and the photo has been taken and confirmed.*/
  @Output()
  selectedCameraPicture = new EventEmitter<string>();

  /** Event fired when the image source has been set to {@link PictureSourceTypes.Upload} and the file has been uploaded and the preview confirmed. */
  @Output()
  selectedFilePicture = new EventEmitter<string>();

  imageSelected = false;
  imageEdited = false;

  selectedSource: PictureSourceTypes = PictureSourceTypes.None;
  PictureSourceTypes = PictureSourceTypes;
  selectedBase64Image = '';

  constructor() {}

  ngOnInit(): void {}

  onStepChange(event: StepperSelectionEvent) {
    // Naively perform "initialization" on step entry?
    if (event.selectedIndex === 0) {
      this.selectedSource = PictureSourceTypes.None;
    }
  }

  setSource(pictureSource: PictureSourceTypes) {
    this.selectedSource = pictureSource;
    // I hate myself for this kind of things...
    // this is required since the sourceSelected state is bound to the step completion input,
    // and without the timeout the next call is ignored
    setTimeout(() => {
      this.myStepper.next();
    });
  }

  onPictureSelected(base64Image: string) {
    this.imageSelected = true;
    this.selectedBase64Image = base64Image;
    setTimeout(() => {
      this.myStepper.next();
    });
  }

  onPictureConfirmed(base64Image: string) {
    switch (this.selectedSource) {
      case PictureSourceTypes.Photo:
        this.selectedCameraPicture.emit(base64Image);
        break;

      case PictureSourceTypes.URL:
        this.selectedUrlPicture.emit(base64Image);
        break;

      case PictureSourceTypes.Upload:
        this.selectedFilePicture.emit(base64Image);
        break;

      default:
        break;
    }

    this.selectedPicture.emit(base64Image);
  }
}
