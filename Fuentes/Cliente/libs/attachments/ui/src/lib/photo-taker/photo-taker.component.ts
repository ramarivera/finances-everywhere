import { Component, EventEmitter, Output } from '@angular/core';
import { WebcamImage } from 'ngx-webcam';
import { Subject } from 'rxjs';
import { ImageUtilsService } from '../image-utils.service';

@Component({
  selector: 'dacs-fe-photo-taker',
  templateUrl: './photo-taker.component.html',
  styleUrls: ['./photo-taker.component.scss'],
})
export class PhotoTakerComponent {
  private selfieStickSubject = new Subject();
  public selfieStick$ = this.selfieStickSubject.asObservable();

  private cameraSwitchSubject = new Subject<boolean>();
  public cameraSwitch$ = this.cameraSwitchSubject.asObservable();

  @Output()
  pictureTaken = new EventEmitter<string>();

  showCamera = true;
  webcamImage: WebcamImage | null = null;

  cameraSize$ = this.imageUtilsService.imageSize$;

  constructor(private imageUtilsService: ImageUtilsService) {}

  onImageCapture(event: WebcamImage) {
    this.webcamImage = event;
  }

  onImageClick() {
    if (!this.showCamera) return;
    this.selfieStickSubject.next();
  }

  onTriggerClicked() {
    this.selfieStickSubject.next();
    this.showCamera = false;
  }

  onConfirmClicked() {
    if (!this.webcamImage) return;
    this.pictureTaken.emit(this.webcamImage?.imageAsBase64);
  }

  onSwitchCameraClicked() {
    this.cameraSwitchSubject.next(true);
  }

  onReTakeClicked() {
    this.webcamImage = null;
    this.showCamera = true;
  }
}
