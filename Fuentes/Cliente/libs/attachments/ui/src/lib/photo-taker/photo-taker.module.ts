import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { WebcamModule } from 'ngx-webcam';
import { PhotoTakerComponent } from './photo-taker.component';

@NgModule({
  declarations: [PhotoTakerComponent],
  exports: [PhotoTakerComponent],
  imports: [CommonModule, WebcamModule, SharedUiMaterialModule],
})
export class PhotoTakerModule {}
