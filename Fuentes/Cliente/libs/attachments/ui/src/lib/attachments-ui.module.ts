import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { ImageCropperModule } from './image-cropper/image-cropper.module';
import { ImageUploadWizardComponent } from './image-upload-wizard/image-upload-wizard.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { ImageViewerComponent } from './image-viewer/image-viewer.component';
import { PhotoTakerModule } from './photo-taker/photo-taker.module';
import { UrlImageSelectorModule } from './url-image-selector/url-image-selector.module';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    ImageCropperModule,
    UrlImageSelectorModule,
    PhotoTakerModule,
  ],
  exports: [ImageCropperModule, ImageUploadWizardComponent, ImageViewerComponent],
  declarations: [ImageUploaderComponent, ImageUploadWizardComponent, ImageViewerComponent],
})
export class AttachmentsUiModule {}
