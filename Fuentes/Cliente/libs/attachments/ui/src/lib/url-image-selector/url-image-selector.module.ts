import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { UrlImageSelectorComponent } from './url-image-selector.component';

@NgModule({
  declarations: [UrlImageSelectorComponent],
  exports: [UrlImageSelectorComponent],
  imports: [CommonModule, FormsModule, SharedUiMaterialModule],
})
export class UrlImageSelectorModule {}
