import { ImageUtilsService } from '../image-utils.service';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  ElementRef,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-url-image-selector',
  templateUrl: './url-image-selector.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UrlImageSelectorComponent implements OnInit {
  @ViewChild('previewImg', { static: false })
  previewImg?: ElementRef<HTMLImageElement>;

  imageUrl = '';
  showPreview = false;
  imagePreviewErrored = false;

  /** Event fired when the user has clicked on the Confirm button. The event payload is the image src provided by the user. */
  @Output()
  imageConfirmed = new EventEmitter<string>();

  previewSize$ = this.imageUtilsService.imageSize$;

  constructor(private imageUtilsService: ImageUtilsService) {}

  ngOnInit(): void {}

  /** Event handler for the preview img tag error */
  onImagePreviewError() {
    this.imagePreviewErrored = true;
  }

  onGetPreview() {
    this.showPreview = true;
  }

  onClearUrl() {
    this.imageUrl = '';
    this.showPreview = false;
    this.imagePreviewErrored = false;
  }

  onUrlChange(newUrl: string) {
    this.imageUrl = newUrl;
    this.showPreview = false;
  }

  onConfirm() {
    if (!this.previewImg) return;
    this.imageUtilsService.getBase64ImageFromURL(this.imageUrl).subscribe((base64Image) => {
      this.imageConfirmed.emit(base64Image);
    });
  }
}
