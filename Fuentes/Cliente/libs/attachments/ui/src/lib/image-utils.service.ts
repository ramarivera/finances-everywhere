import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { escapeRegExp } from 'lodash';
import { Observable, Observer } from 'rxjs';
import { distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

const PngDataUrlPrefix = 'data:image/png;base64,';
const JpegDataUrlPrefix = 'data:image/jpeg;base64,';

@Injectable({
  providedIn: 'root',
})
export class ImageUtilsService {
  constructor(private breakpointObserver: BreakpointObserver) {}

  isMobile$ = this.breakpointObserver
    .observe([
      Breakpoints.Handset,
      Breakpoints.Medium,
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.HandsetPortrait,
    ])
    .pipe(
      map((result) => result.matches),
      distinctUntilChanged(),
      startWith(false)
    );

  imageSize$ = this.isMobile$.pipe(
    map((isMobile) => {
      return {
        height: isMobile ? 400 : 480,
        width: isMobile ? 400 : 640,
        pxHeight: `${isMobile ? 400 : 480}px`,
        pxWidth: `${isMobile ? 400 : 640}px`,
      };
    })
  );

  toDataUrl(base64Image: string) {
    return JpegDataUrlPrefix + this.toBase64(base64Image);
  }

  toBase64(dataUrlImage: string) {
    return dataUrlImage.replace(/^data:image\/(png|jpg|jpeg);base64,/gi, '');
  }

  getImageFromUpload(files: FileList | null) {
    return new Observable<{ base64Image: string }>((observer) => {
      if (files && files[0]) {
        // Size Filter Bytes
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const maxHeight = 15200;
        const maxWidth = 25600;

        if (files[0].size > max_size) {
          observer.error('Maximum size allowed is ' + max_size / 1000 + 'Mb');
        }

        if (!allowed_types.includes(files[0].type)) {
          observer.error('Only Images are allowed ( JPG | PNG )');
        }

        const reader = new FileReader();

        reader.onload = (e: any) => {
          const image = new Image();
          image.src = e.target.result;
          image.onload = (rs: any) => {
            const imgHeight = rs.currentTarget['height'];
            const imgWidth = rs.currentTarget['width'];

            if (imgHeight > maxHeight && imgWidth > maxWidth) {
              observer.error('Maximum dimensions allowed ' + maxHeight + '*' + maxWidth + 'px');
            } else {
              const base64Image = e.target.result;
              observer.next({ base64Image });
              observer.complete();
            }
          };
        };

        reader.readAsDataURL(files[0]);
      } else {
        observer.complete();
      }
    });
  }

  // https://medium.com/better-programming/convert-a-base64-url-to-image-file-in-angular-4-5796a19fdc21
  getBase64ImageFromURL(url: string): Observable<string> {
    return Observable.create((observer: Observer<string>) => {
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;

      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64ImageFromImgTag(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64ImageFromImgTag(img));
        observer.complete();
      }
    });
  }

  private getBase64ImageFromImgTag(img: HTMLImageElement): string {
    const canvas: HTMLCanvasElement = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    const ctx = canvas.getContext('2d');
    ctx?.drawImage(img, 0, 0);

    return this.toBase64(canvas.toDataURL('image/png'));
  }
}
