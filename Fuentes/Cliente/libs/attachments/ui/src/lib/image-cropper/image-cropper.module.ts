import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { ImageCropperModule as NgxImageCropperModule } from 'ngx-image-cropper';
import { ImageCropperComponent } from './image-cropper.component';

@NgModule({
  imports: [CommonModule, SharedUiMaterialModule, NgxImageCropperModule],
  declarations: [ImageCropperComponent],
  exports: [ImageCropperComponent],
})
export class ImageCropperModule {}
