import { ImageUtilsService } from '../image-utils.service';
import {
  ImageCroppedEvent,
  ImageCropperComponent as NgxImageCropperComponent,
} from 'ngx-image-cropper';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss'],
})
export class ImageCropperComponent implements OnInit, OnChanges {
  @ViewChild(NgxImageCropperComponent, { static: false })
  imageCropper!: NgxImageCropperComponent;

  @Input()
  base64Image = '';

  @Input()
  maintainAspectRatio = true;

  @Output()
  imageCropped = new EventEmitter<string>();

  croppedImageData?: ImageCroppedEvent;
  imageRotation = 0;
  base64ImageSrc = '';
  showCropper = true;

  previewSize$ = this.imageUtilsService.imageSize$;

  constructor(private imageUtilsService: ImageUtilsService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.base64Image && !changes.base64Image.isFirstChange()) {
      this.initialize(changes.base64Image.currentValue);
      this.base64ImageSrc = this.imageUtilsService.toDataUrl(changes.base64Image.currentValue);
    }
  }

  ngOnInit(): void {}

  onConfirmClicked() {
    if (!this.croppedImageData || !this.croppedImageData.base64) return;
    this.imageCropped.emit(this.imageUtilsService.toBase64(this.croppedImageData.base64));
  }

  onCropImageClicked() {
    this.croppedImageData = this.imageCropper.crop()!;
    this.showCropper = false;
  }

  onResetClicked() {
    this.initialize(this.base64Image);
  }

  rotateImage(direction: 1 | -1) {
    this.imageRotation = (this.imageRotation + direction) % 4;
  }

  private initialize(base64Image: string) {
    this.imageRotation = 0;
    this.base64ImageSrc = this.imageUtilsService.toDataUrl(base64Image);
    this.showCropper = true;
    this.croppedImageData = undefined;
  }
}
