import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ImageUtilsService } from '../image-utils.service';

@Component({
  selector: 'dacs-fe-image-viewer',
  templateUrl: './image-viewer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageViewerComponent implements OnChanges {
  @Input()
  base64Image = '';

  dataUrlImage = '';

  constructor(public imageUtilsService: ImageUtilsService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.base64Image) {
      this.dataUrlImage = this.imageUtilsService.toDataUrl(changes.base64Image.currentValue);
    }
  }
}
