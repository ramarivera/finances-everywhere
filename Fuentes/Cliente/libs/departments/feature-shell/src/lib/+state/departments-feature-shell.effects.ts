import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { map } from 'rxjs/operators';
import { DepartmentUpdatePageComponent } from '../department-update-page/department-update-page.component';
import { DepartmentsFeatureShellComponent } from '../departments-feature-shell.component';
import {
  departmentsFeatureActivated,
  loadDepartmentForUpdate,
} from './departments-feature-shell.actions';
import {
  DepartmentsPartialState,
  departmentsQuery,
  fetchDepartments,
  selectDepartment,
  updateDepartmentSuccess,
  deleteDepartmentSuccess,
  createDepartmentSuccess,
} from '@dacs-fe/shared/data-access/departments';

@Injectable()
export class DepartmentsFeatureShellEffects {
  constructor(
    private dataPersistence: DataPersistence<DepartmentsPartialState>,
    private actions$: Actions,
    private snackbarService: SnackBarService
  ) {}

  departmentsNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(DepartmentsFeatureShellComponent, {
      run: (routeSnapshot) => {
        if (routeSnapshot.component === DepartmentsFeatureShellComponent) {
          return departmentsFeatureActivated();
        }
      },
    })
  );

  departmentsFeatureActivated$ = createEffect(() =>
    this.dataPersistence.fetch(departmentsFeatureActivated, {
      run: (_, state) => {
        if (!state || departmentsQuery.getBatchLoading(state)) {
          return;
        }

        return fetchDepartments();
      },
    })
  );

  departmentsUpdateNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(DepartmentUpdatePageComponent, {
      run: (snapshot) => loadDepartmentForUpdate({ departmentId: snapshot.params.id }),
    })
  );

  departmentsUpdateLoadEffect$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof loadDepartmentForUpdate>>(
      loadDepartmentForUpdate,
      {
        run: ({ departmentId }) => selectDepartment({ departmentId }),
      }
    )
  );

  departmentSuccessfullyCreated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createDepartmentSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Department created'))
      ),
    { dispatch: false }
  );

  departmentSuccessfullyUpdated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateDepartmentSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Department updated'))
      ),
    { dispatch: false }
  );

  departmentSuccessfullyDeleted$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteDepartmentSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Department deleted'))
      ),
    { dispatch: false }
  );
}
