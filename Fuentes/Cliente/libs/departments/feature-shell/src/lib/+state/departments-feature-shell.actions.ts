import { createAction, props } from '@ngrx/store';

export const departmentsFeatureActivated = createAction(
  '[Departments/Feature] departments activated'
);

export const loadDepartmentForUpdate = createAction(
  '[Departments/Feature] load department for update',
  props<{ departmentId: number }>()
);
