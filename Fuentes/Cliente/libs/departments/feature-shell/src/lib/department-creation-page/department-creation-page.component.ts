import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { createDepartment, DepartmentsFacade } from '@dacs-fe/shared/data-access/departments';
import { DepartmentCreationData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { uniqueId } from 'lodash';

@Component({
  selector: 'dacs-fe-department-creation-page',
  templateUrl: './department-creation-page.component.html',
  styleUrls: ['./department-creation-page.component.scss'],
})
export class DepartmentCreationPageComponent implements OnInit {
  constructor(
    private departmentsFacade: DepartmentsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {}

  onCancel() {
    this.navigateBack();
  }

  onDepartmentSave($event: DepartmentCreationData) {
    const eventId = this.departmentsFacade.dispatch(
      createDepartment({ creationData: $event, eventId: uniqueId('dept') })
    );

    if (eventId) {
      this.departmentsFacade.allDepartments$
        .pipe(entityByEventId(eventId))
        .subscribe((_) => this.navigateBack());
    }
  }

  navigateBack() {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }
}
