import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dacs-fe-departments-feature-shell',
  template: '<router-outlet></router-outlet>',
})
export class DepartmentsFeatureShellComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
