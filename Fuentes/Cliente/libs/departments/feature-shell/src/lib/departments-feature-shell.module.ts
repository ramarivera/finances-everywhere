import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DepartmentsUiModule } from '@dacs-fe/departments/ui';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { EffectsModule } from '@ngrx/effects';
import { DepartmentCreationPageComponent } from './department-creation-page/department-creation-page.component';
import { DepartmentListPageComponent } from './department-list-page/department-list-page.component';
import { DepartmentUpdatePageComponent } from './department-update-page/department-update-page.component';
import { DepartmentsFeatureShellComponent } from './departments-feature-shell.component';
import { DepartmentsFeatureShellEffects } from './+state/departments-feature-shell.effects';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    DepartmentsUiModule,
    RouterModule.forChild([
      {
        path: '',
        component: DepartmentsFeatureShellComponent,
        children: [
          { path: '', component: DepartmentListPageComponent },
          { path: 'new', component: DepartmentCreationPageComponent },
          {
            path: ':id/edit',
            component: DepartmentUpdatePageComponent,
          },
        ],
      },
    ]),
    EffectsModule.forFeature([DepartmentsFeatureShellEffects]),
  ],
  declarations: [
    DepartmentCreationPageComponent,
    DepartmentUpdatePageComponent,
    DepartmentListPageComponent,
    DepartmentsFeatureShellComponent,
  ],
})
export class DepartmentsFeatureShellModule {}
