import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { deleteAccount } from '@dacs-fe/shared/data-access/accounts';
import { deleteDepartment, DepartmentsFacade } from '@dacs-fe/shared/data-access/departments';
import { FabButtonService } from '@dacs-fe/shared/ui/buttons';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { Department } from '@dacs-fe/types';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-department-list-page',
  templateUrl: './department-list-page.component.html',
  styleUrls: ['./department-list-page.component.scss'],
})
export class DepartmentListPageComponent implements OnInit {
  loaded$ = this.facade.hasLoaded$;
  isLoading$ = this.facade.batchLoading$;
  departments$ = this.facade.allDepartments$;

  constructor(
    private facade: DepartmentsFacade,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService
  ) {}

  ngOnInit() {}

  onNewAccountClicked() {
    this.router.navigate(['./new'], { relativeTo: this.route.parent });
  }

  onUpdate(data: Department) {
    this.router.navigate([data.departmentId, 'edit'], { relativeTo: this.route.parent });
  }

  onDelete(data: Department) {
    this.dialogService
      .openConfirmation(
        'Confirm department deletion',
        `Are you sure you want to delete the '${data.name}' department? This operation cannot be undone`
      )
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.facade.dispatch(deleteDepartment({ departmentId: data.departmentId }));
      });
  }
}
