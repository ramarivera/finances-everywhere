import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountsFacade, updateAccount } from '@dacs-fe/shared/data-access/accounts';
import { CurrenciesFacade } from '@dacs-fe/shared/data-access/currencies';
import { DepartmentsFacade, updateDepartment } from '@dacs-fe/shared/data-access/departments';
import { AccountUpdateData, DepartmentUpdateData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { combineLatest } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-department-update-page',
  templateUrl: './department-update-page.component.html',
  styleUrls: ['./department-update-page.component.scss'],
})
export class DepartmentUpdatePageComponent {
  loading$ = this.departmentsFacade.singleLoading$;
  selected$ = this.departmentsFacade.selectedDepartment$;

  constructor(
    private departmentsFacade: DepartmentsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  onCancel() {
    this.navigateBack();
  }

  async onDepartmentSave($event: DepartmentUpdateData) {
    const params = await this.activatedRoute.params.pipe(take(1)).toPromise();
    const eventId = this.departmentsFacade.dispatch(
      updateDepartment({ departmentId: params.id, updateData: $event })
    );
    if (eventId) {
      const updatedDepartment = await this.departmentsFacade.allDepartments$
        .pipe(entityByEventId(eventId))
        .toPromise();
      if (updatedDepartment) {
        this.navigateBack();
      }
    }
  }

  navigateBack() {
    this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
  }
}
