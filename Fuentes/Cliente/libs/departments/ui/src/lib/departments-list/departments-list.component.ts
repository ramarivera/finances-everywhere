import { CommandColumnDef, SimpleColumnDef } from '@dacs-fe/shared/ui/tables';
import { AccountListViewModel, Department } from '@dacs-fe/types';
import {
  AfterViewInit,
  Component,
  Input,
  TemplateRef,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-department-list',
  templateUrl: './departments-list.component.html',
})
export class DepartmentListComponent implements AfterViewInit {
  @Input()
  departments?: Department[];

  @ViewChild('commandsTemplate')
  commandsColumn!: TemplateRef<any>;

  @Output()
  delete = new EventEmitter<Department>();

  @Output()
  update = new EventEmitter<Department>();

  columns: (SimpleColumnDef | CommandColumnDef)[] = [];

  constructor() {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.columns = [
        {
          header: 'Name',
          property: 'name',
        },
        {
          header: 'Actions',
          template: this.commandsColumn,
        },
      ];
    });
  }

  onUpdateClicked(data: Department) {
    this.update.emit(data);
  }

  onDeleteClicked(data: Department) {
    this.delete.emit(data);
  }
}
