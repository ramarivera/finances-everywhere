import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { buildField, buildOptionsField } from '@dacs-fe/shared/forms';
import { AccountCreationData, Currency, Department, DepartmentCreationData } from '@dacs-fe/types';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'dacs-fe-department-creation',
  templateUrl: './department-creation.component.html',
  styleUrls: ['./department-creation.component.scss'],
})
export class DepartmentCreationComponent implements OnInit {
  @Output()
  save = new EventEmitter<DepartmentCreationData>();

  @Output()
  cancel = new EventEmitter<void>();

  departmentCreationForm = new FormGroup({});
  departmentCreationData: Partial<DepartmentCreationData>;
  fields!: FormlyFieldConfig[];

  constructor() {
    this.departmentCreationData = {
      name: '',
      description: '',
    };
  }

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  private buildFieldsConfiguration() {
    return [
      buildField<DepartmentCreationData>(
        { key: 'name', requiredName: 'Department name' },
        { maxLength: 100 }
      ),
      buildField<DepartmentCreationData>(
        { key: 'description', type: 'textarea' },
        { maxLength: 512 }
      ),
    ];
  }

  onSubmit() {
    if (this.departmentCreationForm.valid) {
      this.save.emit(this.departmentCreationData as DepartmentCreationData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
