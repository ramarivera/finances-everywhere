import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedFormsModule } from '@dacs-fe/shared/forms';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { SharedTablesModule } from '@dacs-fe/shared/ui/tables';
import { DepartmentCreationComponent } from './department-creation/department-creation.component';
import { DepartmentUpdateComponent } from './department-update/department-update.component';
import { DepartmentListComponent } from './departments-list/departments-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedFormsModule,
    SharedUiLoadingModule,
    SharedTablesModule,
  ],
  declarations: [DepartmentListComponent, DepartmentCreationComponent, DepartmentUpdateComponent],
  exports: [DepartmentListComponent, DepartmentCreationComponent, DepartmentUpdateComponent],
})
export class DepartmentsUiModule {}
