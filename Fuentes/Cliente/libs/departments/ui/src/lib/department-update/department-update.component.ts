import { FormGroup } from '@angular/forms';
import { DepartmentEntity } from '@dacs-fe/shared/data-access/departments';
import { buildField, buildOptionsFromObjects } from '@dacs-fe/shared/forms';
import { Currency, Department, DepartmentUpdateData } from '@dacs-fe/types';
import { isDetailsEntity } from '@dacs-fe/utils/state';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-department-update',
  templateUrl: './department-update.component.html',
  styleUrls: ['./department-update.component.scss'],
})
export class DepartmentUpdateComponent implements OnInit, OnChanges {
  @Input()
  department!: DepartmentEntity;

  @Output()
  save = new EventEmitter<DepartmentUpdateData>();

  @Output()
  cancel = new EventEmitter<void>();

  departmentUpdateForm = new FormGroup({});

  departmentUpdateData: Partial<DepartmentUpdateData> = {
    name: '',
    description: '',
  };

  fields: FormlyFieldConfig[] = [];

  constructor() {}

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.department && changes.department.currentValue) {
      this.updateForm(this.department);
    }
  }

  private buildFieldsConfiguration() {
    return [
      buildField<DepartmentUpdateData>(
        { key: 'name', requiredName: 'department name' },
        { maxLength: 100 }
      ),
      buildField<DepartmentUpdateData>(
        { key: 'description', type: 'textarea' },
        { maxLength: 512 }
      ),
    ];
  }

  updateForm(department: DepartmentEntity) {
    if (isDetailsEntity(department)) {
      this.departmentUpdateForm.patchValue({
        name: department.name,
        description: department.description,
      });
    }
  }

  onSubmit() {
    if (this.departmentUpdateForm.valid) {
      this.save.emit({ ...this.departmentUpdateData } as DepartmentUpdateData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
