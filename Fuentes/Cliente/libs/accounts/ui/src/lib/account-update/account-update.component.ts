import { FormGroup } from '@angular/forms';
import { AccountEntity } from '@dacs-fe/shared/data-access/accounts';
import { buildField, buildOptionsFromObjects } from '@dacs-fe/shared/forms';
import { AccountUpdateData, Currency, Department } from '@dacs-fe/types';
import { isDetailsEntity } from '@dacs-fe/utils/state';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-account-update',
  templateUrl: './account-update.component.html',
  styleUrls: ['./account-update.component.css'],
})
export class AccountUpdateComponent implements OnInit, OnChanges {
  @Input()
  account!: AccountEntity;

  @Input()
  currencies?: Currency[];

  @Input()
  departments?: Department[];

  @Input()
  readonly = false;

  @Output()
  save = new EventEmitter<AccountUpdateData>();

  @Output()
  cancel = new EventEmitter<void>();

  accountUpdateForm = new FormGroup({});

  accountUpdateData: Partial<AccountUpdateData> = {
    name: '',
    code: '',
    description: '',
  };

  fields: FormlyFieldConfig[] = [];

  constructor() {}

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      (changes.currencies && !changes.currencies.isFirstChange()) ||
      (changes.departments && !changes.departments.isFirstChange())
    ) {
      this.fields = this.buildFieldsConfiguration();
    }

    if (changes.account && changes.account.currentValue) {
      this.updateForm(this.account);
    }

    if (changes.readonly && changes.readonly.currentValue != null) {
      this.setReadonly(this.readonly);
    }
  }

  private buildFieldsConfiguration() {
    return [
      buildField<AccountUpdateData>(
        { key: 'name', requiredName: 'Account name' },
        { maxLength: 100 }
      ),
      buildField<AccountUpdateData>({ key: 'code' }, { maxLength: 100 }),
      buildField<AccountUpdateData>({ key: 'description', type: 'textarea' }, { maxLength: 512 }),
      buildField<AccountUpdateData>(
        {
          key: 'currencyId',
          label: 'Currency code',
          type: 'select',
          requiredName: 'Currency code',
        },
        {
          options: buildOptionsFromObjects(this.currencies, d => d.isoCode, d => d.currencyId),
        },
        {
          expressionProperties: {
            'templateOptions.disabled': () => !this.currencies || !this.currencies.length,
          },
        }
      ),
      buildField<AccountUpdateData>(
        {
          key: 'departmentId',
          label: 'Department',
          type: 'select',
          requiredName: 'Department',
        },
        {
          options: buildOptionsFromObjects(this.departments, d => d.name, d => d.departmentId),
        },
        {
          expressionProperties: {
            'templateOptions.disabled': () => !this.departments || !this.departments.length,
          },
        }
      ),
    ];
  }

  updateForm(account: AccountEntity) {
    if (isDetailsEntity(account)) {
      this.accountUpdateForm.patchValue({
        name: account.name,
        code: account.code,
        description: account.description,
        currencyId: account.currency.currencyId,
        departmentId: account.department.departmentId,
      });
    }
  }

  setReadonly(isReadonly: boolean) {
    if (isReadonly) {
      this.accountUpdateForm.disable();
    } else {
      this.accountUpdateForm.enable();
    }
  }

  onSubmit() {
    if (this.accountUpdateForm.valid) {
      this.save.emit({ ...this.accountUpdateData } as AccountUpdateData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
