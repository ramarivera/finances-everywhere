import { CommandColumnDef, SimpleColumnDef } from '@dacs-fe/shared/ui/tables';
import { AccountListViewModel } from '@dacs-fe/types';
import {
  AfterViewInit,
  Component,
  Input,
  TemplateRef,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'dacs-fe-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css'],
})
export class AccountListComponent implements AfterViewInit {
  @Input()
  accounts?: AccountListViewModel[];

  @ViewChild('commandsTemplate')
  commandsColumn!: TemplateRef<any>;

  @Output()
  delete = new EventEmitter<AccountListViewModel>();

  @Output()
  update = new EventEmitter<AccountListViewModel>();

  columns: (SimpleColumnDef | CommandColumnDef)[] = [];

  constructor() {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.columns = [
        {
          header: 'Name',
          property: 'name',
        },
        {
          header: 'Code',
          property: 'code',
        },
        {
          header: 'Department',
          property: (account: Partial<AccountListViewModel>) =>
            account.department ? account.department.name : '',
        },
        {
          header: 'Currency',
          property: (account: Partial<AccountListViewModel>) =>
            account.currency ? account.currency.isoCode : '',
        },
        {
          header: 'Actions',
          template: this.commandsColumn,
        },
      ];
    });
  }

  onUpdateClicked(data: AccountListViewModel) {
    this.update.emit(data);
  }

  onDeleteClicked(data: AccountListViewModel) {
    this.delete.emit(data);
  }
}
