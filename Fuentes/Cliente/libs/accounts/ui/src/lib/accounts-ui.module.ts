import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedFormsModule } from '@dacs-fe/shared/forms';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { SharedTablesModule } from '@dacs-fe/shared/ui/tables';
import { AccountCreationComponent } from './account-creation/account-creation.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountUpdateComponent } from './account-update/account-update.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedFormsModule,
    SharedUiLoadingModule,
    SharedTablesModule,
  ],
  declarations: [AccountListComponent, AccountCreationComponent, AccountUpdateComponent],
  exports: [AccountListComponent, AccountCreationComponent, AccountUpdateComponent],
})
export class AccountsUiModule {}
