import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { buildField, buildOptionsField } from '@dacs-fe/shared/forms';
import { AccountCreationData, Currency, Department } from '@dacs-fe/types';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'dacs-fe-account-creation',
  templateUrl: './account-creation.component.html',
  styleUrls: ['./account-creation.component.css'],
})
export class AccountCreationComponent implements OnInit {
  @Input()
  currencies?: Currency[];

  @Input()
  departments?: Department[];

  @Output()
  save = new EventEmitter<AccountCreationData>();

  @Output()
  cancel = new EventEmitter<void>();

  accountCreationForm = new FormGroup({});

  // Made a partial in order to handle not available values
  accountCreationData: Partial<AccountCreationData>;

  fields!: FormlyFieldConfig[];

  constructor() {
    this.accountCreationData = {
      name: '',
      code: '',
      description: '',
    };
  }

  ngOnInit() {
    this.fields = this.buildFieldsConfiguration();
  }

  private buildFieldsConfiguration() {
    return [
      buildField<AccountCreationData>(
        { key: 'name', requiredName: 'Account name' },
        { maxLength: 100 }
      ),
      buildField<AccountCreationData>({ key: 'code' }, { maxLength: 100 }),
      buildField<AccountCreationData>({ key: 'description', type: 'textarea' }, { maxLength: 512 }),
      buildOptionsField<AccountCreationData, Department>(
        {
          key: 'departmentId',
          label: 'Department',
          requiredName: 'Department',
        },
        () => this.departments,
        (d) => d.name,
        (d) => d.departmentId
      ),
      buildOptionsField<AccountCreationData, Currency>(
        {
          key: 'currencyId',
          label: 'Currency code',
        },
        () => this.currencies,
        (d) => d.isoCode,
        (d) => d.currencyId
      ),
    ];
  }

  onSubmit() {
    if (this.accountCreationForm.valid) {
      this.save.emit(this.accountCreationData as AccountCreationData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
