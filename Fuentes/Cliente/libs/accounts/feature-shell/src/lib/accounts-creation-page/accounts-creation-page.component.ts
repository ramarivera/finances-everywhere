import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountsFacade, createAccount } from '@dacs-fe/shared/data-access/accounts';
import { CurrenciesFacade } from '@dacs-fe/shared/data-access/currencies';
import { DepartmentsFacade } from '@dacs-fe/shared/data-access/departments';
import { AccountCreationData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-accounts-creation-page',
  templateUrl: './accounts-creation-page.component.html',
  styleUrls: ['./accounts-creation-page.component.sass'],
})
export class AccountsCreationPageComponent implements OnInit {
  departments$ = this.departmentsFacade.allDepartments$;
  currencies$ = this.currenciesFacade.allCurrencies$;

  loading$ = combineLatest(
    this.departmentsFacade.batchLoading$,
    this.currenciesFacade.batchLoading$
  ).pipe(map(loading => loading.some(Boolean)));

  constructor(
    private accountsFacade: AccountsFacade,
    private departmentsFacade: DepartmentsFacade,
    private currenciesFacade: CurrenciesFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {}

  onCancel() {
    this.navigateBack();
  }

  onAccountSave($event: AccountCreationData) {
    const eventId = this.accountsFacade.dispatch(createAccount({ creationData: $event }));

    if (eventId) {
      this.accountsFacade.allAccounts$
        .pipe(entityByEventId(eventId))
        .subscribe(_ => this.navigateBack());
    }
  }

  navigateBack() {
    this.router.navigate(['..'], { relativeTo: this.activatedRoute });
  }
}
