import { Component } from '@angular/core';

@Component({
  selector: 'dacs-fe-accounts',
  template: '<router-outlet></router-outlet>',
})
export class AccountsFeatureShellComponent {
  constructor() {}
}
