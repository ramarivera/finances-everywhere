import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { deleteAccount } from '@dacs-fe/shared/data-access/accounts';
import { FabButtonService } from '@dacs-fe/shared/ui/buttons';
import { DialogService } from '@dacs-fe/shared/ui/dialogs';
import { AccountListViewModel } from '@dacs-fe/types';
import { filter } from 'rxjs/operators';
import { AccountsFeatureShellFacade } from '../+state/accounts-feature-shell.facade';

@Component({
  selector: 'dacs-fe-account-list-page',
  templateUrl: './accounts-list-page.component.html',
})
export class AccountsListPageComponent implements OnDestroy, OnInit {
  loaded$ = this.facade.hasLoaded$;
  isLoading$ = this.facade.batchLoading$;
  accounts$ = this.facade.accountListViewModels$;

  buttonSubscription!: () => void;

  constructor(
    private facade: AccountsFeatureShellFacade,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private fabButtonService: FabButtonService
  ) {}

  ngOnInit() {
    this.buttonSubscription = this.fabButtonService.addButton({
      icon: 'account_balance',
      tooltip: 'New account',
      callback: () => this.onNewAccountClicked(),
    });
  }

  ngOnDestroy() {
    this.buttonSubscription();
  }

  onNewAccountClicked() {
    this.router.navigate(['./new'], { relativeTo: this.route.parent });
  }

  onUpdate(data: AccountListViewModel) {
    this.router.navigate([data.accountId, 'edit'], { relativeTo: this.route.parent });
  }

  onDelete(data: AccountListViewModel) {
    this.dialogService
      .openConfirmation(
        'Confirm account deletion',
        `Are you sure you want to delete the '${data.name}' account? This operation cannot be undone`
      )
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.facade.dispatch(deleteAccount({ accountId: data.accountId }));
      });
  }
}
