import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountsUiModule } from '@dacs-fe/accounts/ui';
import { SharedUiLayoutModule } from '@dacs-fe/shared/ui/layout';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { EffectsModule } from '@ngrx/effects';
import { AccountsCreationPageComponent } from './accounts-creation-page/accounts-creation-page.component';
import { AccountsFeatureShellComponent } from './accounts-feature-shell.component';
import { AccountsListPageComponent } from './accounts-list-page/accounts-list-page.component';
import { AccountsUpdatePageComponent } from './accounts-update-page/accounts-update-page.component';
import { AccountsFeatureShellEffects } from './+state/accounts-feature-shell.effects';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    SharedUiLayoutModule,
    AccountsUiModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccountsFeatureShellComponent,
        children: [
          { path: '', component: AccountsListPageComponent },
          { path: 'new', component: AccountsCreationPageComponent },
          {
            path: ':id/edit',
            component: AccountsUpdatePageComponent,
          },
        ],
      },
    ]),
    EffectsModule.forFeature([AccountsFeatureShellEffects]),
  ],
  declarations: [
    AccountsFeatureShellComponent,
    AccountsListPageComponent,
    AccountsUpdatePageComponent,
    AccountsCreationPageComponent,
  ],
})
export class AccountsFeatureShellModule {}
