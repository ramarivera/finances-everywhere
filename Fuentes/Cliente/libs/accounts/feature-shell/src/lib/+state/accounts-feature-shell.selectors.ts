import { accountsQuery } from '@dacs-fe/shared/data-access/accounts';
import { currenciesQuery } from '@dacs-fe/shared/data-access/currencies';
import { departmentsQuery } from '@dacs-fe/shared/data-access/departments';
import { AccountListViewModel } from '@dacs-fe/types';
import { createSelector } from '@ngrx/store';

export const getAccountListViewModels = createSelector(
  accountsQuery.getAllAccounts,
  departmentsQuery.getDepartmentsEntities,
  currenciesQuery.getAllCurrencies,
  (accounts, departments, currencies) => {
    return accounts.map(account => {
      return {
        ...account,
        department: departments[account.departmentId],
        currency: currencies.find(x => x.currencyId === account.currencyId),
      } as AccountListViewModel;
    });
  }
);
