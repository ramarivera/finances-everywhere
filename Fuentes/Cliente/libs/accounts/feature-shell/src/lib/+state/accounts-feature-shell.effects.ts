import { Injectable } from '@angular/core';
import { currenciesQuery, fetchCurrencies } from '@dacs-fe/shared/data-access/currencies';
import { departmentsQuery, fetchDepartments } from '@dacs-fe/shared/data-access/departments';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountsFeatureShellComponent } from '../accounts-feature-shell.component';
import { AccountsUpdatePageComponent } from '../accounts-update-page/accounts-update-page.component';
import { accountsFeatureActivated, loadAccountForUpdate } from './accounts-feature-shell.actions';
import {
  AccountsPartialState,
  accountsQuery,
  fetchAccounts,
  selectAccount,
  updateAccountSuccess,
  deleteAccountSuccess,
  createAccountSuccess,
} from '@dacs-fe/shared/data-access/accounts';

@Injectable()
export class AccountsFeatureShellEffects {
  constructor(
    private dataPersistence: DataPersistence<AccountsPartialState>,
    private actions$: Actions,
    private snackbarService: SnackBarService
  ) {}

  accountsNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(AccountsFeatureShellComponent, {
      run: routeSnapshot => {
        if (routeSnapshot.component === AccountsFeatureShellComponent) {
          return accountsFeatureActivated();
        }
      },
    })
  );

  accountsFeatureActivated$ = createEffect(() =>
    this.dataPersistence.fetch(accountsFeatureActivated, {
      run: (_, state) => {
        if (!state) {
          return;
        }

        const actions = [];

        if (!accountsQuery.getBatchLoading(state)) {
          actions.push(fetchAccounts());
        }

        if (!departmentsQuery.getBatchLoading(state)) {
          actions.push(fetchDepartments());
        }

        if (!currenciesQuery.getBatchLoading(state)) {
          actions.push(fetchCurrencies());
        }

        if (actions.length) {
          return from(actions);
        }
      },
    })
  );

  accountsUpdateNavigation$ = createEffect(() =>
    this.dataPersistence.navigation(AccountsUpdatePageComponent, {
      run: snapshot => loadAccountForUpdate({ accountId: snapshot.params.id }),
    })
  );

  accountsUpdateLoadEffect$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof loadAccountForUpdate>>(loadAccountForUpdate, {
      run: ({ accountId }) => selectAccount({ accountId }),
    })
  );

  accountSuccessfullyCreated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createAccountSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Account created'))
      ),
    { dispatch: false }
  );

  accountSuccessfullyUpdated$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateAccountSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Account updated'))
      ),
    { dispatch: false }
  );

  accountSuccessfullyDeleted$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteAccountSuccess),
        map(() => this.snackbarService.openSnackBar('success', 'Account deleted'))
      ),
    { dispatch: false }
  );
}
