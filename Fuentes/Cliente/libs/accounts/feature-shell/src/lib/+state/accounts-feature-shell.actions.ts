import { createAction, props } from '@ngrx/store';

export const accountsFeatureActivated = createAction('[Accounts/Feature] accounts activated');

export const loadAccountForUpdate = createAction(
  '[Accounts/Feature] load account for update',
  props<{ accountId: number }>()
);
