import { Injectable } from '@angular/core';
import { AccountsFacade, AccountsPartialState } from '@dacs-fe/shared/data-access/accounts';
import { select, Store } from '@ngrx/store';
import { getAccountListViewModels } from './accounts-feature-shell.selectors';

@Injectable({ providedIn: 'root' })
export class AccountsFeatureShellFacade extends AccountsFacade {
  accountListViewModels$ = this.store.pipe(select(getAccountListViewModels));

  constructor(protected store: Store<AccountsPartialState>) {
    super(store);
  }
}
