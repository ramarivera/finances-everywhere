import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountsFacade, updateAccount } from '@dacs-fe/shared/data-access/accounts';
import { CurrenciesFacade } from '@dacs-fe/shared/data-access/currencies';
import { DepartmentsFacade } from '@dacs-fe/shared/data-access/departments';
import { AccountUpdateData } from '@dacs-fe/types';
import { entityByEventId } from '@dacs-fe/utils/state';
import { combineLatest } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'dacs-fe-accounts-update-page',
  templateUrl: './accounts-update-page.component.html',
  styleUrls: ['./accounts-update-page.component.sass'],
})
export class AccountsUpdatePageComponent {
  loading$ = combineLatest(
    this.facade.singleLoading$,
    this.currenciesFacade.batchLoading$,
    this.departmentsFacade.batchLoading$
  ).pipe(map(loadings => loadings.some(Boolean)));
  selected$ = this.facade.selectedAccount$;
  currencies$ = this.currenciesFacade.allCurrencies$;
  departments$ = this.departmentsFacade.allDepartments$;

  constructor(
    private facade: AccountsFacade,
    private currenciesFacade: CurrenciesFacade,
    private departmentsFacade: DepartmentsFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  onCancel() {
    this.navigateBack();
  }

  async onAccountSave($event: AccountUpdateData) {
    const params = await this.activatedRoute.params.pipe(take(1)).toPromise();
    const eventId = this.facade.dispatch(
      updateAccount({ accountId: params.id, updateData: $event })
    );
    if (eventId) {
      const updatedAccount = await this.facade.allAccounts$
        .pipe(entityByEventId(eventId))
        .toPromise();
      if (updatedAccount) {
        this.navigateBack();
      }
    }
  }

  navigateBack() {
    this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
  }
}
