import { Action, ActionReducer, createSelector, MemoizedSelector } from '@ngrx/store';
import { CallState, CallStateUnion } from './models';

export interface Triggers {
  loading: string[];
  resting: string[];
  erroring: string[];
}

export function callStateReducer<S extends CallState, A extends Action>(
  baseReducer: ActionReducer<S, A>,
  triggers: {
    single?: Triggers;
    batch?: Triggers;
  }
) {
  return (state: S, action: A) => {
    const singleCallState = extractCallState(action, triggers.single);
    const batchCallState = extractCallState(action, triggers.batch);

    return baseReducer(
      singleCallState || batchCallState
        ? {
            ...state,
            callState: {
              single: singleCallState || state.callState.single,
              batch: batchCallState || state.callState.batch,
            },
          }
        : state,
      action
    );
  };
}

function extractCallState<A extends Action>(action: A, triggers?: Triggers): CallStateUnion | null {
  if (!triggers) {
    return null;
  }

  switch (true) {
    case triggers.loading.includes(action.type):
      return 'loading';

    case triggers.resting.includes(action.type):
      return 'resting';

    case triggers.erroring.includes(action.type):
      return (<{ error?: any }>action).error;

    default:
      return null;
  }
}

export function getCallStateError(callState: CallStateUnion) {
  if (typeof callState === 'string') {
    return null;
  }

  return callState.error;
}

export function getCallStateSelectors<T extends CallState>(featureSelector: MemoizedSelector<object, T>) {
  const getCallState = createSelector(
    featureSelector,
    featureState => featureState.callState
  );

  const isSingleLoading = createSelector(
    getCallState,
    callState => callState.single === 'loading'
  );

  const isSingleResting = createSelector(
    getCallState,
    callState => callState.single === 'resting'
  );

  const getSingleError = createSelector(
    getCallState,
    callState => getCallStateError(callState.single)
  );

  const isBatchLoading = createSelector(
    getCallState,
    callState => callState.batch === 'loading'
  );

  const isBatchResting = createSelector(
    getCallState,
    callState => callState.batch === 'resting'
  );

  const getBatchError = createSelector(
    getCallState,
    callState => getCallStateError(callState.batch)
  );

  return {
    isSingleLoading,
    isSingleResting,
    getSingleError,
    isBatchLoading,
    isBatchResting,
    getBatchError,
  };
}

export const initialCallState: CallState = {
  callState: {
    single: 'resting',
    batch: 'resting',
  },
};
