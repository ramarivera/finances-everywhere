import * as _ from 'lodash';
import { pipe } from 'rxjs';
import { distinctUntilChanged, filter, map, take } from 'rxjs/operators';
import { DacsDetailEntity, DacsEntity } from './models';

export function markAsFull<TEntity>(
  entity: DacsEntity<TEntity>,
  eventId?: string
): DacsEntity<TEntity> {
  return { _metadata: { isFull: true, ...(eventId ? { eventId } : {}) }, ...entity };
}

export function isDetailsEntity<TEntity, TEntityDetails>(
  entity: DacsEntity<TEntity, TEntityDetails>
): entity is DacsDetailEntity<TEntityDetails> {
  return (entity as any)?._metadata != null;
}

function onlyDetailEntities<TEntity, TDetailsEntity>() {
  return map((entities: DacsEntity<TEntity, TDetailsEntity>[]) => {
    return entities.filter(isDetailsEntity);
  });
}

export function entityByEventId<TEntity, TDetailsEntity>(eventId: string) {
  return pipe(
    onlyDetailEntities<TEntity, TDetailsEntity>(),
    map((entities) => entities.find((x) => x._metadata.eventId === eventId)),
    filter(isDefined),
    distinctUntilChanged(),
    take(1)
  );
}

export function isDefined<TValue>(value: TValue | undefined | null): value is TValue {
  return Boolean(value);
}
