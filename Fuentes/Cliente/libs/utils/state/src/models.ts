export type LoadingState = 'loading' | 'resting';

export interface ErrorState {
  error: any;
}

export type CallStateUnion = LoadingState | ErrorState;

export interface CallState {
  callState: {
    single: CallStateUnion;
    batch: CallStateUnion;
  };
}

export interface EntityStateMetadata {
  _metadata: {
    isFull: boolean;
    eventId: string;
  };
}

export type DacsDetailEntity<TEntityDetails> = TEntityDetails & EntityStateMetadata;
export type DacsEntity<TEntity, TEntityDetails = TEntity> = TEntity | DacsDetailEntity<TEntityDetails>;
