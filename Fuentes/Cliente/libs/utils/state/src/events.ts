import { props } from '@ngrx/store';
import { Props } from '@ngrx/store/src/models';
import { uniqueId } from 'lodash';

interface EventPayload {
  eventId: string;
}

type WithEventId<T> = T & EventPayload;

type EventBuilder = () => EventPayload;

export function errorProps() {
  return props<WithEventId<{ error?: any }>>();
}

export function createEventBuilder(namespace: string): EventBuilder {
  return () => ({
    eventId: uniqueId(namespace),
  });
}

export function createEventCreatorFactory(eventBuilderOrNamespace: EventBuilder | string) {
  const eventBuilder =
    typeof eventBuilderOrNamespace === 'function'
      ? eventBuilderOrNamespace
      : createEventBuilder(eventBuilderOrNamespace);

  // Empty overload, defaults to EventPayload
  function payloadBuilder(): () => EventPayload;
  function payloadBuilder<TIn extends object>(
    payloadProps: Props<TIn>
  ): (payload: TIn) => WithEventId<TIn>;
  function payloadBuilder<TIn extends object>(
    payloadProps?: Props<TIn>
  ): (() => EventPayload) | ((payload: TIn) => WithEventId<TIn>) {
    if (payloadProps === undefined) {
      return () => eventBuilder();
    }

    return (payload: TIn) => ({
      ...payload,
      ...eventBuilder(),
    });
  }

  return payloadBuilder;
}

export function withEventPayload<TIn>() {
  return props<WithEventId<TIn>>();
}
