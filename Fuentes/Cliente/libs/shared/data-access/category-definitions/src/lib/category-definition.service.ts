import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BaseApiUrl } from '@dacs-fe/utils/environment';
import {
  CategoryDefinition,
  CategoryDefinitionCreationData,
  CategoryDefinitionUpdateData,
  CategoryDefinitionDetails,
} from '@dacs-fe/types';

@Injectable()
export class CategoryDefinitionService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'category-definitions';
  }

  getCategoryDefinitions() {
    return this.http.get<CategoryDefinition[]>(this.baseApiUrl);
  }

  getCategoryDefinition(categoryDefinitionId: number) {
    return this.http.get<CategoryDefinitionDetails>(`${this.baseApiUrl}/${categoryDefinitionId}`);
  }

  createCategoryDefinition(creationData: CategoryDefinitionCreationData) {
    return this.http.post<CategoryDefinitionDetails>(this.baseApiUrl, creationData);
  }

  deleteCategoryDefinition(categoryDefinitionId: number) {
    return this.http.delete<void>(`${this.baseApiUrl}/${categoryDefinitionId}`);
  }

  updateCategoryDefinition(categoryDefinitionId: number, categoryToUpdate: CategoryDefinitionUpdateData) {
    return this.http.put<CategoryDefinitionDetails>(`${this.baseApiUrl}/${categoryDefinitionId}`, categoryToUpdate);
  }
}
