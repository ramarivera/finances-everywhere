import { CategoryDefinition, CategoryDefinitionDetails } from '@dacs-fe/types';
import { CallState, callStateReducer, DacsEntity, initialCallState, markAsFull } from '@dacs-fe/utils/state';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as categoryDefinitionActions from './category-definitions.actions';

export type CategoryDefinitionEntity = DacsEntity<CategoryDefinition, CategoryDefinitionDetails>;

export const CATEGORYDEFINITIONS_FEATURE_KEY = 'categoryDefinitions';

export const adapter = createEntityAdapter<CategoryDefinitionEntity>({
  selectId: categoryDefinition => categoryDefinition.categoryDefinitionId,
  sortComparer: (first, second) => first.name.localeCompare(second.name),
});

export interface CategoryDefinitionsState extends EntityState<CategoryDefinitionEntity>, CallState {
  selectedId?: number;
  loaded: boolean;
}

export interface CategoryDefinitionsPartialState {
  readonly [CATEGORYDEFINITIONS_FEATURE_KEY]: CategoryDefinitionsState;
}

export const initialState: CategoryDefinitionsState = adapter.getInitialState({ ...initialCallState, loaded: false });

export const reducer = createReducer(
  initialState,

  on(categoryDefinitionActions.createCategoryDefinitionSuccess, (state, { categoryDefinition, eventId }) =>
    adapter.addOne(markAsFull(categoryDefinition, eventId), state)
  ),

  on(categoryDefinitionActions.updateCategoryDefinitionSuccess, (state, update) =>
    adapter.updateOne({ id: update.id, changes: markAsFull(update.changes, update.eventId) }, state)
  ),

  on(categoryDefinitionActions.deleteCategoryDefinitionSuccess, (state, { categoryDefinitionId }) =>
    adapter.removeOne(categoryDefinitionId, state)
  ),

  on(categoryDefinitionActions.fetchCategoryDefinitionsSuccess, (state, { categoryDefinitions }) =>
    adapter.addAll(categoryDefinitions, { ...state, loaded: true })
  ),

  on(categoryDefinitionActions.fetchCategoryDefinitionSuccess, (state, { categoryDefinition }) =>
    adapter.upsertOne(markAsFull(categoryDefinition), state)
  ),

  on(categoryDefinitionActions.selectCategoryDefinition, (state, { categoryDefinitionId }) => ({
    ...state,
    selectedId: categoryDefinitionId,
  }))
);

export const categoryDefinitionsCallStateTriggers = {
  single: {
    loading: [
      categoryDefinitionActions.fetchCategoryDefinition,
      categoryDefinitionActions.updateCategoryDefinition,
      categoryDefinitionActions.deleteCategoryDefinition,
      categoryDefinitionActions.createCategoryDefinition,
    ].map(x => x.type),
    resting: [
      categoryDefinitionActions.fetchCategoryDefinitionSuccess,
      categoryDefinitionActions.updateCategoryDefinitionSuccess,
      categoryDefinitionActions.deleteCategoryDefinitionSuccess,
      categoryDefinitionActions.createCategoryDefinitionSuccess,
    ].map(x => x.type),
    erroring: [
      categoryDefinitionActions.fetchCategoryDefinitionFailure,
      categoryDefinitionActions.updateCategoryDefinitionFailure,
      categoryDefinitionActions.deleteCategoryDefinitionFailure,
      categoryDefinitionActions.createCategoryDefinitionFailure,
    ].map(x => x.type),
  },
  batch: {
    loading: [categoryDefinitionActions.fetchCategoryDefinitions.type],
    resting: [categoryDefinitionActions.fetchCategoryDefinitionsSuccess.type],
    erroring: [categoryDefinitionActions.fetchCategoryDefinitionsFailure.type],
  },
};

export function categoryDefinitionsReducer(
  state: CategoryDefinitionsState = initialState,
  action: Action
): CategoryDefinitionsState {
  return callStateReducer(reducer, categoryDefinitionsCallStateTriggers)(state, action);
}
