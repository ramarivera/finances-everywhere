import { createAction, props } from '@ngrx/store';
import { uniqueId } from 'lodash';
import {
  CategoryDefinition,
  CategoryDefinitionDetails,
  CategoryDefinitionCreationData,
  CategoryDefinitionUpdateData,
} from '@dacs-fe/types';

const errorProps = props<{ error?: any; eventId?: string }>();

const event = () => () => {
  return {
    eventId: generateId(),
  };
};

const eventPayload = <TIn>() => (payload: TIn) => {
  return {
    ...payload,
    eventId: generateId(),
  };
};

const eventProps = <TIn>() => props<TIn & { eventId: string }>();

const generateId = () => uniqueId('catdefid_');

export const fetchCategoryDefinition = createAction(
  '[Category Definitions/API] fetch category definition',
  eventPayload<{ categoryDefinitionId: number }>()
);

export const fetchCategoryDefinitionSuccess = createAction(
  '[Category Definitions/API] fetch category definition success',
  eventProps<{ categoryDefinition: CategoryDefinitionDetails }>()
);

export const fetchCategoryDefinitionFailure = createAction(
  '[Category Definitions/API] fetch category definition failure',
  errorProps
);

export const fetchCategoryDefinitions = createAction('[Category Definitions/API] fetch category definitions', event());

export const fetchCategoryDefinitionsSuccess = createAction(
  '[Category Definitions/API] fetch category definitions success',
  eventProps<{ categoryDefinitions: CategoryDefinition[] }>()
);

export const fetchCategoryDefinitionsFailure = createAction(
  '[Category Definitions/API] fetch category definitions failure',
  errorProps
);

export const createCategoryDefinition = createAction(
  '[Category Definitions/API] create category definition',
  eventPayload<{ creationData: CategoryDefinitionCreationData }>()
);

export const createCategoryDefinitionSuccess = createAction(
  '[Category Definitions/API] create category definition success',
  eventProps<{ categoryDefinition: CategoryDefinitionDetails }>()
);

export const createCategoryDefinitionFailure = createAction(
  '[Category Definitions/API] create category definition failure',
  errorProps
);

export const deleteCategoryDefinition = createAction(
  '[Category Definitions/API] delete category definition',
  eventPayload<{ categoryDefinitionId: number }>()
);

export const deleteCategoryDefinitionSuccess = createAction(
  '[Category Definitions/API] delete category definition success',
  eventProps<{ categoryDefinitionId: number }>()
);

export const deleteCategoryDefinitionFailure = createAction(
  '[Category Definitions/API] delete category definition failure',
  errorProps
);

export const updateCategoryDefinition = createAction(
  '[Category Definitions/API] update category definition',
  eventPayload<{ categoryDefinitionId: number; updateData: CategoryDefinitionUpdateData }>()
);

export const updateCategoryDefinitionSuccess = createAction(
  '[Category Definitions/API] update category definition success',
  (category: CategoryDefinitionDetails, eventId: string) => ({
    id: category.categoryDefinitionId,
    changes: category,
    eventId,
  })
);

export const updateCategoryDefinitionFailure = createAction(
  '[Category Definitions/API] update category definition failure',
  errorProps
);

export const selectCategoryDefinition = createAction(
  '[Category Definitions] select category definition',
  props<{ categoryDefinitionId: number }>()
);
