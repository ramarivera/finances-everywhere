import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { CategoryDefinitionsPartialState } from './category-definitions.reducer';
import { categoryDefinitionsQuery } from './category-definitions.selectors';

@Injectable()
export class CategoryDefinitionsFacade {
  hasLoaded$ = this.store.pipe(select(categoryDefinitionsQuery.getHasLoaded));
  batchLoading$ = this.store.pipe(select(categoryDefinitionsQuery.getBatchLoading));
  singleLoading$ = this.store.pipe(select(categoryDefinitionsQuery.getSingleLoading));
  allCategoryDefinitions$ = this.store.pipe(
    select(categoryDefinitionsQuery.getAllCategoryDefinitions)
  );
  selectedCategoryDefinition$ = this.store.pipe(
    select(categoryDefinitionsQuery.getSelectedCategoryDefinition)
  );

  constructor(private store: Store<CategoryDefinitionsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
