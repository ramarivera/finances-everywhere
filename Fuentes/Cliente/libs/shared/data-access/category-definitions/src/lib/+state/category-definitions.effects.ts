import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map, tap } from 'rxjs/operators';
import { CategoryDefinitionService } from '../category-definition.service';
import { CategoryDefinitionsPartialState } from './category-definitions.reducer';
import {
  fetchCategoryDefinition,
  fetchCategoryDefinitionSuccess,
  fetchCategoryDefinitionFailure,
  fetchCategoryDefinitions,
  fetchCategoryDefinitionsSuccess,
  fetchCategoryDefinitionsFailure,
  selectCategoryDefinition,
  createCategoryDefinition,
  createCategoryDefinitionSuccess,
  createCategoryDefinitionFailure,
  updateCategoryDefinition,
  updateCategoryDefinitionSuccess,
  updateCategoryDefinitionFailure,
  deleteCategoryDefinition,
  deleteCategoryDefinitionSuccess,
  deleteCategoryDefinitionFailure,
} from './category-definitions.actions';

@Injectable()
export class CategoryDefinitionsEffects {
  /**
   * Fetches the latest category definition information on selection
   */
  @Effect()
  selectCategoryDefinition$ = this.dataPersistence.fetch<ReturnType<typeof selectCategoryDefinition>>(
    selectCategoryDefinition.type,
    {
      run: ({ categoryDefinitionId }) => fetchCategoryDefinition({ categoryDefinitionId }),
    }
  );

  /**
   * Fetches category definitions
   */
  @Effect()
  loadCategoryDefinitions$ = this.dataPersistence.fetch<ReturnType<typeof fetchCategoryDefinitions>>(
    fetchCategoryDefinitions.type,
    {
      run: action => {
        return this.categoryDefinitionService.getCategoryDefinitions().pipe(
          delay(3500),
          map(categoryDefinitions => fetchCategoryDefinitionsSuccess({ categoryDefinitions, eventId: action.eventId }))
        );
      },
      onError: (action, error) => {
        return fetchCategoryDefinitionsFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  loadCategoryDefinition$ = this.dataPersistence.fetch<ReturnType<typeof fetchCategoryDefinition>>(
    fetchCategoryDefinition.type,
    {
      run: action => {
        return this.categoryDefinitionService
          .getCategoryDefinition(action.categoryDefinitionId)
          .pipe(
            map(categoryDefinition => fetchCategoryDefinitionSuccess({ categoryDefinition, eventId: action.eventId }))
          );
      },
      onError: (action, error) => {
        return fetchCategoryDefinitionFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  createCategoryDefinitionEffect$ = this.dataPersistence.fetch<ReturnType<typeof createCategoryDefinition>>(
    createCategoryDefinition.type,
    {
      run: action => {
        return this.categoryDefinitionService
          .createCategoryDefinition(action.creationData)
          .pipe(
            map(categoryDefinition => createCategoryDefinitionSuccess({ categoryDefinition, eventId: action.eventId }))
          );
      },

      onError: (action, error) => {
        return createCategoryDefinitionFailure({ eventId: action.eventId, error });
      },
    }
  );

  @Effect()
  updateCategoryDefinitionEffect$ = this.dataPersistence.fetch<ReturnType<typeof updateCategoryDefinition>>(
    updateCategoryDefinition.type,
    {
      run: ({ categoryDefinitionId, updateData, eventId }) => {
        return this.categoryDefinitionService
          .updateCategoryDefinition(categoryDefinitionId, updateData)
          .pipe(map(categoryDefinition => updateCategoryDefinitionSuccess(categoryDefinition, eventId)));
      },

      onError: ({ eventId }, error) => {
        return updateCategoryDefinitionFailure({ eventId, error });
      },
    }
  );

  @Effect()
  deleteCategoryDefinitionEffect$ = this.dataPersistence.fetch<ReturnType<typeof deleteCategoryDefinition>>(
    deleteCategoryDefinition.type,
    {
      run: ({ categoryDefinitionId, eventId }) => {
        return this.categoryDefinitionService.deleteCategoryDefinition(categoryDefinitionId).pipe(
          map(_ =>
            deleteCategoryDefinitionSuccess({
              categoryDefinitionId,
              eventId,
            })
          )
        );
      },
      onError: ({ eventId }, error) => {
        return deleteCategoryDefinitionFailure({ eventId, error });
      },
    }
  );

  errorsEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          deleteCategoryDefinitionFailure,
          createCategoryDefinitionFailure,
          updateCategoryDefinitionFailure,
          fetchCategoryDefinitionFailure,
          fetchCategoryDefinitionsFailure
        ),
        tap(_ => {
          this.snackBarService.openSnackBar('alert', 'An error has ocurred');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private snackBarService: SnackBarService,
    private dataPersistence: DataPersistence<CategoryDefinitionsPartialState>,
    private categoryDefinitionService: CategoryDefinitionService
  ) {}
}
