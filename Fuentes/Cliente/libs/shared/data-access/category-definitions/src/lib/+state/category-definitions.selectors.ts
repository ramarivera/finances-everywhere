import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  adapter,
  CATEGORYDEFINITIONS_FEATURE_KEY,
  CategoryDefinitionsState,
} from './category-definitions.reducer';

// Lookup the 'CategoryDefinitions' feature state managed by NgRx
const getCategoryDefinitionsState = createFeatureSelector<CategoryDefinitionsState>(
  CATEGORYDEFINITIONS_FEATURE_KEY
);

const {
  selectAll: getAllCategoryDefinitions,
  selectIds: getCategoryDefinitionsIds,
  selectEntities: getCategoryDefinitionsEntities,
} = adapter.getSelectors(getCategoryDefinitionsState);

const callStateSelectors = getCallStateSelectors(getCategoryDefinitionsState);

const getHasLoaded = createSelector(getCategoryDefinitionsState, (state) => state.loaded);

const getSelectedId = createSelector(getCategoryDefinitionsState, (state) => state.selectedId);

const getSelectedCategoryDefinition = createSelector(
  getCategoryDefinitionsEntities,
  getSelectedId,
  (categoryDefinitions, id) => {
    return id ? categoryDefinitions[id] : undefined;
  }
);

export const categoryDefinitionsQuery = {
  getSingleLoading: callStateSelectors.isSingleLoading,
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllCategoryDefinitions,
  getCategoryDefinitionsEntities,
  getSelectedCategoryDefinition,
  getHasLoaded,
};
