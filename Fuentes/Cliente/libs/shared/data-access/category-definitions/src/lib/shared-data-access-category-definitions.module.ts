import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CategoryDefinitionService } from './category-definition.service';
import {
  CATEGORYDEFINITIONS_FEATURE_KEY,
  categoryDefinitionsReducer,
} from './+state/category-definitions.reducer';
import { CategoryDefinitionsEffects } from './+state/category-definitions.effects';
import { CategoryDefinitionsFacade } from './+state/category-definitions.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(CATEGORYDEFINITIONS_FEATURE_KEY, categoryDefinitionsReducer),
    EffectsModule.forFeature([CategoryDefinitionsEffects]),
  ],
  providers: [CategoryDefinitionsFacade, CategoryDefinitionService],
})
export class SharedDataAccessCategoryDefinitionsModule {}
