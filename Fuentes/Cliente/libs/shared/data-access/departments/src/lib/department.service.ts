import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Department, DepartmentCreationData, DepartmentDetails, DepartmentUpdateData } from '@dacs-fe/types';
import { BaseApiUrl } from '@dacs-fe/utils/environment';

@Injectable()
export class DepartmentService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'departments';
  }

  getDepartments() {
    return this.http.get<Department[]>(this.baseApiUrl);
  }

  getDepartment(departmentId: number) {
    return this.http.get<DepartmentDetails>(`${this.baseApiUrl}/${departmentId}`);
  }

  createDepartment(creationData: DepartmentCreationData) {
    return this.http.post<DepartmentDetails>(this.baseApiUrl, creationData);
  }

  deleteDepartment(departmentId: number) {
    return this.http.delete<void>(`${this.baseApiUrl}/${departmentId}`);
  }

  updateDepartment(departmentId: number, departmentToUpdate: DepartmentUpdateData) {
    return this.http.put<DepartmentDetails>(`${this.baseApiUrl}/${departmentId}`, departmentToUpdate);
  }
}
