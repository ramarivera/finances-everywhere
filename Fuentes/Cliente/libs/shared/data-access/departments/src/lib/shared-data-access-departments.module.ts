import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DepartmentService } from './department.service';
import { CATEGORYDEFINITIONS_FEATURE_KEY, departmentsReducer } from './+state/departments.reducer';
import { DepartmentsEffects } from './+state/departments.effect';
import { DepartmentsFacade } from './+state/departments.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(CATEGORYDEFINITIONS_FEATURE_KEY, departmentsReducer),
    EffectsModule.forFeature([DepartmentsEffects]),
  ],
  providers: [DepartmentsFacade, DepartmentService],
})
export class SharedDataAccessDepartmentsModule {}
