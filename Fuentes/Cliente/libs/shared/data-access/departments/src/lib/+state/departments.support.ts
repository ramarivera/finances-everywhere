// import { DepartmentDetails } from '@dacs-fe/types';
// import { createEventCreatorFactory } from '@dacs-fe/utils/state';
// import { createAction, props } from '@ngrx/store';
// import { PropsReturnType } from '@ngrx/store/src/models';
// import { uniqueId } from 'lodash';

// const errorProps = props<WithEventId<{ error?: any }>>();

// interface EventPayload {
//   eventId: string;
// }

// type WithEventId<T> = T & EventPayload;

// const buildEvent = (): EventPayload => ({
//   eventId: uniqueId('department_'),
// });

// const withEventPayload = <TIn>() => (payload: TIn): WithEventId<TIn> => ({
//   ...payload,
//   ...buildEvent(),
// });

// const eventProps = <TIn>() => props<WithEventId<TIn>>();

// export const fetchDepartmentStart = createAction(
//   '[Departments/API] fetch department start',
//   withEventPayload<{ departmentId: number }>()
// );

// export const fetchDepartmentSuccess = createAction(
//   '[Departments/API] fetch department success',
//   eventProps<{ department: DepartmentDetails }>()
// );

// export const fetchDepartmentFailure = createAction('[Departments/API] fetch department failure', errorProps);

// function createAsyncActionCreators<TStart extends object>(
//   namespace: string,
//   action: string,
//   startProps: PropsReturnType<TStart>
// ) {
//   const eventCreatorFactory = createEventCreatorFactory(namespace);
//   const start = createAction('something', eventCreatorFactory(props<TStart>()));

//   return {
//     start,
//   };
// }
