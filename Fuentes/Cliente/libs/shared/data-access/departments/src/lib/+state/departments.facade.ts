import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { DepartmentsPartialState } from './departments.reducer';
import { departmentsQuery } from './departments.selectors';

@Injectable()
export class DepartmentsFacade {
  hasLoaded$ = this.store.pipe(select(departmentsQuery.getHasLoaded));
  batchLoading$ = this.store.pipe(select(departmentsQuery.getBatchLoading));
  singleLoading$ = this.store.pipe(select(departmentsQuery.getSingleLoading));
  allDepartments$ = this.store.pipe(select(departmentsQuery.getAllDepartments));
  selectedDepartment$ = this.store.pipe(select(departmentsQuery.getSelectedDepartment));

  constructor(private store: Store<DepartmentsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
