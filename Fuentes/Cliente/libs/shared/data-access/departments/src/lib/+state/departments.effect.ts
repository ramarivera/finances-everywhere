import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map, tap } from 'rxjs/operators';
import { DepartmentService } from '../department.service';
import { DepartmentsPartialState } from './departments.reducer';
import {
  fetchDepartment,
  fetchDepartmentSuccess,
  fetchDepartmentFailure,
  fetchDepartments,
  fetchDepartmentsSuccess,
  fetchDepartmentsFailure,
  selectDepartment,
  createDepartment,
  createDepartmentSuccess,
  createDepartmentFailure,
  updateDepartment,
  updateDepartmentSuccess,
  updateDepartmentFailure,
  deleteDepartment,
  deleteDepartmentSuccess,
  deleteDepartmentFailure,
} from './departments.actions';

@Injectable()
export class DepartmentsEffects {
  /**
   * Fetches the latest department information on selection
   */
  @Effect()
  selectDepartment$ = this.dataPersistence.fetch<ReturnType<typeof selectDepartment>>(selectDepartment, {
    run: ({ departmentId }) => fetchDepartment({ departmentId }),
  });

  /**
   * Fetches departments
   */
  @Effect()
  loadDepartments$ = this.dataPersistence.fetch<ReturnType<typeof fetchDepartments>>(fetchDepartments, {
    run: action => {
      return this.departmentService.getDepartments().pipe(
        delay(3500),
        map(departments => fetchDepartmentsSuccess({ departments, eventId: action.eventId }))
      );
    },
    onError: (action, error) => {
      return fetchDepartmentsFailure({ error, eventId: action.eventId });
    },
  });

  @Effect()
  loadDepartment$ = this.dataPersistence.fetch<ReturnType<typeof fetchDepartment>>(fetchDepartment, {
    run: action => {
      return this.departmentService
        .getDepartment(action.departmentId)
        .pipe(map(department => fetchDepartmentSuccess({ department, eventId: action.eventId })));
    },
    onError: (action, error) => {
      return fetchDepartmentFailure({ error, eventId: action.eventId });
    },
  });

  @Effect()
  createDepartmentEffect$ = this.dataPersistence.fetch<ReturnType<typeof createDepartment>>(createDepartment, {
    run: action => {
      return this.departmentService
        .createDepartment(action.creationData)
        .pipe(map(department => createDepartmentSuccess({ department, eventId: action.eventId })));
    },

    onError: (action, error) => {
      return createDepartmentFailure({ eventId: action.eventId, error });
    },
  });

  @Effect()
  updateDepartmentEffect$ = this.dataPersistence.fetch<ReturnType<typeof updateDepartment>>(updateDepartment, {
    run: ({ departmentId, updateData, eventId }) => {
      return this.departmentService
        .updateDepartment(departmentId, updateData)
        .pipe(map(department => updateDepartmentSuccess(department, eventId)));
    },

    onError: ({ eventId }, error) => {
      return updateDepartmentFailure({ eventId, error });
    },
  });

  @Effect()
  deleteDepartmentEffect$ = this.dataPersistence.fetch<ReturnType<typeof deleteDepartment>>(deleteDepartment, {
    run: ({ departmentId, eventId }) => {
      return this.departmentService.deleteDepartment(departmentId).pipe(
        map(_ =>
          deleteDepartmentSuccess({
            departmentId,
            eventId,
          })
        )
      );
    },
    onError: ({ eventId }, error) => {
      return deleteDepartmentFailure({ eventId, error });
    },
  });

  errorsEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          deleteDepartmentFailure,
          createDepartmentFailure,
          updateDepartmentFailure,
          fetchDepartmentFailure,
          fetchDepartmentsFailure
        ),
        tap(_ => {
          this.snackBarService.openSnackBar('alert', 'An error has ocurred');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private snackBarService: SnackBarService,
    private dataPersistence: DataPersistence<DepartmentsPartialState>,
    private departmentService: DepartmentService
  ) {}
}
