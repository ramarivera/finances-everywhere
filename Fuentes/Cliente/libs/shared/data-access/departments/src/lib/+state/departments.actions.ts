import { createEventCreatorFactory, errorProps, withEventPayload } from '@dacs-fe/utils/state';
import { createAction, props } from '@ngrx/store';
import {
  Department,
  DepartmentCreationData,
  DepartmentDetails,
  DepartmentUpdateData,
} from '@dacs-fe/types';

const eventCreatorFactory = createEventCreatorFactory('department');

export const fetchDepartment = createAction(
  '[Departments/API] fetch department start',
  eventCreatorFactory(props<{ departmentId: number }>())
);

export const fetchDepartmentSuccess = createAction(
  '[Departments/API] fetch department success',
  withEventPayload<{ department: DepartmentDetails }>()
);

export const fetchDepartmentFailure = createAction(
  '[Departments/API] fetch department failure',
  errorProps()
);

export const fetchDepartments = createAction(
  '[Departments/API] fetch departments',
  eventCreatorFactory()
);

export const fetchDepartmentsSuccess = createAction(
  '[Departments/API] fetch departments success',
  withEventPayload<{ departments: Department[] }>()
);

export const fetchDepartmentsFailure = createAction(
  '[Departments/API] fetch departments failure',
  errorProps()
);

export const createDepartment = createAction(
  '[Departments/API] create department',
  withEventPayload<{ creationData: DepartmentCreationData }>()
);

export const createDepartmentSuccess = createAction(
  '[Departments/API] create department success',
  withEventPayload<{ department: DepartmentDetails }>()
);

export const createDepartmentFailure = createAction(
  '[Departments/API] create department failure',
  errorProps()
);

export const deleteDepartment = createAction(
  '[Departments/API] delete department',
  eventCreatorFactory(props<{ departmentId: number }>())
);

export const deleteDepartmentSuccess = createAction(
  '[Departments/API] delete department success',
  withEventPayload<{ departmentId: number }>()
);

export const deleteDepartmentFailure = createAction(
  '[Departments/API] delete department failure',
  errorProps()
);

export const updateDepartment = createAction(
  '[Departments/API] update department',
  eventCreatorFactory(props<{ departmentId: number; updateData: DepartmentUpdateData }>())
);

export const updateDepartmentSuccess = createAction(
  '[Departments/API] update department success',
  (department: DepartmentDetails, eventId: string) => ({
    id: department.departmentId,
    changes: department,
    eventId,
  })
);

export const updateDepartmentFailure = createAction(
  '[Departments/API] update department failure',
  errorProps()
);

export const selectDepartment = createAction(
  '[Departments] select department',
  props<{ departmentId: number }>()
);
