import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, CATEGORYDEFINITIONS_FEATURE_KEY, DepartmentsState } from './departments.reducer';

// Lookup the 'Departments' feature state managed by NgRx
const getDepartmentsState = createFeatureSelector<DepartmentsState>(
  CATEGORYDEFINITIONS_FEATURE_KEY
);

const {
  selectAll: getAllDepartments,
  selectIds: getDepartmentsIds,
  selectEntities: getDepartmentsEntities,
} = adapter.getSelectors(getDepartmentsState);

const callStateSelectors = getCallStateSelectors(getDepartmentsState);

const getHasLoaded = createSelector(
  getDepartmentsState,
  state => state.loaded
);

const getSelectedId = createSelector(
  getDepartmentsState,
  state => state.selectedId
);

const getSelectedDepartment = createSelector(
  getDepartmentsEntities,
  getSelectedId,
  (departments, id) => {
    return id ? departments[id] : undefined;
  }
);

export const departmentsQuery = {
  getSingleLoading: callStateSelectors.isSingleLoading,
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllDepartments,
  getDepartmentsEntities,
  getSelectedDepartment,
  getHasLoaded,
};
