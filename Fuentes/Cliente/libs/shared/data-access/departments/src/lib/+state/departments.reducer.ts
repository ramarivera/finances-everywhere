import { Department, DepartmentDetails } from '@dacs-fe/types';
import { CallState, callStateReducer, DacsEntity, initialCallState, markAsFull } from '@dacs-fe/utils/state';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as departmentActions from './departments.actions';

export type DepartmentEntity = DacsEntity<Department, DepartmentDetails>;

export const CATEGORYDEFINITIONS_FEATURE_KEY = 'departments';

export const adapter = createEntityAdapter<DepartmentEntity>({
  selectId: department => department.departmentId,
  sortComparer: (first, second) => first.name.localeCompare(second.name),
});

export interface DepartmentsState extends EntityState<DepartmentEntity>, CallState {
  selectedId?: number;
  loaded: boolean;
}

export interface DepartmentsPartialState {
  readonly [CATEGORYDEFINITIONS_FEATURE_KEY]: DepartmentsState;
}

export const initialState: DepartmentsState = adapter.getInitialState({ ...initialCallState, loaded: false });

export const reducer = createReducer(
  initialState,

  on(departmentActions.createDepartmentSuccess, (state, { department, eventId }) =>
    adapter.addOne(markAsFull(department, eventId), state)
  ),

  on(departmentActions.updateDepartmentSuccess, (state, update) =>
    adapter.updateOne({ id: update.id, changes: markAsFull(update.changes, update.eventId) }, state)
  ),

  on(departmentActions.deleteDepartmentSuccess, (state, { departmentId }) => adapter.removeOne(departmentId, state)),

  on(departmentActions.fetchDepartmentsSuccess, (state, { departments }) =>
    adapter.addAll(departments, { ...state, loaded: true })
  ),

  on(departmentActions.fetchDepartmentSuccess, (state, { department }) =>
    adapter.upsertOne(markAsFull(department), state)
  ),

  on(departmentActions.selectDepartment, (state, { departmentId }) => ({
    ...state,
    selectedId: departmentId,
  }))
);

export const departmentsCallStateTriggers = {
  single: {
    loading: [
      departmentActions.fetchDepartment,
      departmentActions.updateDepartment,
      departmentActions.deleteDepartment,
      departmentActions.createDepartment,
    ].map(x => x.type),
    resting: [
      departmentActions.fetchDepartmentSuccess,
      departmentActions.updateDepartmentSuccess,
      departmentActions.deleteDepartmentSuccess,
      departmentActions.createDepartmentSuccess,
    ].map(x => x.type),
    erroring: [
      departmentActions.fetchDepartmentFailure,
      departmentActions.updateDepartmentFailure,
      departmentActions.deleteDepartmentFailure,
      departmentActions.createDepartmentFailure,
    ].map(x => x.type),
  },
  batch: {
    loading: [departmentActions.fetchDepartments.type],
    resting: [departmentActions.fetchDepartmentsSuccess.type],
    erroring: [departmentActions.fetchDepartmentsFailure.type],
  },
};

export function departmentsReducer(state: DepartmentsState = initialState, action: Action): DepartmentsState {
  return callStateReducer(reducer, departmentsCallStateTriggers)(state, action);
}
