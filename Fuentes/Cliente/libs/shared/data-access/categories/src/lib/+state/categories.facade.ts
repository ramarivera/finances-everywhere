import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import * as fromCategories from './categories.reducer';
import { categoriesQuery } from './categories.selectors';

@Injectable()
export class CategoriesFacade {
  loading$ = this.store.pipe(select(categoriesQuery.getBatchLoading));
  loaded$ = this.store.pipe(select(categoriesQuery.getHasLoaded));
  allCategories$ = this.store.pipe(select(categoriesQuery.getAllCategories));

  constructor(private store: Store<fromCategories.CategoriesPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
