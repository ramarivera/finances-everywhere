import { Category } from '@dacs-fe/types';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as transactionActions from './categories.actions';
import {
  CallState,
  callStateReducer,
  DacsEntity,
  initialCallState,
} from '@dacs-fe/utils/state';

export type CategoryEntity = DacsEntity<Category>;

export const CATEGORIES_FEATURE_KEY = 'Categories';

export const adapter = createEntityAdapter<CategoryEntity>({
  selectId: category => category.categoryId,
});

export interface CategoriesState extends EntityState<CategoryEntity>, CallState {
  loaded: boolean;
}

export interface CategoriesPartialState {
  readonly [CATEGORIES_FEATURE_KEY]: CategoriesState;
}

export const initialState: CategoriesState = adapter.getInitialState({
  ...initialCallState,
  loaded: false,
});

export const reducer = createReducer(
  initialState,

  on(transactionActions.fetchCategoriesSuccess, (state, { categories }) =>
    adapter.addAll(categories, { ...state, loaded: true })
  )
);

export const CategoriesCallStateTriggers = {
  batch: {
    loading: [transactionActions.fetchCategories.type],
    resting: [transactionActions.fetchCategoriesSuccess.type],
    erroring: [transactionActions.fetchCategoriesFailure.type],
  },
};

export function CategoriesReducer(
  state: CategoriesState = initialState,
  action: Action
): CategoriesState {
  return callStateReducer(reducer, CategoriesCallStateTriggers)(state, action);
}
