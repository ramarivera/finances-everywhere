import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CategoriesService } from './categories.service';
import * as fromCategories from './+state/categories.reducer';
import { CategoriesEffects } from './+state/categories.effects';
import { CategoriesFacade } from './+state/categories.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromCategories.CATEGORIES_FEATURE_KEY, fromCategories.reducer),
    EffectsModule.forFeature([CategoriesEffects]),
  ],
  providers: [CategoriesFacade, CategoriesService],
})
export class SharedDataAccessCategoriesModule {}
