import { Injectable } from '@angular/core';
import { Actions, createEffect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map } from 'rxjs/operators';
import { CategoriesService } from '../categories.service';
import { CategoriesPartialState } from './categories.reducer';
import {
  fetchCategories,
  fetchCategoriesSuccess,
  fetchCategoriesFailure,
} from './categories.actions';

@Injectable()
export class CategoriesEffects {
  loadCategories$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof fetchCategories>>(fetchCategories, {
      run: action => {
        return this.categoriesService.getCategories().pipe(
          delay(3500),
          map(categories => fetchCategoriesSuccess({ categories, eventId: action.eventId }))
        );
      },
      onError: (action, error) => {
        return fetchCategoriesFailure({ error, eventId: action.eventId });
      },
    })
  );

  constructor(
    private actions$: Actions,
    private categoriesService: CategoriesService,
    private dataPersistence: DataPersistence<CategoriesPartialState>
  ) {}
}
