import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Category } from '@dacs-fe/types';
import { BaseApiUrl } from '@dacs-fe/utils/environment';

@Injectable()
export class CategoriesService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'categories';
  }

  getCategories() {
    return this.http.get<Category[]>(this.baseApiUrl);
  }
}
