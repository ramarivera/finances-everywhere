import { Category } from '@dacs-fe/types';
import { createEventCreatorFactory, errorProps, withEventPayload } from '@dacs-fe/utils/state';
import { createAction } from '@ngrx/store';

const eventCreatorFactory = createEventCreatorFactory('categories');

export const fetchCategories = createAction(
  '[Categories/API] fetch categories',
  eventCreatorFactory()
);

export const fetchCategoriesSuccess = createAction(
  '[Categories/API] fetch categories success',
  withEventPayload<{ categories: Category[] }>()
);

export const fetchCategoriesFailure = createAction(
  '[Categories/API] fetch categories failure',
  errorProps()
);
