import { categoryDefinitionsQuery } from '@dacs-fe/shared/data-access/category-definitions';
import { NamedCategory } from '@dacs-fe/types';
import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, CATEGORIES_FEATURE_KEY, CategoriesState } from './categories.reducer';
// Lookup the 'Categories' feature state managed by NgRx
const getCategoriesState = createFeatureSelector<CategoriesState>(CATEGORIES_FEATURE_KEY);

const {
  selectAll: getAllCategories,
  selectIds: getCategoriesIds,
  selectEntities: getCategoriesEntities,
} = adapter.getSelectors(getCategoriesState);

const callStateSelectors = getCallStateSelectors(getCategoriesState);

const getHasLoaded = createSelector(getCategoriesState, (state) => state.loaded);

const getCategoriesWithName = createSelector(
  categoryDefinitionsQuery.getCategoryDefinitionsEntities,
  categoryDefinitionsQuery.getHasLoaded,
  getAllCategories,
  (categoryDefinitions, categoryDefsHaveLoaded, categories) => {
    if (!categoryDefsHaveLoaded) return [];

    // this is a baaad idea....
    return categories.map((category) => {
      return {
        ...category,
        categoryName: categoryDefinitions[category.categoryDefinitionId]?.name,
      } as NamedCategory;
    });
  }
);

export const categoriesQuery = {
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllCategories,
  getCategoriesEntities,
  getHasLoaded,
  getCategoriesWithName,
};
