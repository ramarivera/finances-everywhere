import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromReporting from './reporting.reducer';
import { reportingQuery } from './reporting.selectors';

@Injectable()
export class ReportingFacade {
  loaded$ = this.store.pipe(select(reportingQuery.getHasLoaded));
  allReports$ = this.store.pipe(select(reportingQuery.getAllReports));
  selectedReport$ = this.store.pipe(select(reportingQuery.getSelectedReport));

  constructor(private store: Store<fromReporting.ReportingPartialState>) {}
}
