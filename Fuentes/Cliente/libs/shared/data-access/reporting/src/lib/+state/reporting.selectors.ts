import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { memoize } from 'lodash';
import {
  REPORTING_FEATURE_KEY,
  reportingAdapter,
  ReportingState,
  ReportEntity,
} from './reporting.reducer';

const getReportingState = createFeatureSelector<ReportingState>(REPORTING_FEATURE_KEY);

const {
  selectAll: getAllReports,
  selectIds: getReportIds,
  selectEntities: getReportEntities,
} = reportingAdapter.getSelectors(getReportingState);

const callStateSelectors = getCallStateSelectors(getReportingState);

const getHasLoaded = createSelector(
  getReportingState,
  state => state.loaded
);

const getSelectedId = createSelector(
  getReportingState,
  state => state.selectedId
);

const getSelectedReport = createSelector(
  getReportEntities,
  getSelectedId,
  (reports, id) => {
    return id ? reports[id] : undefined;
  }
);

const getReportDescription = memoize(
  (report: ReportEntity) => {
    return `Filter ${report.eventId}`;
  },
  (report: ReportEntity) => report.eventId
);

const getAllFilters = createSelector(
  getAllReports,
  reports => reports.map(x => ({ eventId: x.eventId, ...x.filter }))
);

export const reportingQuery = {
  getSingleLoading: callStateSelectors.isSingleLoading,
  getAllReports,
  getSelectedReport,
  getHasLoaded,
  getAllFilters,
};
