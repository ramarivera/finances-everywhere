import { Injectable } from '@angular/core';
import { Actions, createEffect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { map } from 'rxjs/operators';
import { ReportingService } from '../reporting.service';
import { requestReport, requestReportFailure, requestReportSuccess } from './reporting.actions';
import { ReportingPartialState } from './reporting.reducer';

@Injectable()
export class ReportingEffects {
  requestReport$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof requestReport>>(requestReport, {
      run: ({ filter, eventId }) => {
        return this.reportingService
          .executeReport(filter)
          .pipe(map(report => requestReportSuccess({ eventId, report, filter })));
      },
      onError: (action, error) => {
        return requestReportFailure({ eventId: action.eventId, error });
      },
    })
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<ReportingPartialState>,
    private reportingService: ReportingService
  ) {}
}
