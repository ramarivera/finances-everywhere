import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { TransactionReport, TransactionReportFilter } from '@dacs-fe/types';
import { BaseApiUrl } from '@dacs-fe/utils/environment';

@Injectable({ providedIn: 'root' })
export class ReportingService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'reports';
  }

  executeReport(filter: TransactionReportFilter) {
    return this.http.post<TransactionReport>(this.baseApiUrl, filter);
  }
}
