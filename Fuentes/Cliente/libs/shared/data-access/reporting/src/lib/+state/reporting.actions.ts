import { TransactionReport, TransactionReportFilter } from '@dacs-fe/types';
import { createEventCreatorFactory, errorProps, withEventPayload } from '@dacs-fe/utils/state';
import { createAction, props } from '@ngrx/store';

const eventCreatorFactory = createEventCreatorFactory('department');

export const requestReport = createAction(
  '[Reporting/API] fetch report',
  eventCreatorFactory(props<{ filter: TransactionReportFilter }>())
);

export const requestReportSuccess = createAction(
  '[Reporting/API] fetch report success',
  withEventPayload<{ report: TransactionReport; filter: TransactionReportFilter }>()
);

export const requestReportFailure = createAction(
  '[Reporting/API] fetch report failure',
  errorProps()
);
