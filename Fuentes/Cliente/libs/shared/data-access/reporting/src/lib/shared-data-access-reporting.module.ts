import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromReporting from './+state/reporting.reducer';
import { ReportingEffects } from './+state/reporting.effects';
import { ReportingFacade } from './+state/reporting.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromReporting.REPORTING_FEATURE_KEY, fromReporting.reducer),
    EffectsModule.forFeature([ReportingEffects]),
  ],
  providers: [ReportingFacade],
})
export class SharedDataAccessReportingModule {}
