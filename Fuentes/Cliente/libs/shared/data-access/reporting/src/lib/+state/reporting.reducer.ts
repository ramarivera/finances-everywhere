import { TransactionReport, TransactionReportFilter } from '@dacs-fe/types';
import { CallState, callStateReducer, initialCallState } from '@dacs-fe/utils/state';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import { requestReport, requestReportFailure, requestReportSuccess } from './reporting.actions';

export const REPORTING_FEATURE_KEY = 'reporting';

export interface ReportEntity extends TransactionReport {
  eventId: string;
  filter: TransactionReportFilter;
}

export interface ReportingState extends EntityState<ReportEntity>, CallState {
  selectedId?: string | number;
  loaded: boolean;
  error?: string | null;
}

export interface ReportingPartialState {
  readonly [REPORTING_FEATURE_KEY]: ReportingState;
}

export const reportingAdapter: EntityAdapter<ReportEntity> = createEntityAdapter<ReportEntity>({
  selectId: report => report.eventId,
});

export const initialState: ReportingState = reportingAdapter.getInitialState({
  ...initialCallState,
  loaded: false,
});

export const reducer = createReducer(
  initialState,

  on(requestReportSuccess, (state, { report, eventId, filter }) =>
    reportingAdapter.addOne({ ...report, eventId, filter }, state)
  )

  // on(selectReport, (state, { reportEventId }) => ({
  //   ...state,
  //   selectedId: reportEventId,
  // }))
);

export const reportingCallStateTriggers = {
  single: {
    loading: [requestReport].map(x => x.type),
    resting: [requestReportSuccess].map(x => x.type),
    erroring: [requestReportFailure].map(x => x.type),
  },
};

export function reportingReducer(
  state: ReportingState = initialState,
  action: Action
): ReportingState {
  return callStateReducer(reducer, reportingCallStateTriggers)(state, action);
}
