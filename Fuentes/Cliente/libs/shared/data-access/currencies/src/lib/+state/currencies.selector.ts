import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  CURRENCIES_FEATURE_KEY,
  CurrenciesState,
} from './currencies.reducer';

const getCurrenciesState = createFeatureSelector<CurrenciesState>(CURRENCIES_FEATURE_KEY);

const callStateSelectors = getCallStateSelectors(getCurrenciesState);

const getHasLoaded = createSelector(
  getCurrenciesState,
  state => state.loaded
);

const getAllCurrencies = createSelector(
  getCurrenciesState,
  state => state.currencies
);

export const currenciesQuery = {
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllCurrencies,
  getHasLoaded,
};
