import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CurrencyService } from './currency.service';
import { CurrenciesFacade } from './+state/currencies.facade';
import { CURRENCIES_FEATURE_KEY, currenciesReducer } from './+state/currencies.reducer';
import { CurrenciesEffects } from './+state/currencies.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(CURRENCIES_FEATURE_KEY, currenciesReducer),
    EffectsModule.forFeature([CurrenciesEffects]),
  ],
  providers: [CurrencyService, CurrenciesFacade],
})
export class SharedDataAccessCurrenciesModule {}
