import { Currency } from '@dacs-fe/types';
import { createEventCreatorFactory, errorProps, withEventPayload } from '@dacs-fe/utils/state';
import { createAction } from '@ngrx/store';

const eventCreatorFactory = createEventCreatorFactory('currencies_');

export const fetchCurrencies = createAction(
  '[Currencies/API] fetch currencies',
  eventCreatorFactory()
);

export const fetchCurrenciesSuccess = createAction(
  '[Currencies/API] fetch currencies success',
  withEventPayload<{ currencies: Currency[] }>()
);

export const fetchCurrenciesFailure = createAction(
  '[Currencies/API] fetch currencies failure',
  errorProps()
);
