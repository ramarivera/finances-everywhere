import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Currency } from '@dacs-fe/types';
import { BaseApiUrl } from '@dacs-fe/utils/environment';

@Injectable()
export class CurrencyService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'currencies';
  }

  getCurrencies() {
    return this.http.get<Currency[]>(this.baseApiUrl);
  }

  getDefaultCurrency() {
    return this.http.get<Currency>(`${this.baseApiUrl}/default`);
  }
}
