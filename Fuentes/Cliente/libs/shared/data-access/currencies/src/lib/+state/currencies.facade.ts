import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { CurrenciesPartialState } from './currencies.reducer';
import { currenciesQuery } from './currencies.selector';

@Injectable()
export class CurrenciesFacade {
  hasLoaded$ = this.store.pipe(select(currenciesQuery.getHasLoaded));
  batchLoading$ = this.store.pipe(select(currenciesQuery.getBatchLoading));
  allCurrencies$ = this.store.pipe(select(currenciesQuery.getAllCurrencies));

  constructor(private store: Store<CurrenciesPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
