import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map, tap } from 'rxjs/operators';
import { CurrencyService } from '../currency.service';
import { CurrenciesPartialState } from './currencies.reducer';
import {
  fetchCurrencies,
  fetchCurrenciesSuccess,
  fetchCurrenciesFailure,
} from './currencies.actions';

@Injectable()
export class CurrenciesEffects {
  loadCurrencies$ = createEffect(() =>
    this.dataPersistence.fetch<ReturnType<typeof fetchCurrencies>>(fetchCurrencies, {
      run: action =>
        this.currencyService.getCurrencies().pipe(
          delay(3500),
          map(currencies => fetchCurrenciesSuccess({ currencies, eventId: action.eventId }))
        ),
      onError: (action, error) => fetchCurrenciesFailure({ error, eventId: action.eventId }),
    })
  );

  errorsEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fetchCurrenciesFailure),
        tap(_ => {
          this.snackBarService.openSnackBar('alert', 'An error has ocurred');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private snackBarService: SnackBarService,
    private dataPersistence: DataPersistence<CurrenciesPartialState>,
    private currencyService: CurrencyService
  ) {}
}
