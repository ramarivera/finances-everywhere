import { Currency } from '@dacs-fe/types';
import { Action, createReducer, on } from '@ngrx/store';
import * as currencyActions from './currencies.actions';
import {
  CallState,
  callStateReducer,
  initialCallState,
} from '@dacs-fe/utils/state';

export const CURRENCIES_FEATURE_KEY = 'currencies';

export interface CurrenciesState extends CallState {
  loaded: boolean;
  currencies: Currency[];
}

export interface CurrenciesPartialState {
  readonly [CURRENCIES_FEATURE_KEY]: CurrenciesState;
}

export const initialState: CurrenciesState = {
  ...initialCallState,
  currencies: [],
  loaded: false,
};

export const reducer = createReducer(
  initialState,

  on(currencyActions.fetchCurrenciesSuccess, (state, { currencies }) => ({
    ...state,
    currencies,
  }))
);

export const currenciesCallStateTriggers = {
  batch: {
    loading: [currencyActions.fetchCurrencies.type],
    resting: [currencyActions.fetchCurrenciesSuccess.type],
    erroring: [currencyActions.fetchCurrenciesFailure.type],
  },
};

export function currenciesReducer(
  state: CurrenciesState = initialState,
  action: Action
): CurrenciesState {
  return callStateReducer(reducer, currenciesCallStateTriggers)(state, action);
}
