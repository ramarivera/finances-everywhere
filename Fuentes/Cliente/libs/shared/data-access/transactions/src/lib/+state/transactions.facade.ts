import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { TransactionsPartialState } from './transactions.reducer';
import { transactionsQuery } from './transactions.selectors';

@Injectable()
export class TransactionsFacade {
  hasLoaded$ = this.store.pipe(select(transactionsQuery.getHasLoaded));
  batchLoading$ = this.store.pipe(select(transactionsQuery.getBatchLoading));
  singleLoading$ = this.store.pipe(select(transactionsQuery.getSingleLoading));
  allTransactions$ = this.store.pipe(select(transactionsQuery.getAllTransactions));
  selectedTransaction$ = this.store.pipe(select(transactionsQuery.getSelectedAccount));

  constructor(protected store: Store<TransactionsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  private isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
