import { createEventCreatorFactory, errorProps, withEventPayload } from '@dacs-fe/utils/state';
import { createAction, props } from '@ngrx/store';
import {
  Transaction,
  TransactionCreationData,
  TransactionDetails,
  TransactionUpdateData,
} from '@dacs-fe/types';

const eventCreatorFactory = createEventCreatorFactory('transaction');

export const fetchTransaction = createAction(
  '[Transactions/API] fetch transaction start',
  eventCreatorFactory(props<{ transactionId: number }>())
);

export const fetchTransactionSuccess = createAction(
  '[Transactions/API] fetch transaction success',
  withEventPayload<{ transaction: TransactionDetails }>()
);

export const fetchTransactionFailure = createAction(
  '[Transactions/API] fetch transaction failure',
  errorProps()
);

export const fetchTransactions = createAction(
  '[Transactions/API] fetch transactions',
  eventCreatorFactory()
);

export const fetchTransactionsSuccess = createAction(
  '[Transactions/API] fetch transactions success',
  withEventPayload<{ transactions: Transaction[] }>()
);

export const fetchTransactionsFailure = createAction(
  '[Transactions/API] fetch transactions failure',
  errorProps()
);

export const createTransaction = createAction(
  '[Transactions/API] create transaction',
  eventCreatorFactory(props<{ creationData: TransactionCreationData }>())
);

export const createTransactionSuccess = createAction(
  '[Transactions/API] create transaction success',
  withEventPayload<{ transaction: TransactionDetails }>()
);

export const createTransactionFailure = createAction(
  '[Transactions/API] create transaction failure',
  errorProps()
);

export const deleteTransaction = createAction(
  '[Transactions/API] delete transaction',
  eventCreatorFactory(props<{ transactionId: number }>())
);

export const deleteTransactionSuccess = createAction(
  '[Transactions/API] delete transaction success',
  withEventPayload<{ transactionId: number }>()
);

export const deleteTransactionFailure = createAction(
  '[Transactions/API] delete transaction failure',
  errorProps()
);

export const updateTransaction = createAction(
  '[Transactions/API] update transaction',
  eventCreatorFactory(props<{ transactionId: number; updateData: TransactionUpdateData }>())
);

export const updateTransactionSuccess = createAction(
  '[Transactions/API] update transaction success',
  (transaction: TransactionDetails, eventId: string) => ({
    id: transaction.transactionId,
    changes: transaction,
    eventId,
  })
);

export const updateTransactionFailure = createAction(
  '[Transactions/API] update transaction failure',
  errorProps()
);

export const selectTransaction = createAction(
  '[Transactions] select transaction',
  props<{ transactionId: number }>()
);
