import { Transaction, TransactionDetails } from '@dacs-fe/types';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as transactionActions from './transactions.actions';
import {
  CallState,
  callStateReducer,
  DacsEntity,
  initialCallState,
  markAsFull,
} from '@dacs-fe/utils/state';

export type TransactionEntity = DacsEntity<Transaction, TransactionDetails>;

export const TRANSACTIONS_FEATURE_KEY = 'Transactions';

export const adapter = createEntityAdapter<TransactionEntity>({
  selectId: transaction => transaction.transactionId,
  sortComparer: (first, second) => first.title.localeCompare(second.title),
});

export interface TransactionsState extends EntityState<TransactionEntity>, CallState {
  selectedId?: number;
  loaded: boolean;
}

export interface TransactionsPartialState {
  readonly [TRANSACTIONS_FEATURE_KEY]: TransactionsState;
}

export const initialState: TransactionsState = adapter.getInitialState({
  ...initialCallState,
  loaded: false,
});

export const reducer = createReducer(
  initialState,

  on(transactionActions.createTransactionSuccess, (state, { transaction, eventId }) =>
    adapter.addOne(markAsFull(transaction, eventId), state)
  ),

  on(transactionActions.updateTransactionSuccess, (state, update) =>
    adapter.updateOne({ id: update.id, changes: markAsFull(update.changes, update.eventId) }, state)
  ),

  on(transactionActions.deleteTransactionSuccess, (state, { transactionId }) =>
    adapter.removeOne(transactionId, state)
  ),

  on(transactionActions.fetchTransactionsSuccess, (state, { transactions }) =>
    adapter.addAll(transactions, { ...state, loaded: true })
  ),

  on(transactionActions.fetchTransactionSuccess, (state, { transaction }) =>
    adapter.upsertOne(markAsFull(transaction), state)
  ),

  on(transactionActions.selectTransaction, (state, { transactionId }) => ({
    ...state,
    selectedId: transactionId,
  }))
);

export const TransactionsCallStateTriggers = {
  single: {
    loading: [
      transactionActions.fetchTransaction,
      transactionActions.updateTransaction,
      transactionActions.deleteTransaction,
      transactionActions.createTransaction,
    ].map(x => x.type),
    resting: [
      transactionActions.fetchTransactionSuccess,
      transactionActions.updateTransactionSuccess,
      transactionActions.deleteTransactionSuccess,
      transactionActions.createTransactionSuccess,
    ].map(x => x.type),
    erroring: [
      transactionActions.fetchTransactionFailure,
      transactionActions.updateTransactionFailure,
      transactionActions.deleteTransactionFailure,
      transactionActions.createTransactionFailure,
    ].map(x => x.type),
  },
  batch: {
    loading: [transactionActions.fetchTransactions.type],
    resting: [transactionActions.fetchTransactionsSuccess.type],
    erroring: [transactionActions.fetchTransactionsFailure.type],
  },
};

export function TransactionsReducer(
  state: TransactionsState = initialState,
  action: Action
): TransactionsState {
  return callStateReducer(reducer, TransactionsCallStateTriggers)(state, action);
}
