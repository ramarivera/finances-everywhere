import { TransactionDetails } from '@dacs-fe/types';
import { getCallStateSelectors, isDetailsEntity } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { adapter, TRANSACTIONS_FEATURE_KEY, TransactionsState } from './transactions.reducer';

// Lookup the 'Transactions' feature state managed by NgRx
const getTransactionsState = createFeatureSelector<TransactionsState>(TRANSACTIONS_FEATURE_KEY);

const {
  selectAll: getAllTransactions,
  selectIds: getTransactionsIds,
  selectEntities: getTransactionsEntities,
} = adapter.getSelectors(getTransactionsState);

const callStateSelectors = getCallStateSelectors(getTransactionsState);

const getHasLoaded = createSelector(getTransactionsState, (state) => state.loaded);

const getSelectedId = createSelector(getTransactionsState, (state) => state.selectedId);

const getSelectedTransaction = createSelector(
  getTransactionsEntities,
  getSelectedId,
  (transactions, selectedId) => {
    if (!selectedId) return;
    return isDetailsEntity(transactions[selectedId])
      ? (transactions[selectedId] as TransactionDetails)
      : undefined;
  }
);

export const transactionsQuery = {
  getSingleLoading: callStateSelectors.isSingleLoading,
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllTransactions,
  getTransactionsEntities,
  getSelectedAccount: getSelectedTransaction,
  getHasLoaded,
};
