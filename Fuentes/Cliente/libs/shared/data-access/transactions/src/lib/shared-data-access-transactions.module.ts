import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TransactionsService } from './transactions.service';
import { TRANSACTIONS_FEATURE_KEY, TransactionsReducer } from './+state/transactions.reducer';
import { TransactionsEffects } from './+state/transactions.effects';
import { TransactionsFacade } from './+state/transactions.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(TRANSACTIONS_FEATURE_KEY, TransactionsReducer),
    EffectsModule.forFeature([TransactionsEffects]),
  ],
  providers: [TransactionsFacade, TransactionsService],
})
export class SharedDataAccessTransactionsModule {}
