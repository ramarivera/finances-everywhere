import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BaseApiUrl } from '@dacs-fe/utils/environment';
import {
  Transaction,
  TransactionCreationData,
  TransactionUpdateData,
  TransactionDetails,
} from '@dacs-fe/types';

@Injectable()
export class TransactionsService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'transactions';
  }

  getTransactions() {
    return this.http.get<Transaction[]>(this.baseApiUrl);
  }

  getTransaction(transactionId: number) {
    return this.http.get<TransactionDetails>(`${this.baseApiUrl}/${transactionId}`);
  }

  createTransaction(creationData: TransactionCreationData) {
    return this.http.post<TransactionDetails>(this.baseApiUrl, creationData);
  }

  deleteTransaction(transactionId: number) {
    return this.http.delete<void>(`${this.baseApiUrl}/${transactionId}`);
  }

  updateTransaction(transactionId: number, categoryToUpdate: TransactionUpdateData) {
    return this.http.put<TransactionDetails>(
      `${this.baseApiUrl}/${transactionId}`,
      categoryToUpdate
    );
  }
}
