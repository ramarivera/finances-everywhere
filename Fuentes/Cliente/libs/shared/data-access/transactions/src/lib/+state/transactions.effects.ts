import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map, tap } from 'rxjs/operators';
import { TransactionsService } from '../transactions.service';
import { TransactionsPartialState } from './transactions.reducer';
import {
  fetchTransaction,
  fetchTransactionSuccess,
  fetchTransactionFailure,
  fetchTransactions,
  fetchTransactionsSuccess,
  fetchTransactionsFailure,
  selectTransaction,
  createTransaction,
  createTransactionSuccess,
  createTransactionFailure,
  updateTransaction,
  updateTransactionSuccess,
  updateTransactionFailure,
  deleteTransaction,
  deleteTransactionSuccess,
  deleteTransactionFailure,
} from './transactions.actions';

@Injectable()
export class TransactionsEffects {
  constructor(
    private actions$: Actions,
    private snackBarService: SnackBarService,
    private dataPersistence: DataPersistence<TransactionsPartialState>,
    private transactionsService: TransactionsService
  ) {}
  /**
   * Fetches the latest transaction information on selection
   */
  @Effect()
  selectTransaction$ = this.dataPersistence.fetch<ReturnType<typeof selectTransaction>>(
    selectTransaction,
    {
      run: ({ transactionId }) => fetchTransaction({ transactionId }),
    }
  );

  /**
   * Fetches transactions
   */
  @Effect()
  loadTransactions$ = this.dataPersistence.fetch<ReturnType<typeof fetchTransactions>>(
    fetchTransactions.type,
    {
      run: (action) => {
        return this.transactionsService.getTransactions().pipe(
          delay(3500),
          map((transactions) => fetchTransactionsSuccess({ transactions, eventId: action.eventId }))
        );
      },
      onError: (action, error) => {
        return fetchTransactionsFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  loadTransaction$ = this.dataPersistence.fetch<ReturnType<typeof fetchTransaction>>(
    fetchTransaction.type,
    {
      run: (action) => {
        return this.transactionsService
          .getTransaction(action.transactionId)
          .pipe(
            map((transaction) => fetchTransactionSuccess({ transaction, eventId: action.eventId }))
          );
      },
      onError: (action, error) => {
        return fetchTransactionFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  createTransactionEffect$ = this.dataPersistence.fetch<ReturnType<typeof createTransaction>>(
    createTransaction.type,
    {
      run: (action) => {
        return this.transactionsService
          .createTransaction(action.creationData)
          .pipe(
            map((transaction) => createTransactionSuccess({ transaction, eventId: action.eventId }))
          );
      },

      onError: (action, error) => {
        return createTransactionFailure({ eventId: action.eventId, error });
      },
    }
  );

  @Effect()
  updateTransactionEffect$ = this.dataPersistence.fetch<ReturnType<typeof updateTransaction>>(
    updateTransaction.type,
    {
      run: ({ transactionId, updateData, eventId }) => {
        return this.transactionsService
          .updateTransaction(transactionId, updateData)
          .pipe(map((transaction) => updateTransactionSuccess(transaction, eventId)));
      },

      onError: ({ eventId }, error) => {
        return updateTransactionFailure({ eventId, error });
      },
    }
  );

  @Effect()
  deleteTransactionEffect$ = this.dataPersistence.fetch<ReturnType<typeof deleteTransaction>>(
    deleteTransaction.type,
    {
      run: ({ transactionId, eventId }) => {
        return this.transactionsService.deleteTransaction(transactionId).pipe(
          map((_) =>
            deleteTransactionSuccess({
              transactionId,
              eventId,
            })
          )
        );
      },
      onError: ({ eventId }, error) => {
        return deleteTransactionFailure({ eventId, error });
      },
    }
  );

  errorsEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          deleteTransactionFailure,
          createTransactionFailure,
          updateTransactionFailure,
          fetchTransactionFailure,
          fetchTransactionsFailure
        ),
        tap((_) => {
          this.snackBarService.openSnackBar('alert', 'An error has ocurred');
        })
      ),
    { dispatch: false }
  );

  // myFetch<A extends ((...args: any[]) => R) & TypedAction<T>, T extends string, R extends object>(
  //   actionType: A,
  //   opts: FetchOpts<any, R & TypedAction<T>>
  // ) {
  //   return this.dataPersistence.fetch(actionType, opts);
  // }

  // fetch<A extends Action = Action>(actionType: string | ActionCreator, opts: FetchOpts<T, A>): Observable<any>;

  //   type ActionCreator<T extends string = string, C extends Creator = Creator> = C & TypedAction<T>;

  //   interface FetchOpts<T, A> {
  //     id?(a: A, state?: T): any;
  //     run(a: A, state?: T): Observable<Action> | Action | void;
  //     onError?(a: A, e: any): Observable<any> | any;
  // }
}
