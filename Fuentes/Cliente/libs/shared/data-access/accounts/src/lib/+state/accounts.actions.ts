import { Account, AccountCreationData, AccountDetails, AccountUpdateData } from '@dacs-fe/types';
import { createAction, props } from '@ngrx/store';
import { uniqueId } from 'lodash';

const errorProps = props<{ error?: any; eventId?: string }>();

const event = () => () => {
  return {
    eventId: generateId(),
  };
};

const eventPayload = <TIn>() => (payload: TIn) => {
  return {
    ...payload,
    eventId: generateId(),
  };
};

const eventProps = <TIn>() => props<TIn & { eventId: string }>();

const generateId = () => uniqueId('account_');

export const fetchAccount = createAction('[Account/API] fetch account', eventPayload<{ accountId: number }>());

export const fetchAccountSuccess = createAction(
  '[Account/API] fetch account success',
  eventProps<{ account: AccountDetails }>()
);

export const fetchAccountFailure = createAction('[Account/API] fetch account failure', errorProps);

export const fetchAccounts = createAction('[Account/API] fetch accounts', event());

export const fetchAccountsSuccess = createAction(
  '[Account/API] fetch accounts success',
  eventProps<{ accounts: Account[] }>()
);

export const fetchAccountsFailure = createAction('[Account/API] fetch accounts failure', errorProps);

export const createAccount = createAction(
  '[Account/API] create account',
  eventPayload<{ creationData: AccountCreationData }>()
);

export const createAccountSuccess = createAction(
  '[Account/API] create account success',
  eventProps<{ account: AccountDetails }>()
);

export const createAccountFailure = createAction('[Account/API] create account failure', errorProps);

export const deleteAccount = createAction('[Account/API] delete account', eventPayload<{ accountId: number }>());

export const deleteAccountSuccess = createAction(
  '[Account/API] delete account success',
  eventProps<{ accountId: number }>()
);

export const deleteAccountFailure = createAction('[Account/API] delete account failure', errorProps);

export const updateAccount = createAction(
  '[Account/API] update account',
  eventPayload<{ accountId: number; updateData: AccountUpdateData }>()
);

export const updateAccountSuccess = createAction(
  '[Account/API] update account success',
  (account: AccountDetails, eventId: string) => ({
    id: account.accountId,
    changes: account,
    eventId,
  })
);

export const updateAccountFailure = createAction('[Account/API] update account failure', errorProps);

export const selectAccount = createAction('[Account] select account', props<{ accountId: number }>());
