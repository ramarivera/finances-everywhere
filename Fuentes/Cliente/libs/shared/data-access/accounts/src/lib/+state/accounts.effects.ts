import { Injectable } from '@angular/core';
import { SnackBarService } from '@dacs-fe/shared/ui/snackbars';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { delay, map, tap } from 'rxjs/operators';
import { AccountsService } from '../accounts.service';
import { AccountsPartialState } from './accounts.reducer';
import {
  fetchAccount,
  fetchAccountSuccess,
  fetchAccountFailure,
  fetchAccounts,
  fetchAccountsSuccess,
  fetchAccountsFailure,
  selectAccount,
  createAccount,
  createAccountSuccess,
  createAccountFailure,
  updateAccount,
  updateAccountSuccess,
  updateAccountFailure,
  deleteAccount,
  deleteAccountSuccess,
  deleteAccountFailure,
} from './accounts.actions';

@Injectable()
export class AccountsEffects {
  /**
   * Fetches the latest account information on selection
   */
  @Effect()
  selectAccount$ = this.dataPersistence.fetch<ReturnType<typeof selectAccount>>(
    selectAccount.type,
    {
      run: ({ accountId }) => fetchAccount({ accountId }),
    }
  );

  /**
   * Fetches accounts
   */
  @Effect()
  loadAccounts$ = this.dataPersistence.fetch<ReturnType<typeof fetchAccounts>>(
    fetchAccounts.type,
    {
      run: action => {
        return this.accountsService.getAccounts().pipe(
          delay(3500),
          map(accounts => fetchAccountsSuccess({ accounts, eventId: action.eventId }))
        );
      },
      onError: (action, error) => {
        return fetchAccountsFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  loadAccount$ = this.dataPersistence.fetch<ReturnType<typeof fetchAccount>>(
    fetchAccount.type,
    {
      run: action => {
        return this.accountsService
          .getAccount(action.accountId)
          .pipe(
            map(account => fetchAccountSuccess({ account, eventId: action.eventId }))
          );
      },
      onError: (action, error) => {
        return fetchAccountFailure({ error, eventId: action.eventId });
      },
    }
  );

  @Effect()
  createAccountEffect$ = this.dataPersistence.fetch<ReturnType<typeof createAccount>>(
    createAccount.type,
    {
      run: action => {
        return this.accountsService
          .createAccount(action.creationData)
          .pipe(
            map(account => createAccountSuccess({ account, eventId: action.eventId }))
          );
      },

      onError: (action, error) => {
        return createAccountFailure({ eventId: action.eventId, error });
      },
    }
  );

  @Effect()
  updateAccountEffect$ = this.dataPersistence.fetch<ReturnType<typeof updateAccount>>(
    updateAccount.type,
    {
      run: ({ accountId, updateData, eventId }) => {
        return this.accountsService
          .updateAccount(accountId, updateData)
          .pipe(map(account => updateAccountSuccess(account, eventId)));
      },

      onError: ({ eventId }, error) => {
        return updateAccountFailure({ eventId, error });
      },
    }
  );

  @Effect()
  deleteAccountEffect$ = this.dataPersistence.fetch<ReturnType<typeof deleteAccount>>(
    deleteAccount.type,
    {
      run: ({ accountId, eventId }) => {
        return this.accountsService.deleteAccount(accountId).pipe(
          map(_ =>
            deleteAccountSuccess({
              accountId,
              eventId,
            })
          )
        );
      },
      onError: ({ eventId }, error) => {
        return deleteAccountFailure({ eventId, error });
      },
    }
  );

  errorsEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          deleteAccountFailure,
          createAccountFailure,
          updateAccountFailure,
          fetchAccountFailure,
          fetchAccountsFailure
        ),
        tap(_ => {
          this.snackBarService.openSnackBar('alert', 'An error has ocurred');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private snackBarService: SnackBarService,
    private dataPersistence: DataPersistence<AccountsPartialState>,
    private accountsService: AccountsService
  ) {}
}
