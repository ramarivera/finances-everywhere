import { getCallStateSelectors } from '@dacs-fe/utils/state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ACCOUNTS_FEATURE_KEY, AccountsState, adapter } from './accounts.reducer';

// Lookup the 'Accounts' feature state managed by NgRx
const getAccountsState = createFeatureSelector<AccountsState>(ACCOUNTS_FEATURE_KEY);

const {
  selectAll: getAllAccounts,
  selectIds: getAccountsIds,
  selectEntities: getAccountsEntities,
} = adapter.getSelectors(getAccountsState);

const callStateSelectors = getCallStateSelectors(getAccountsState);

const getHasLoaded = createSelector(
  getAccountsState,
  state => state.loaded
);

const getSelectedId = createSelector(
  getAccountsState,
  state => state.selectedId
);

const getSelectedAccount = createSelector(
  getAccountsEntities,
  getSelectedId,
  (accounts, id) => {
    return id ? accounts[id] : undefined;
  }
);

export const accountsQuery = {
  getSingleLoading: callStateSelectors.isSingleLoading,
  getBatchLoading: callStateSelectors.isBatchLoading,
  getAllAccounts,
  getAccountsEntities,
  getSelectedAccount,
  getHasLoaded,
};
