import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { AccountsPartialState } from './accounts.reducer';
import { accountsQuery } from './accounts.selectors';

@Injectable()
export class AccountsFacade {
  hasLoaded$ = this.store.pipe(select(accountsQuery.getHasLoaded));
  batchLoading$ = this.store.pipe(select(accountsQuery.getBatchLoading));
  singleLoading$ = this.store.pipe(select(accountsQuery.getSingleLoading));
  allAccounts$ = this.store.pipe(select(accountsQuery.getAllAccounts));
  selectedAccount$ = this.store.pipe(select(accountsQuery.getSelectedAccount));

  constructor(protected store: Store<AccountsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
    if (this.isEventAction(action)) {
      return action.eventId;
    }
  }

  isEventAction(action: any): action is { eventId: string } {
    return !!(action as { eventId: string }).eventId;
  }
}
