import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BaseApiUrl } from '@dacs-fe/utils/environment';
import {
  Account,
  AccountCreationData,
  AccountUpdateData,
  AccountDetails,
} from '@dacs-fe/types';

@Injectable()
export class AccountsService {
  private baseApiUrl: string;

  constructor(private http: HttpClient, @Inject(BaseApiUrl) baseApiUrl: string) {
    this.baseApiUrl = baseApiUrl + 'accounts';
  }

  getAccounts() {
    return this.http.get<Account[]>(this.baseApiUrl);
  }

  getAccount(AccountId: number) {
    return this.http.get<AccountDetails>(`${this.baseApiUrl}/${AccountId}`);
  }

  createAccount(creationData: AccountCreationData) {
    return this.http.post<AccountDetails>(this.baseApiUrl, creationData);
  }

  deleteAccount(AccountId: number) {
    return this.http.delete<void>(`${this.baseApiUrl}/${AccountId}`);
  }

  updateAccount(AccountId: number, categoryToUpdate: AccountUpdateData) {
    return this.http.put<AccountDetails>(`${this.baseApiUrl}/${AccountId}`, categoryToUpdate);
  }
}
