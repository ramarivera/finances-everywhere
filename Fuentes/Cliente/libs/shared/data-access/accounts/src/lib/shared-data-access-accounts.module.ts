import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AccountsService } from './accounts.service';
import {
  ACCOUNTS_FEATURE_KEY,
  AccountsReducer,
} from './+state/accounts.reducer';
import { AccountsEffects } from './+state/accounts.effects';
import { AccountsFacade } from './+state/accounts.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(ACCOUNTS_FEATURE_KEY, AccountsReducer),
    EffectsModule.forFeature([AccountsEffects]),
  ],
  providers: [AccountsFacade, AccountsService],
})
export class SharedDataAccessAccountsModule {}
