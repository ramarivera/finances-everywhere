import { Account, AccountDetails } from '@dacs-fe/types';
import { CallState, callStateReducer, DacsEntity, initialCallState, markAsFull } from '@dacs-fe/utils/state';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as accountActions from './accounts.actions';

export type AccountEntity = DacsEntity<Account, AccountDetails>;

export const ACCOUNTS_FEATURE_KEY = 'Accounts';

export const adapter = createEntityAdapter<AccountEntity>({
  selectId: account => account.accountId,
  sortComparer: (first, second) => first.name.localeCompare(second.name),
});

export interface AccountsState extends EntityState<AccountEntity>, CallState {
  selectedId?: number;
  loaded: boolean;
}

export interface AccountsPartialState {
  readonly [ACCOUNTS_FEATURE_KEY]: AccountsState;
}

export const initialState: AccountsState = adapter.getInitialState({ ...initialCallState, loaded: false });

export const reducer = createReducer(
  initialState,

  on(accountActions.createAccountSuccess, (state, { account, eventId }) =>
    adapter.addOne(markAsFull(account, eventId), state)
  ),

  on(accountActions.updateAccountSuccess, (state, update) =>
    adapter.updateOne({ id: update.id, changes: markAsFull(update.changes, update.eventId) }, state)
  ),

  on(accountActions.deleteAccountSuccess, (state, { accountId }) =>
    adapter.removeOne(accountId, state)
  ),

  on(accountActions.fetchAccountsSuccess, (state, { accounts }) =>
    adapter.addAll(accounts, { ...state, loaded: true })
  ),

  on(accountActions.fetchAccountSuccess, (state, { account }) =>
    adapter.upsertOne(markAsFull(account), state)
  ),

  on(accountActions.selectAccount, (state, { accountId }) => ({
    ...state,
    selectedId: accountId,
  }))
);

export const AccountsCallStateTriggers = {
  single: {
    loading: [
      accountActions.fetchAccount,
      accountActions.updateAccount,
      accountActions.deleteAccount,
      accountActions.createAccount,
    ].map(x => x.type),
    resting: [
      accountActions.fetchAccountSuccess,
      accountActions.updateAccountSuccess,
      accountActions.deleteAccountSuccess,
      accountActions.createAccountSuccess,
    ].map(x => x.type),
    erroring: [
      accountActions.fetchAccountFailure,
      accountActions.updateAccountFailure,
      accountActions.deleteAccountFailure,
      accountActions.createAccountFailure,
    ].map(x => x.type),
  },
  batch: {
    loading: [accountActions.fetchAccounts.type],
    resting: [accountActions.fetchAccountsSuccess.type],
    erroring: [accountActions.fetchAccountsFailure.type],
  },
};

export function AccountsReducer(
  state: AccountsState = initialState,
  action: Action
): AccountsState {
  return callStateReducer(reducer, AccountsCallStateTriggers)(state, action);
}
