/**
 * Interface for the 'Attachments' data
 */
export interface AttachmentsEntity {
  id: string | number; // Primary ID
}
