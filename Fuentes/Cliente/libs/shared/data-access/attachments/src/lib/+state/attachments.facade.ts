import { Injectable } from '@angular/core';

import { select, Store, Action } from '@ngrx/store';

import * as fromAttachments from './attachments.reducer';
import * as AttachmentsSelectors from './attachments.selectors';

@Injectable()
export class AttachmentsFacade {
  loaded$ = this.store.pipe(select(AttachmentsSelectors.getAttachmentsLoaded));
  allAttachments$ = this.store.pipe(select(AttachmentsSelectors.getAllAttachments));
  selectedAttachments$ = this.store.pipe(select(AttachmentsSelectors.getSelected));

  constructor(private store: Store<fromAttachments.AttachmentsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
