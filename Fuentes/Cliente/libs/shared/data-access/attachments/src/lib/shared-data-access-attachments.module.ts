import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromAttachments from './+state/attachments.reducer';
import { AttachmentsEffects } from './+state/attachments.effects';
import { AttachmentsFacade } from './+state/attachments.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAttachments.ATTACHMENTS_FEATURE_KEY, fromAttachments.reducer),
    EffectsModule.forFeature([AttachmentsEffects]),
  ],
  providers: [AttachmentsFacade],
})
export class SharedDataAccessAttachmentsModule {}
