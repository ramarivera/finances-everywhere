import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import * as fromAttachments from './attachments.reducer';

@Injectable()
export class AttachmentsEffects {
  // /** When an image is selected by URL (and confirmed) in the attachment wizard, start fetching it. */
  // fetchImageByUrl$ = createEffect(() =>
  //   this.dataPersistence.fetch<ReturnType<typeof AttachmentsActions.selectImageUrl>>(
  //     AttachmentsActions.selectImageUrl,
  //     {
  //       run: ({ imageUrl }) => AttachmentsActions.fetchImage({ imageUrl }),
  //     }
  //   )
  // );

  // fetchImage$ = createEffect(() =>
  //   this.dataPersistence.fetch<ReturnType<typeof AttachmentsActions.fetchImage>>(
  //     AttachmentsActions.fetchImage,
  //     {
  //       run: ({ imageUrl }) => AttachmentsActions.fetchImage({ imageUrl }),
  //     }
  //   )
  // );

  // //   categoryDefinitionsUpdateLoadEffect$ = createEffect(() =>
  // //   this.dataPersistence.fetch<ReturnType<typeof loadCategoryDefinitionForUpdate>>(
  // //     loadCategoryDefinitionForUpdate,
  // //     {
  // //       run: ({ categoryDefinitionId }) => selectCategoryDefinition({ categoryDefinitionId }),
  // //     }
  // //   )
  // // );

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private dataPersistence: DataPersistence<fromAttachments.AttachmentsPartialState>
  ) {}
}
