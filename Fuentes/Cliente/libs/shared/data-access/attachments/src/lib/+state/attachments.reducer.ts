import { PictureSourceTypes } from '@dacs-fe/types';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as AttachmentsActions from './attachments.actions';
import { AttachmentsEntity } from './attachments.models';

export const ATTACHMENTS_FEATURE_KEY = 'attachments';

export interface State extends EntityState<AttachmentsEntity> {
  selectedId?: string | number; // which Attachments record has been selected
  loaded: boolean; // has the Attachments list been loaded
  error?: string | null; // last none error (if any)
  selectedImageSource: PictureSourceTypes;
  selectedImageBase64?: string;
}

export interface AttachmentsPartialState {
  readonly [ATTACHMENTS_FEATURE_KEY]: State;
}

export const attachmentsAdapter: EntityAdapter<AttachmentsEntity> = createEntityAdapter<
  AttachmentsEntity
>();

export const initialState: State = attachmentsAdapter.getInitialState({
  // set initial required properties
  loaded: false,
  selectedImageSource: PictureSourceTypes.None,
});

const attachmentsReducer = createReducer(
  initialState,
  on(AttachmentsActions.loadAttachments, (state) => ({ ...state, loaded: false, error: null })),
  on(AttachmentsActions.loadAttachmentsSuccess, (state, { attachments }) =>
    attachmentsAdapter.addAll(attachments, { ...state, loaded: true })
  ),
  on(AttachmentsActions.loadAttachmentsFailure, (state, { error }) => ({ ...state, error })),
  on(AttachmentsActions.selectImageFromCamera, (state, { base64Image }) => ({
    ...state,
    selectedImageSource: PictureSourceTypes.Photo,
    selectedImageBase64: base64Image,
  })),
  on(AttachmentsActions.selectImageFromUpload, (state, { base64Image }) => ({
    ...state,
    selectedImageSource: PictureSourceTypes.Upload,
    selectedImageBase64: base64Image,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return attachmentsReducer(state, action);
}
