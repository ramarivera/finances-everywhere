import { createAction, props } from '@ngrx/store';
import { AttachmentsEntity } from './attachments.models';

export const loadAttachments = createAction('[Attachments] Load Attachments');

export const loadAttachmentsSuccess = createAction(
  '[Attachments] Load Attachments Success',
  props<{ attachments: AttachmentsEntity[] }>()
);

export const loadAttachmentsFailure = createAction(
  '[Attachments] Load Attachments Failure',
  props<{ error: any }>()
);

export const selectImageUrl = createAction(
  '[Attachments/Images] Select Image by URL',
  props<{ imageUrl: string }>()
);

export const selectImageFromUpload = createAction(
  '[Attachments/Images] Select Image from uploading',
  props<{ base64Image: string }>()
);

export const selectImageFromCamera = createAction(
  '[Attachments/Images] Select Image from camera',
  props<{ base64Image: string }>()
);

export const fetchImage = createAction('[Attachments] Fetch image', props<{ imageUrl: string }>());

export const fetchImageSuccess = createAction(
  '[Attachments/Images] Fetch image success',
  props<{ base64Image: string }>()
);

export const fetchImageFailure = createAction(
  '[Attachments/Images] Fetch image Failure',
  props<{ error: any }>()
);
