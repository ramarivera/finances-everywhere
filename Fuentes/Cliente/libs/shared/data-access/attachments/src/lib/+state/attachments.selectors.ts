import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  ATTACHMENTS_FEATURE_KEY,
  State,
  AttachmentsPartialState,
  attachmentsAdapter,
} from './attachments.reducer';

// Lookup the 'Attachments' feature state managed by NgRx
export const getAttachmentsState = createFeatureSelector<AttachmentsPartialState, State>(
  ATTACHMENTS_FEATURE_KEY
);

const { selectAll, selectEntities } = attachmentsAdapter.getSelectors();

export const getAttachmentsLoaded = createSelector(
  getAttachmentsState,
  (state: State) => state.loaded
);

export const getAttachmentsError = createSelector(
  getAttachmentsState,
  (state: State) => state.error
);

export const getAllAttachments = createSelector(getAttachmentsState, (state: State) =>
  selectAll(state)
);

export const getAttachmentsEntities = createSelector(getAttachmentsState, (state: State) =>
  selectEntities(state)
);

export const getSelectedId = createSelector(
  getAttachmentsState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getAttachmentsEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);
