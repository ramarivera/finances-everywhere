import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { Error404Component } from './error-404/error-404.component';

const errorRoutes: Routes = [{ path: '', component: Error404Component }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(errorRoutes), SharedUiMaterialModule],
  declarations: [Error404Component],
  exports: [Error404Component],
})
export class SharedErrorsModule {}
