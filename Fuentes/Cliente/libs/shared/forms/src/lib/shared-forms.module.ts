import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { prefixAndSuffixExtension } from './formly-extensions';
import { PrefixAndSuffixWrapper } from './wrappers/prefix-and-suffix.wrapper';

@NgModule({
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      wrappers: [
        {
          name: 'dacs-prefix-suffix',
          component: PrefixAndSuffixWrapper,
        },
      ],
      extensions: [{ name: 'dacs-prefix-suffix', extension: { onPopulate: prefixAndSuffixExtension } }],
    }),
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
  ],
  exports: [ReactiveFormsModule, FormlyModule],
  declarations: [PrefixAndSuffixWrapper],
})
export class SharedFormsModule {}
