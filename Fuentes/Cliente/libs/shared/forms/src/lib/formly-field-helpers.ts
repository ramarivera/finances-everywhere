import { AbstractControl } from '@angular/forms';
import { FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import * as _ from 'lodash';

interface BasicFieldConfig<TModel> {
  key: Extract<keyof TModel, string>;
  label?: string;
  type?: string;
  requiredName?: string;
}

export function buildField<TModel>(
  { key, label, type, requiredName }: BasicFieldConfig<TModel>,
  templateOptionsOverrides?: Partial<FormlyTemplateOptions>,
  configOverrides?: Partial<FormlyFieldConfig>
): FormlyFieldConfig {
  const fieldConfig: FormlyFieldConfig = {
    ...configOverrides,
    key,
    type: type || 'input',
    templateOptions: {
      type: 'text',
      label: label || key.charAt(0).toUpperCase() + key.slice(1),
      appearance: 'outline',
      required: !!requiredName,
      ...templateOptionsOverrides,
    },
    modelOptions: {
      updateOn: 'blur',
    },
  };

  if (requiredName) {
    fieldConfig.validation = _.merge(fieldConfig.validation, {
      messages: {
        required: `${requiredName} is required`,
      },
    });
  }

  return fieldConfig;
}

export function buildOptionsField<TModel, TOptions>(
  basicConfig: Omit<BasicFieldConfig<TModel>, 'type'>,
  optionsGetter: () => TOptions[] | undefined,
  labelGetter: (item: TOptions) => string,
  valueGetter: (item: TOptions) => number
) {
  return buildField(
    { ...basicConfig, type: 'select' },
    {},
    {
      expressionProperties: {
        'templateOptions.disabled': () => !optionsGetter()?.length,
        'templateOptions.options': () =>
          buildOptionsFromObjects(optionsGetter(), labelGetter, valueGetter),
      },
    }
  );
}

export function buildGroup<TModel, TKey extends Extract<keyof TModel, string>>(
  key: TKey,
  fields: BasicFieldConfig<TModel[TKey]>[],
  validators: any
) {
  const field: FormlyFieldConfig = {
    key,
    fieldGroup: fields.map((x) => buildField(x)),
    validators,
  };

  return field;
}

const stringIsNumber = (value: string) => isNaN(Number(value)) === false;

export function getOptionsFromEnum(enumeration: any) {
  return Object.keys(enumeration)
    .filter(stringIsNumber)
    .map((key) => ({ label: enumeration[key], value: Number(key) }));
}

export function buildOptionsFromObjects<T>(
  data: T[] | undefined,
  labelGetter: (item: T) => string,
  valueGetter: (item: T) => number
) {
  if (!data) {
    return [];
  }
  return data.map((item) => ({ label: labelGetter(item), value: valueGetter(item) }));
}

export function buildDefaultEndDateValidator({
  startDatePath = 'from',
  endDatePath = 'until',
  startDateLabel = 'from',
  endDateLabel = 'until',
  validatorName = 'endDateGreater',
}: {
  startDatePath?: string;
  endDatePath?: string;
  startDateLabel?: string;
  endDateLabel?: string;
  validatorName?: string;
}) {
  return {
    [validatorName]: {
      expression: (control: AbstractControl) => {
        const datesGroup = control.value;
        if (!datesGroup) {
          return true;
        }
        const startDate = datesGroup[startDatePath];
        const endDate = datesGroup[endDatePath];
        return !startDate || !endDate || startDate <= endDate;
      },
      message: `${endDateLabel} should be smaller than ${startDateLabel}`,
      errorPath: endDatePath,
    },
  };
}
