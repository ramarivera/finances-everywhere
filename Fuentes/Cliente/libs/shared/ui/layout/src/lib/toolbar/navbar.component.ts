import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'dacs-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent {
  @Output()
  clicked = new EventEmitter<void>();

  constructor() {}

  onButtonClick() {
    this.clicked.emit();
  }
}
