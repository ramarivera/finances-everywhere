import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dacs-page-container',
  templateUrl: './page-container.component.html',
  styleUrls: ['./page-container.component.css'],
})
export class PageContainerComponent implements OnInit {
  @Input()
  refreshing = false;

  @Input()
  loading?: boolean;

  @Input()
  size: 'large' | 'medium' | 'small' = 'large';

  private flexSizes = {
    large: '100%',
    medium: '75%',
    small: '50%',
  };

  constructor() {}

  ngOnInit() {}

  get isLoading() {
    return this.loading === true;
  }

  get flexBasis() {
    const basis = this.flexSizes[this.size];
    return basis || this.flexSizes.large;
  }
}
