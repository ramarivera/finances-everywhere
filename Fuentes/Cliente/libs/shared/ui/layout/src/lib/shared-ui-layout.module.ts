import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedUiButtonsModule } from '@dacs-fe/shared/ui/buttons';
import { SharedUiLoadingModule } from '@dacs-fe/shared/ui/loading';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { LayoutContainerComponent } from './layout-container/layout-container.component';
import { MainContentContainerComponent } from './main-content-container/main-content-container.component';
import { PageContainerComponent } from './page-container/page-container.component';
import { PageContentComponent } from './page-content/page-content.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SliderPanelComponent } from './slider-panel/slider-panel.component';
import { NavbarComponent } from './toolbar/navbar.component';

@NgModule({
  declarations: [
    LayoutContainerComponent,
    MainContentContainerComponent,
    SidebarComponent,
    NavbarComponent,
    PageHeaderComponent,
    PageContainerComponent,
    PageContentComponent,
    SliderPanelComponent,
  ],
  imports: [
    CommonModule,
    SharedUiMaterialModule,
    RouterModule,
    SharedUiLoadingModule,
    SharedUiButtonsModule,
  ],
  exports: [
    LayoutContainerComponent,
    PageHeaderComponent,
    PageContainerComponent,
    PageContentComponent,
    SliderPanelComponent,
  ],
})
export class SharedUiLayoutModule {}
