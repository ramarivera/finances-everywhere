import { Component, Input } from '@angular/core';
// tslint:disable: no-input-rename

@Component({
  selector: 'dacs-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css'],
})
export class PageHeaderComponent {
  @Input()
  title = '';

  constructor() {}
}
