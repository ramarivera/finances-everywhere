import { Component } from '@angular/core';

@Component({
  selector: 'dacs-layout-container',
  templateUrl: './layout-container.component.html',
  styleUrls: ['./layout-container.component.css'],
})
export class LayoutContainerComponent {
  sidebarState = false;

  onNavbarButtonClicked() {
    this.sidebarState = !this.sidebarState;
  }
}
