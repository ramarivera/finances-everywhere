import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dacs-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent {
  private _opened = false;

  @Input()
  set opened(value: boolean) {
    this._opened = value;
    this.openedChange.emit(this._opened);
  }

  get opened() {
    return this._opened;
  }

  @Output()
  openedChange = new EventEmitter<boolean>();

  toggle() {
    this.opened = !this.opened;
  }

  constructor() {}
}
