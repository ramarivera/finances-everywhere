import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

export interface NotificationData {
  type: 'alert' | 'warn' | 'info' | 'success';
  message: string;
  icon?: string;
}

const typeMap: { [key: string]: string } = {
  alert: 'red',
  info: 'automatic',
  success: 'green',
  warn: 'orange',
};

@Component({
  selector: 'dacs-fe-notification-snack',
  templateUrl: './notification-snack.component.html',
  styleUrls: ['./notification-snack.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationSnackComponent {
  message: string;
  icon: string | undefined;
  type: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: NotificationData) {
    this.message = data.message;
    this.icon = data.icon;
    this.type = data.type;
  }

  get style() {
    return {
      color: typeMap[this.type],
    };
  }
}
