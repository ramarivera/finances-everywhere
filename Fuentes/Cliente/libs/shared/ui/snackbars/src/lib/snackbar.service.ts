import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationData, NotificationSnackComponent } from './notification-snack/notification-snack.component';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  durationInSeconds = 5;

  constructor(private snackBar: MatSnackBar) {}

  openSnackBar(type: NotificationData['type'], message: string, icon?: string) {
    this.snackBar.openFromComponent(NotificationSnackComponent, {
      duration: this.durationInSeconds * 1000,
      data: {
        type,
        message,
        icon,
      },
    });
  }
}
