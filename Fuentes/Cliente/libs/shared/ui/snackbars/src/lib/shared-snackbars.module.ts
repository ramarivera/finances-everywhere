import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationSnackComponent } from './notification-snack/notification-snack.component';

@NgModule({
  imports: [CommonModule],
  declarations: [NotificationSnackComponent],
  exports: [NotificationSnackComponent],
  entryComponents: [NotificationSnackComponent],
})
export class SharedUiSnackBarsModule {}
