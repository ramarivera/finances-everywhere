import { Component, Input } from '@angular/core';

@Component({
  selector: 'dacs-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.css'],
})
export class LoadingBarComponent {
  @Input()
  visible = false;

  @Input()
  mode = 'indeterminate';

  @Input()
  value?: number;

  @Input()
  color = 'primary';

  constructor() {}
}
