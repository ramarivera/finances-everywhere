import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { LoadingBarComponent } from './loading-bar/loading-bar.component';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  imports: [CommonModule, SharedUiMaterialModule],
  declarations: [LoadingBarComponent, SpinnerComponent],
  exports: [LoadingBarComponent, SpinnerComponent],
})
export class SharedUiLoadingModule {}
