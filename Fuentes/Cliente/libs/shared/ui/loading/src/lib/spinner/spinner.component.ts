import { Component, Input } from '@angular/core';

@Component({
  selector: 'dacs-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css'],
})
export class SpinnerComponent {
  @Input()
  mode = 'indeterminate';

  @Input()
  value?: number;

  @Input()
  color = 'primary';

  @Input()
  diameter = 100;

  constructor() {}
}
