import { Overlay } from '@angular/cdk/overlay';
import { Injectable, Type } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogData } from './confirmation-dialog/confirmation-dialog.model';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private dialog: MatDialog, private overlay: Overlay) {}

  openConfirmation(title: string, body: string) {
    const dialogConfig = new MatDialogConfig<ConfirmationDialogData>();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title,
      body,
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this.dialog.open(
      ConfirmationDialogComponent,
      dialogConfig
    );

    return dialogRef.afterClosed();
  }

  openDialog<TDialogResult>(component: Type<any>): Observable<TDialogResult | undefined>;
  openDialog<TDialogResult, TDialogData>(
    component: Type<any>,
    data: TDialogData
  ): Observable<TDialogResult | undefined>;
  openDialog<TDialogResult, TDialogData>(
    component: Type<any>,
    data?: TDialogData
  ): Observable<TDialogResult | undefined> {
    const dialogConfig = new MatDialogConfig<TDialogData>();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    dialogConfig.panelClass = 'dacs-dialog-container';
    dialogConfig.minWidth = '30vw';
    dialogConfig.maxWidth = '80vw';

    const dialogRef: MatDialogRef<any, TDialogResult> = this.dialog.open(component, dialogConfig);

    return dialogRef.afterClosed();
  }
}
