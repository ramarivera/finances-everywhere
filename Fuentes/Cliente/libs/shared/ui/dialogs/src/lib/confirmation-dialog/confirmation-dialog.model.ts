export interface ConfirmationDialogData {
  title: string;
  body?: string;
  yesLabel?: string;
  noLabel?: string;
}
