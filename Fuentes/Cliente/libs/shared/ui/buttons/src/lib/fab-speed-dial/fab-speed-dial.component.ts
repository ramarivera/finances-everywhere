import { Component, OnInit } from '@angular/core';
import { ButtonDef } from '../button-def.model';
import { FabButtonService } from '../fab-button.service';
import { fabSpeedDialAnimations } from './fab-speed-dial.animations';

@Component({
  selector: 'dacs-fab-speed-dial',
  templateUrl: './fab-speed-dial.component.html',
  styleUrls: ['./fab-speed-dial.component.scss'],
  animations: fabSpeedDialAnimations,
})
export class FabSpeedDialComponent implements OnInit {
  fabTogglerState: 'active' | 'inactive' = 'inactive';

  activeButtons: ButtonDef[] = [];
  visibleButtons: ButtonDef[] = [];

  constructor(private fabButtonService: FabButtonService) {}

  ngOnInit() {
    this.fabButtonService.activeButtons$.subscribe(activeButtons => {
      this.activeButtons = activeButtons;
    });
  }

  showItems() {
    this.fabTogglerState = 'active';
    this.visibleButtons = this.activeButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.visibleButtons = [];
  }

  onToggleFab() {
    this.fabTogglerState === 'active' ? this.hideItems() : this.showItems();
  }

  executeCallback(button: ButtonDef) {
    button.callback();
    this.onToggleFab();
  }
}
