import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';
import { FabSpeedDialComponent } from './fab-speed-dial/fab-speed-dial.component';

@NgModule({
  imports: [CommonModule, SharedUiMaterialModule],
  declarations: [FabSpeedDialComponent],
  exports: [FabSpeedDialComponent],
})
export class SharedUiButtonsModule {}
