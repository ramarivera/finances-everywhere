export interface ButtonDef {
  icon: string;
  tooltip?: string;
  callback: () => void;
  color?: string;
}
