import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { ButtonDef } from './button-def.model';

@Injectable({
  providedIn: 'root',
})
export class FabButtonService {
  activeButtons$: Observable<ButtonDef[]>;

  private buttonsSubject = new BehaviorSubject<ButtonDef[]>([]);

  private get buttons() {
    return this.buttonsSubject.value;
  }

  constructor() {
    this.activeButtons$ = this.buttonsSubject.asObservable();
  }

  addButton(button: ButtonDef) {
    return this._addButton(button);
  }

  private _addButton(button: ButtonDef | ButtonDef[]) {
    this.buttonsSubject.next(_.flatten([...this.buttons, button]));
    return () => this.removeButton(button);
  }

  private removeButton(buttons: ButtonDef | ButtonDef[]) {
    if (Array.isArray(buttons)) {
      this.buttonsSubject.next(this.buttons.filter(x => !buttons.includes(x)));
    } else {
      this.buttonsSubject.next(this.buttons.filter(x => x !== buttons));
    }
  }
}
