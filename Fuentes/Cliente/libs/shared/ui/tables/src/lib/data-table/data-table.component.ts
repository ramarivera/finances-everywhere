import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';
import { Observable, of, Subscription } from 'rxjs';
import { debounceTime, delay, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
  SimpleChanges,
  OnChanges,
  EventEmitter,
  OnDestroy,
  TemplateRef,
} from '@angular/core';

export interface SimpleColumnDef<TItem = any> {
  header: string;
  property: string | ((item: TItem) => string);
  format?: (value: string) => string;
}

export interface CommandColumnDef {
  header: string;
  template: TemplateRef<any>;
}

@Component({
  selector: 'dacs-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataTableComponent<TItem> implements AfterViewInit, OnInit, OnChanges, OnDestroy {
  @ViewChild(MatPaginator) paginator?: MatPaginator;
  @ViewChild(MatSort) sort?: MatSort;
  @ViewChild(MatTable) table!: MatTable<TItem>;

  private dataSource: MatTableDataSource<TItem>;

  @Input()
  columns: (SimpleColumnDef | CommandColumnDef)[] = [];

  _columnDataAccessors: { [column: string]: { print: (data: TItem) => string } } = {};

  @Input()
  items: TItem[] = [];

  @Input()
  enablePagination = true;

  @Input()
  enableSorting = true;

  @Input()
  enableFiltering = true;

  @Input()
  displayIndex = false;

  keyUp = new EventEmitter<string>();

  filterChanges$: Observable<string> = of('');

  private filterChangesSubscription: Subscription | null = null;
  filter = '';

  get simpleColumns() {
    return this.columns.filter((x) => 'property' in x) as SimpleColumnDef[];
  }

  get commandColumns() {
    return this.columns.filter((x) => 'template' in x) as CommandColumnDef[];
  }

  constructor() {
    this.dataSource = new MatTableDataSource<TItem>();
  }

  ngOnInit() {
    if (this.enableFiltering) {
      this.filterChanges$ = this.keyUp.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap((search) => of(search).pipe(delay(500)))
      );
    }

    this.buildDataAccessors(this.simpleColumns);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items) {
      this.dataSource.data = this.items;
    }
    if (changes.columns) {
      this.buildDataAccessors(this.simpleColumns);
    }
  }

  ngOnDestroy() {
    if (this.filterChangesSubscription) {
      this.filterChangesSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    if (this.sort) {
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item: TItem, sortHeaderId: string) => {
        return this._columnDataAccessors[sortHeaderId].print(item);
      };
    }

    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }

    this.table.dataSource = this.dataSource;

    this.filterChangesSubscription = this.filterChanges$.subscribe((filter) => {
      this.dataSource.filter = filter;
      this.filter = filter;
    });
  }

  buildDataAccessors(columns: SimpleColumnDef[]) {
    this._columnDataAccessors = {};
    columns.forEach((column) => {
      this._columnDataAccessors[column.header] = {
        print: _.memoize((item: TItem) => {
          return this.resolveValue(item, column);
        }),
      };
    });
  }

  resolveValue(item: TItem, column: SimpleColumnDef) {
    let cellValue: any;
    if (typeof column.property === 'string') {
      cellValue = _.get(item, column.property);
    } else {
      cellValue = column.property(item);
    }

    return (column.format ?? _.toString)(cellValue);
  }

  get columnNames() {
    const columnNames = this.columns.map((x) => x.header);
    return this.displayIndex ? ['index', ...columnNames] : columnNames;
  }
}
