import { Pipe, PipeTransform } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { escapeRegExp } from 'lodash';

@Pipe({
  name: 'highlight',
})
export class HighlightPipe implements PipeTransform {
  transform(
    content?: string,
    search?: string,
    caseSensitive: boolean = false,
    highlightClass: string = 'search-highlight'
  ): SafeHtml {
    try {
      if (content) {
        const caseFlag = 'g' + (caseSensitive ? '' : 'i');
        const regex = new RegExp(escapeRegExp(search), caseFlag);
        const replaced = content.replace(regex, match => `<span class="${highlightClass}">${match}</span>`);
        return replaced;
      } else {
        return '';
      }
    } catch (e) {
      console.log(e);
      return '';
    }
  }
}
