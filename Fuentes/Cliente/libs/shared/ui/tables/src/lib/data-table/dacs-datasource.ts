import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort, SortDirection } from '@angular/material/sort';
import { get } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';

export class DacsDataSource<TItem> extends DataSource<TItem> {
  paginator?: MatPaginator;
  sort?: MatSort;

  public set items(items: TItem[]) {
    this._items = [...items];
    this.itemsSubject.next(items);
    if (this.paginator) {
      this.paginator.length = items.length;
    }
  }

  public get items() {
    return this._items;
  }

  private _items: TItem[] = [];
  private itemsSubject = new BehaviorSubject<TItem[]>([]);

  constructor() {
    super();
  }

  connect(): Observable<TItem[]> {
    const getPageObservable = () => {
      if (!this.paginator) {
        return of<PageEvent>();
      }

      const initialPage = new PageEvent();
      initialPage.pageIndex = this.paginator.pageIndex;
      initialPage.length = this.items.length;
      initialPage.pageSize = this.paginator.pageSize;

      return this.paginator.page.pipe(startWith(initialPage));
    };

    const getSortObservable = () => {
      if (!this.sort) {
        return of<Sort>();
      }

      const initialSort: Sort = { active: '', direction: '' };

      return this.sort.sortChange.pipe(startWith(initialSort));
    };

    const dataObservable = combineLatest(this.itemsSubject.asObservable(), getPageObservable(), getSortObservable());

    return dataObservable.pipe(
      debounceTime(0),
      map(([items, page, sort]) => {
        const pagedData = page ? this.getPagedData(items, page.pageIndex, page.pageSize) : items;
        const sortedData = sort ? this.getSortedData(pagedData, sort.active, sort.direction) : pagedData;
        return sortedData;
      })
    );
  }

  disconnect() {}

  private getPagedData(data: TItem[], index: number, size: number) {
    const startIndex = index * size;
    return data.slice(startIndex, startIndex + size);
  }

  private getSortedData(data: TItem[], sortProperty: string, sortDirection: SortDirection) {
    if (!sortProperty || sortDirection === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = sortDirection === 'asc';
      return compare(resolve(a, sortProperty), resolve(b, sortProperty), isAsc);
    });
  }
}

function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

function resolve<T>(item: T, property: string) {
  return get(item, property, undefined);
}
