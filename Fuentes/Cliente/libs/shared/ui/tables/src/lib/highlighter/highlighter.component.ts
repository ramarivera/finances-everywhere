import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dacs-highlighter',
  templateUrl: './highlighter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HighlighterComponent implements OnInit {
  @Input()
  text = '';

  @Input()
  search = '';

  @Input()
  highlightClass?: string;

  @Input()
  caseInsensitive = false;

  constructor() {}

  ngOnInit() {}
}
