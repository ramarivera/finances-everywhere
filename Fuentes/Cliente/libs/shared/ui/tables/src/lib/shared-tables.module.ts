import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DataTableComponent } from './data-table/data-table.component';
import { HighlightPipe } from './highlight.pipe';
import { HighlighterComponent } from './highlighter/highlighter.component';
import { SharedUiMaterialModule } from '@dacs-fe/shared/ui/material';

@NgModule({
  imports: [CommonModule, SharedUiMaterialModule],
  declarations: [HighlighterComponent, HighlightPipe, DataTableComponent],
  exports: [HighlighterComponent, HighlightPipe, DataTableComponent],
})
export class SharedTablesModule {}
