import { chain, externalSchematic, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import * as path from 'path';

// export interface NormalizedSchema extends Schema {
//   name: string;
//   fileName: string;
//   projectRoot: string;
//   projectDirectory: string;
//   parsedTags: string[];
// }

export default function(schema: any): Rule {
  const featureName = schema['feature-name']
  const tempFilePath = path.join('libs', featureName, 'temp')
  return chain([
    (tree: Tree) => tree.create(tempFilePath, ''),
    externalSchematic('@nrwl/workspace', 'lib', {
      name: 'feature-shell',
      directory: featureName,
      routing: true,
      lazy: true,
      tags: `scope:${featureName}, type:feature`,
      style: 'sass',
    }),
    (tree: Tree) => tree.delete(tempFilePath),
  ])
}
